leaf_file_name = 'your_simulation_result.h5';

time_steps = h5read(leaf_file_name, '/time_steps');
time_steps_idx = h5read(leaf_file_name, '/time_steps_idx');

num_cells = zeros(i,1);

for i = 1:length(time_steps)
    step_idx = time_steps_idx(i);
    cells_id = h5read(leaf_file_name, ['/step_', num2str(step_idx), '/cells_id']);
    num_cells(i) = length(cells_id);
end

plot(time_steps, num_cells, '.-');
xlabel('Time (sec)');
ylabel('Number of cells');
grid on;

