%############################################################################
% Copyright 2011-2016 Universiteit Antwerpen
%
% Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
% the European Commission - subsequent versions of the EUPL (the "Licence");
% You may not use this work except in compliance with the Licence.
% You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
%
% Unless required by applicable law or agreed to in writing, software 
% distributed under the Licence is distributed on an "AS IS" basis,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the Licence for the specific language governing
% permissions and limitations under the Licence.
%############################################################################


\chapter{VPTissue HDF5 file specification}
\label{chap:SimPT HDF5 file specification}

The VPTissue framework uses a consistently defined file format for data
storage and interchange that is based on the HDF5 storage format for numerical
data.  Hierarchical Data Format 5 files are self-descriptive and well
structured. This document describes the structure of HDF5 based storage for
data files used in VPTissue and its ecosystem of related tools.

The internal data structure inside VPTissue can be summarized by the following
statements:

\begin{itemize}
    \item nodes are points in space
    \item cells are polygons with nodes connected by edges nodes 
    \item walls (membranes) are defined as entities separating two cells
    \item cells know their nodes and walls (and hence their neighbouring cells)
    \item walls know the two cells they connect and their two end-nodes
\end{itemize}

This is a (simplified) variant of what's commonly called a \emph{winged
edge}\footnote{\url{http://en.wikipedia.org/wiki/Winged_edge}} representation.
We use these connections to define arrays in the HDF5 file that describe the
tissue structure.

HDF5 is a standardized file format for storing general numerical data in a
hierarchical structure. The two main types of entities in a HDF5 file are
\emph{groups} and \emph{datasets}.

\begin{itemize}
    \item Groups define the hierarchical structure of the data.  They are
        analogous to directories or file folders in a (UNIX-like) file system.
    \item Datasets contain numerical data stored in general rank-n arrays of
        well-defined dimensions and type of data they store. They are the
        ``files'' in the file system analogy.
\end{itemize}

Navigating through a HDF5 file is also very analogous to browsing through a
file system: The root of the hierarchy is denoted by \texttt{/} and subgroups
and datasets are addressed by separating their names with a \texttt{/}.  For
example: a dataset called \texttt{nodes\_xy} inside a subgroup \texttt{step\_2}
of the root group \texttt{/} has the ``absolute'' path
\texttt{/step\_2/nodes\_xy}.

A HDF5 file is self-descriptive in the sense that it contains all the necessary
information to read the stored information correctly without any other
additional resources.  For instance, datasets know the numeric type of data
they contain. The format, however, does not define any semantics or
interpretation of the data.  A gentle overview of HDF5 can be found on
Wikipedia\footnote{\url{https://en.wikipedia.org/wiki/Hierarchical_Data_Format}}.
More in-depth info and technical documentation on HDF5 can be found on the HDF
Group website\footnote{\url{https://www.hdfgroup.org/}}.

\section{HDF5 file stucture}
\subsection{Root level}
 
The root of the VPTissue file contains two datasets related to the time
progression of the simulation. They are required and must be extendable, which
means they can grow or shrink as more simulation steps are appended or
unnecessary steps are being purged.

\begin{table}[htp!]
\footnotesize
\centering
\begin{tabular}{@{}llll@{}}
    \toprule
    Name & Data type & Dimensions & Notes \\
    \midrule
    \texttt{time\_steps} & \texttt{double} & (\#steps) & Time stamps in simulation specific time units \\
    \texttt{time\_steps\_idx} & \texttt{int} & (\#steps) & Index values of the simulation steps \\
    \bottomrule
\end{tabular}
\caption{Time steps}
\end{table}

The important point to note is that the file contains a subset of all the
steps performed by the simulator, depending on the write-stride.  For example:
if the stride is 3 and the simulator ran 10 steps starting from 0 we would end
up having:

\begin{itemize}
    \item \texttt{time\_steps} : \texttt{[0.0, 0.3, 0.6, 0.9]}
    \item \texttt{time\_steps\_idx} : \texttt{[0, 3, 6, 9]}
\end{itemize}

The numbers in \texttt{time\_steps\_idx} translate which $m$-th saved state
corresponds to which $n$-th simulator state. The state of the simulation at
specific times is saved in separate \texttt{/step\_\{n\}} subgroups of the
\texttt{/} group.  Note that in the case of stride being 1, $n$ will have the
same meaning as $m$. Although this is not true in a general case.

\subsection{Inside a \texttt{/step\_\{n\}} group}

For a given time step index n the \texttt{/step\_\{n\}} group contains all the
information about the geometry of the tissue, the values of all the attributes
that define the geometry as well as the parameters used in the current
simulation step. Table~\ref{tab:stepn} summarises all the datasets found inside
a \texttt{/step\_\{n\}} that are relevant to geometry and connectivity of
cells:

\begin{table}[htp!]
\footnotesize
\centering
\begin{tabular}{@{}lllp{5cm}@{}}
    \toprule
    Name & Data type & Dimensions & Notes \\
    \midrule
    \texttt{nodes\_id} & \texttt{int} & (\#nodes) & IDs of all the
        nodes in the tissue \\
    \texttt{nodes\_xy} & \texttt{double} & (\#nodes,2) & 2D
        coordinates of nodes. In figure~\ref{fig:auxin_leaf_labelled} nodes are
        represented by yellow points labeled with black numbers. \\
    \texttt{cells\_id} & \texttt{int} & (\#cells) & IDs of all the
        cells in the tissue \\
    \texttt{cells\_num\_nodes} & \texttt{int} & (\#cells) &
        Number of nodes (yellow points in \ref{fig:auxin_leaf_labelled}) used
        to define the polygon of each cell. \\
    \texttt{cells\_nodes} & \texttt{int} &
        (\#cells,max(\#nodes/cell)) & Each row represents a cell. The elements
        in each row are indices of the nodes (as defined in \texttt{nodes\_xy})
        that define the polygon of the cell in counterclockwise order. As such,
        this dataset defines the geometry of all cells. For conveniece, rows
        that represent cells with less than maximal number of nodes are padded
        with -1 (which is an invalid node index). \\
    \texttt{cells\_num\_walls} & \texttt{int} & (\#cells) & Number
        of walls for each cell. \\
    \texttt{cells\_walls} & \texttt{int} &
        (\#cells,max(\#walls/cell)) & IDs of walls in each cell. \\
    \texttt{walls\_id} & \texttt{int} & (\#walls) & IDs of all the
        walls in the tissue \\
    \texttt{walls\_cells} & \texttt{int} & (\#walls,2) & Indices
        of the two cells connected by a wall. The region outside the tissue is
        labeled by -1. All ``outer'' cells that lie on the boundary of the
        tissue have this special -1 cell among their neighbors. In figure~
        \ref{fig:auxin_leaf_labelled} walls are denoted by red labels.\\
    \texttt{walls\_nodes} & \texttt{int} & (\#walls,2) & Each wall
        has two extreme nodes. The indices of these nodes are kept in this
        dataset. The order in which the nodes are specified is tied to the
        order in which the two neighboring cells are given in
        \texttt{walls\_cells}. See red arrows in \ref{fig:auxin_leaf_labelled}
        for a visual clarification of the orientation that is used.\\
    \bottomrule
\end{tabular}
\caption{Required datasets inside one step}
\label{tab:stepn}
\end{table}

Additionally, nodes, cells and walls can have an arbitrary number of named
attributes with values of any type. These are defined by datasets with a common
prefix name such as \texttt{nodes\_attr\_} concatenated with the name of the
attribute.

\begin{table}[htp!]
\footnotesize
\centering
\begin{tabular}{@{}lllp{6cm}@{}}
    \toprule
    Name & Data type & Dimensions & Notes \\
    \midrule
    \texttt{nodes\_attr\_\{name\}} & \emph{any} & (\#nodes) & Datasets with values of node-based attributes \\
    \texttt{cells\_attr\_\{name\}} & \emph{any} & (\#cells) & Datasets with values of cell-based attributes \\
    \texttt{walls\_attr\_\{name\}} & \emph{any} & (\#walls) & Datasets with values of wall-based attributes \\
    \bottomrule
\end{tabular}
\caption{Optional datasets inside one step related to cell/node/wall attributes}
\label{tab:attributes}
\end{table}

The used data types are summarized in tabel~\ref{tab:datatypes}.

\begin{table}[htp!]
\footnotesize
\centering
\begin{tabular}{@{}lllp{6cm}@{}}
    \toprule
    Name & HDF5 data type & Notes \\
    \midrule
    \texttt{int} & \texttt{H5T\_STD\_I32LE} & or compatible, such as \texttt{H5T\_NATIVE\_INT} \\
    \texttt{double} & \texttt{H5T\_IEEE\_F64LE} & or compatible, such as \texttt{H5T\_NATIVE\_DOUBLE} \\
    \texttt{string} & \texttt{H5T\_STRING} & Variable length, C-style null-terminated string \\
    \bottomrule
\end{tabular}
\caption{HDF5 data types}
\label{tab:datatypes}
\end{table}

\begin{figure}[htp!]
	\centering
	\includegraphics[width=10cm]{images/auxin_leaf_labelled}
    \caption{A schematic representation of the geometry as used by VPTissue.
    Yellow points are the \emph{nodes}, labeled by black numbers. Green numbers
represent the labels of the four \emph{cells} in this particular example. The
green arrows show how nodes are connected to form a cell. For example: cell 1
is made up from nodes (12, 10, 1, 21, 2, 3, 19, 20, 18), in that precise order.
Red labels represent the walls. For example: wall 5 connects cells 1 and 3, in
that precise order.}
	\label{fig:auxin_leaf_labelled}
\end{figure}

\section{Tools}
\subsection{HDFView}
HDF5 files contain data in binary form and are not human readable.
HDFView\footnote{\url{http://www.hdfgroup.org/hdf-java-html/hdfview/}} is a
Java-based easy to use graphical tool to browse, create and modify HDF5 file.
You can use it to quickly inspect VPTissue output for further processing.

\begin{figure}[htp!]
	\centering
	\includegraphics[width=10cm]{images/hdfview}
    \caption{A screenshot of the HDFView program while browsing the contents of
    a VPTissue results file.}
	\label{fig:hdfview}
\end{figure}

\subsection{Python}
Python supports handling of HDF5 files through the
\texttt{h5py}\footnote{\url{http://www.h5py.org/}} package. Most probably you
can install it using your system's package manager or with \texttt{pip}. Access
to the data in HDF5 files is provided using
\texttt{numpy}\footnote{\url{http://www.numpy.org/}} arrays which makes working
with \texttt{h5py} very straightforward.

The following Python script can be used to plot the
example file used above (or any other file from VPTissue, since this script
only uses a rather small subset of the available data).

\lstdefinestyle{pylisting}{numbers=left, numberstyle=\tiny, numbersep=7pt, basicstyle=\ttfamily\footnotesize\bfseries, language=python, frame=leftline, keywordstyle=\color{blue}, identifierstyle=\color{red}, commentstyle=\color[gray]{.6}, showstringspaces=false}
\lstinputlisting[language=Python, style=pylisting]{simPT_HDF5_example.py}

Running \texttt{python simPT\_HDF5\_example.py tissue.h5} gives
figure~\ref{fig:tissue_h5}.

\begin{figure}[htp!]
    \centering
    \includegraphics[width=10cm]{images/tissue_h5}
    \caption{A VPTissue HDF5 file plot with Python}
    \label{fig:tissue_h5}
\end{figure}

\subsection{Matlab}
Matlab supports HDF5 files out of the box. See: MathWorks Documentation Center
for a detailed
overview\footnote{\url{http://www.mathworks.nl/help/matlab/hdf5-files.html}}.
Reading data from a VPTissue file (using the ``High-Level'' functions) is
straightforward; a really basic example is shown in the following script.

\lstdefinestyle{matlablisting}{numbers=left, numberstyle=\tiny, numbersep=7pt, basicstyle=\ttfamily\footnotesize\bfseries, language=matlab, frame=leftline, keywordstyle=\color{blue}, identifierstyle=\color{red}, commentstyle=\color[gray]{.6}, showstringspaces=false}
\lstinputlisting[language=Matlab, style=matlablisting]{simPT_HDF5_example.m}

\begin{figure}[htp!]
    \centering
    \includegraphics[width=10cm]{images/simPT_matlab}
    \caption{Analysis of VPTissue HDF5 files in Matlab}
    \label{fig:simPT_matlab}
\end{figure}

\subsection{ParaView}
We have also developed a HDF5-based plugin for
ParaView\footnote{\url{http://www.paraview.org/}}, which can be found in the
\texttt{src/main/resources/paraview/} directory.

