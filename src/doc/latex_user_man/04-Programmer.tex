%############################################################################
% Copyright 2011-2016 Universiteit Antwerpen
%
% Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
% the European Commission - subsequent versions of the EUPL (the "Licence");
% You may not use this work except in compliance with the Licence.
% You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
%
% Unless required by applicable law or agreed to in writing, software 
% distributed under the Licence is distributed on an "AS IS" basis,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the Licence for the specific language governing
% permissions and limitations under the Licence.
%############################################################################

\chapter{Programming with the VPTissue framework}
\label{chap:Programming}


VPTissue is a simulation framework with C++ as its development language. The code is up-to-date with the C++11 standard (\cite{Stroustrup2013,Lippman2013} and adheres to current coding practices.

We have done our utmost to facilitate the job of extending or modifying the simulator and or the models it simulates through separation into layers and packages, minimization dependencies, classes with clear cut responsibilities, naming etcetera.

Developers adopting the platform will of course need to spend some time inspecting the code and familiarizing themselves with it. The first task will most probably be adding models which  means adding components and or attributes to the code base. in the section below we describe briefly how to go about these tasks.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Adding attribute
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Adding models and components}
\label{sec:Components}
The code to deal with models is to be found in two locations:
\begin{description}
%
\item[src/main/cpp\_sim/model] \ \\
This code specifies the model interfaces and the factory construction provides a component, given its name. The code need only be extended if and when the user decides to introduce a new component type. In that case a copy-paste-modify approach starting from an existing component is the easy way to make the new code.
%
\item[src/main/models/''model family name''/components/''component type''] \ \\
This code contains the components for each of the model families, separated into a sub directory per component type. If and when the user intends to add a specific component of an existing model family, the code for that component has to be put into the sub directory of the corresponding type and the file \texttt{factories.cpp} in the sub directory has to be edited to register the new component and its name and the \texttt{CMakeLists.txt} file has to be edited to include the new component. Again, the copy-paste-modify approach is the easiest. To add a new model all specific components need to be added.

If and when the user wants to introduce a new model family,  he has to remember that it is possible to ''borrow'' components for the Default model family, simply by not providing your own implementation -- see section \ref{sec:Components}. Thus a new sub directory 
\texttt{src/main/models/''model family name''/components/''component type''} has to be created only for each of the component types for which one has a specific implementation. The file implementing the component(s) and files \texttt{factories.h} and \texttt{factories.cpp} need to be added to the subdirectory. The code for the component and for the factories is again straightforward to make with the copy-paste-modify approach. Finally the \texttt{CMakeLists.txt}  file has to be copied and edited to reflect the names of the new component files.
%
\end{description}

In addition to generating the code for new models one needs to provide input data file. The can be built from scratch using the VPTissue Tissue editor (see \ref{chap:Editor}) or by copy-paste-modify of an existing input file. The modification can be done either with a text editor, an xml editor or the VPTissue Tissue editor, depending on your preference and the number of changes that need to be made.\\
\\
To implement a so called meta-component that combines the code of different (sub-)components, a new component class (\textit{e.g.} MetaSplit) has to be created and the .h files of the sub-components need to be included in the header. Both the Initialize and operator functions need to be adapted similar to the example below.\\
\\
\lstset{basicstyle=\footnotesize}
	\begin{lstlisting}
#include MetaSplit.h
#include Split1.h
#include Split2.h

void MetaSplit::Initialize(const CoreData& cd)
{
	m_cd =cd;
	...
	m_splitter1 = (g_component_factories.at("Split1"))(m_cd);
	m_splitter2 = (g_component_factories.at("Split2"))(m_cd);
}		

std::tuple<bool, bool, std::array<double>, 3>> MetaSplit::operator()\
(Cell* cell)
{
	...
	return ('some condition') ? m_splitter1(cell): m_splitter2(cell);
}
	\end{lstlisting}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Adding attribute
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Adding an attribute}
\label{sec:Attribute}
\lstset{basicstyle=\footnotesize}

When you want to add a new model or simulation parameter (like a kinetic constant) you simply need to add the text field with its value to the data input file ('leaf file'). Its value is then accessible to the component classes via the CoreData data structure, like other parameters. When you want to add an attribute to the tissue, cell, wall or node, you need 
to take a number of steps. We detail them here, by way of example, for the 
addition of an attribute name \texttt{solute} to cells. The cases for wall and node are similar
\begin{itemize}
%
	\item Write the attribute into the \texttt{CellAttributes.h} header 
	file and add getter and setter methods for the attribute if needed.
	\begin{lstlisting}
public:
    double GetSolute() const {return m_solute;}
    void   SetSolute(double s) {m_solute = s;}
protected:
    double m_solute;
	\end{lstlisting}
%
	\item In the \texttt{CellAttributes.cpp} implementation file initialize 
	the new attribute in the constructors and assignment operator.
		\begin{lstlisting}
CellAttributes::CellAttributes(...)
    , m_solute (0.0);
	     
CellAttributes&CellAttributes::operator=(...)
m_solute = src.m_solute;
	\end{lstlisting}
%
	\item In the \texttt{CellAttributes.cpp} implementation file extend 
	the \texttt{ReadPtree} and \texttt{ToPtree} methods to include processing of the new attribute.
	\begin{lstlisting}
void CellAttributes::ReadPtree (...)
m_solute = cell_pt.get<double>("solute");
	     
ptree CellAttributes::ToPtree() const
ret.put("solute", m_solute);
	\end{lstlisting}
%
	\item In the \texttt{MBMBuilder.cpp} implementation file 
	extend the \texttt{BuildCells} method to include processing of the new attribute.
		\begin{lstlisting}
void MBMBDirector::BuildCells(...)
attributes_pt.put("solute", 
    mesh_state.GetCellAttribute<double>(cell_idx, "solute"));
	\end{lstlisting}
%
	\item In the \texttt{Mesh.cpp} implementation file extend 
	the \texttt{GetState} method to include processing of the new attribute.
	\begin{lstlisting}
MeshState Mesh::GetState() const
// Define the attribute
state.AddCellAttribute<double>("solute");
// Set cell attribute value in state
state.SetCellAttribute<double>(cell_idx, "solute", c->GetSolute());
	\end{lstlisting}
%
	\item Add the new attribute to the input xml files. Do this 
	for the template workspace xml files and for the files in the test directories.
%
	\item Edit the tooltips code if you want the new attribute to show up in the tooltip.
%
\end{itemize}

