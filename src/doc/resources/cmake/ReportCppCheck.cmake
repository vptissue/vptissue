#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################

#
# Executes the cppcheck analysis provided the cppcheck executable
# can be found. Writes results to files named cppcheckdir-<dirname>.xml 
# in the target/reports/cppcheck dir.
# 
# Execution triggered via a make target named 
#       cppcheckdir-<dirname>
# to analyze sources in dir <dirname>, or a global make target named 
#       cppcheck
# to analyze all sources.
# Note: dirname is the dir path relative to project root.
#############################################################################

#############################################################################
# CppCheck reporting
#############################################################################
find_package( cppcheck )

#############################################################################
# Function to generate report; no-op function if cppcheck not found.
#############################################################################
function( ReportCppCheck THIS_DIR )
	
	if( CPPCHECK_FOUND )
		
		# construct target name based on path relative to project root directory
		string( REPLACE ${CMAKE_SOURCE_DIR} "" RELDIR ${THIS_DIR} )
		string( REPLACE "/" "-" RELDIR ${RELDIR} )
		set( TARGET cppcheckdir${RELDIR} )		
				
		add_custom_target( ${TARGET}
			${CPPCHECK_EXECUTABLE} --xml --enable=all --suppress=missingInclude --xml-version=2 --inline-suppr ${THIS_DIR} 2> ${CPPCHECK_DIR}/${TARGET}.xml  
			WORKING_DIRECTORY ${THIS_DIR} 
		)		
		add_dependencies( cppcheck ${TARGET} )
		
		unset( RELDIR )	
		unset( TARGET  )

	endif( CPPCHECK_FOUND )
		
endfunction( ReportCppCheck THIS_DIR )

#############################################################################
# Aggregate cppcheck target.
#############################################################################
if( CPPCHECK_FOUND )

	add_custom_target( cppcheck ALL )	
	set( CPPCHECK_DIR ${CMAKE_CURRENT_BINARY_DIR}/reports/cppcheck )
	file( MAKE_DIRECTORY ${CPPCHECK_DIR} )
	
endif( CPPCHECK_FOUND )

#############################################################################
