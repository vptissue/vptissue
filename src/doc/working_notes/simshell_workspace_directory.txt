#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################


 Normally, the directory cpp_simshell/workspace would have only contained the
template classes declared in the files
  Project.h,
  Project_def.h
  Workspace.h
  Workspace_def.h.

 If this was the case, all workspace logic would be 100% clean and templatized.
WorkspaceQtModel (in gui/qtmodel) would also have to be templatized, since it
creates a QAbstractItemModel from a generic workspace. 

However, Qt does not support QObjects to be templated. Hence, interfaces in the files
  IFile.h
  IProject.h
  IWorkspace.h
were added, as well as an abstract factory interface
  IWorkspaceFactory.h,
which is used by WorkspaceQtModel to do it's thing.

Instantiating the template classes comes down to writing your own IFile
implementation, and declaring a type
Workspace<Project<YourIFileImplementation>>. That type will be a complete
implementation of the IWorkspace interface.

Note that when constructing a Project object in a project directory, IFiles
are created for every file that is found in that directory. If the constructor
of your IFile implementation throws an Exception, that file is skipped and not 
added to the project. For instance, the startupFile (in cpp_simptshell/workspace) 
class constructor throws if the extension is not .xml or .h5.
