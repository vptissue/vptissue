#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################

#------------------------------------------------------------------------------------------
# Types of Viewers.
#------------------------------------------------------------------------------------------
There are at present four categories of viewers:

REMARK: The ViewerPreferences template has to be eliminated I think because there is no
commonality between the preferences in the different catgories of viewers.
If its only a matter of selecting the appropriate class for the particular viewer we
can have type members of traits to do that.

(1) Loggers:
The LogViewer and the LogWindowViewer.
REMARK: The Loggers do look at any preferences. Seems conceptually ok that
you do not stride a logger.

(2) Persistent file based:
The Hdf5Viewer opens a file and keeps it open and when triggered writes to it.
REMARK: There is only one for now, so it seems overkill to build it via a template.

(3) Single shot file based text viewers:
The Csv, Ply, Xml viewers when triggered open a file with a time labeled name, write 
to it and close it. All of them have a compression capability.
REMARK: There are three here, so maybe a tempate TextViewer would be ok. The preference 
should a  non-template class that has stride and gzip preferences. The annoying thing 
here is that you the need two Exporter classes, but you do want to change the number 
of template type parameters ---> use traits to encapsulate the necessary info.

(4) Single shot file / canvas based graphic viewers: 
The Pdf, Png, Qt, OpenGl viewers when triggered open a file with a time labeled name, 
write  to it and close it, or write to a Qt window. They all require Graphics preferences.
Again, there are enough of them to warrant building them via a template, provided we do
not need too many specializations. 
REMARK: So it would be nice that all viewers see the whole viewers preferences xml node, 
but that in turn means the viewers (remember it is a template and we do not want to 
specialize too much) need to know in which xml sub-node to look for the stride. Can we 
fix this via traits also?


#------------------------------------------------------------------------------------------
# HOW WORKSPACE/PROJECT PREFERENCES CHANGES PROPAGATE TO VIEWERS
#------------------------------------------------------------------------------------------

The first part of the propagation trajectory through the code is from a PTreeEditorWindow
in the gui to the WorkspaceController /ProjectController. It is implemented with Qt's signals 
and slots. To trace the tajectory you need to inspect the "emit" and "connect" statements 
and the various slot defintions.

(subordinate PTree related gui stuff signals a change) 
------> PTreeEditorWindow::Apply emits ApplyTriggered 
------> PTreeContainer emits Applied (direct connect on the ApplyTriggered signal)
------> ProjectController executes SLOT_ApplyPreferences 
------> ProjectController::SLOT_ApplyPreferences executes project->SetPreferences

Of course, there are many more signals emiited and slots activated to update the variuous gui 
widgets involved in the makeup of the gui PTreeWindow (a.o. PTreeView, PTreeMenu, ...).

The second part of the propagation journey proceeds via the observer pattern implementation
in de code (see class cpp_util/util/Subject.h):

IPreferences : public UA_CoMP::Util::Subject<Event::PreferencesChanged, std::weak_ptr<const void>>
Preferences: public IPreferences
Project : public Preferences

So project is a subject for PreferencesChanged events (that have the preferences ptree as payload) 
and Project::Preferences::SetPreferences executes a Notify. Similarly for workspace.

A MergedPreferences object is created for every session in startupFile<Ptree or Hdf5>::CreateSession,
and it is registered as an observer accepting PreferencesChanged event notifications from the project 
and the workspaces preferences. 

Through the MergedPreferences::GetChild (used to provide each kind of viewer with the appropriate subtree)
a hierarchy of such objects is set up. Each child registered as an observer accepting 
PreferencesChanged events from workspace and project preferences. 

The PreferencesChanged update to MergedPreferences objects (MegedPreferences::ListenPreferencesChanged 
is registered as event handler) triggers an MergedPreferencesChanged event notification to the 
PreferencesObserver<ViewerType> object in the viewers. The latter construction is implemented by the 
MergedPreferences: public UA_CoMP::Util::Subject<Event::MergedPreferencesChanged, std::weak_ptr<const void>>.

Each viewer has a PreferencesObserver<ViewerType> member object that manages the preferences that viewer
needs to know about (and stores those in data format so as not to have to read in the ptree all the time)
When instantiated these object register with their Update method with the corresponding MergedPreferences 
child in the PreferencesObserver<ViewerType>::Create method.


1) Workspace or project preferences changed (somehow)

                                 SimShell::Ws::Event::PreferencesChanged
 Workspace  ----------------------------------------->                        SimShell::Event::MergedPreferencesChanged
                                                      SimShell::Preferences   --------------------------------------->  BinaryPreferences<ViewerType>::Update
                                                      (created by GuiController or SimShellFactory)
 Project  (for currently opened project)  ----------->
 
 
2) Viewer changes preferences

               ::SetPreferences(prefs)
 Workspace <-----------------
                                                              ::Put<T>(path, value)
   OR                             SimShell::Preferences   <------------------------------ Viewer
   
               ::SetPreferences(prefs)
 Project <-------------------
 
 
 ... which causes another SimShell::Ws::Event::PreferencesChanged event to reach all viewers (including the one that put something in preferences)