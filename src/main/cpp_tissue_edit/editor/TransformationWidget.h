#ifndef SIMPT_EDITOR_TRANSFORMATION_WIDGET_H_INCLUDED
#define SIMPT_EDITOR_TRANSFORMATION_WIDGET_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for TransformationWidget.
 */

#include <QWidget>

class QDoubleSpinBox;
class QSlider;
class QSpinBox;

namespace SimPT_Editor {

/**
 * Qt GUI to enter a transformation of an item
 */
class TransformationWidget: public QWidget
{
	Q_OBJECT
public:
	/**
	 * Constructor
	 *
	 * @param	maxScaleRatio		The maximum ratio that can be scaled
	 * @param	maxTranslationX		The maximum translation in the x direction
	 * @param	maxTranslationY		The maximum translation in the y direction
	 * @param	parent			The widget's parent
	 */
	TransformationWidget(int maxScaleRatio, double maxTranslationX, double maxTranslationY, QWidget *parent = nullptr);

	/**
	 * Destructor
	 */
	virtual ~TransformationWidget();


	/**
	 * Gets the rotation of the transformation
	 *
	 * @return	int	The rotation (in integer degrees)
	 */
	int GetRotation() { return m_rotation; }

	/**
	 * Gets the x scaling of the transformation
	 *
	 * @return	double	The scaling factor in the x direction
	 */
	double GetScalingX() { return m_scaling_x; }

	/**
	 * Gets the y scaling of the transformation
	 *
	 * @return	double	The scaling factor in the y direction
	 */
	double GetScalingY() { return m_aspect_ratio_maintained ? m_scaling_x : m_scaling_y; }

	/**
	 * Gets the x translation of the transformation
	 *
	 * @return	double	The translation distance in the x direction
	 */
	double GetTranslationX() { return m_translation_x; }

	/**
	 * Gets the y translation of the transformation
	 *
	 * @return	double	The translation distance in the y direction
	 */
	double GetTranslationY() { return m_translation_y; }

signals:
	/**
	 * Emitted when the transformation is changed by the user
	 */
	void TransformationChanged();

private slots:
	void UpdateRotation(int degrees);

	void UpdateScalingX(double factor);
	void UpdateScalingX(int value);

	void UpdateScalingY(double factor);
	void UpdateScalingY(int value);

	void UpdateAspectRatioMaintained(bool checked);

	void UpdateTranslationX(double position);
	void UpdateTranslationX(int position);

	void UpdateTranslationY(double position);
	void UpdateTranslationY(int position);

private:
	void SetupGui(int maxScaleRatio, double maxTranslationX, double maxTranslationY);

private:
	int          m_rotation;
	double       m_scaling_x;
	double       m_scaling_y;
	bool         m_aspect_ratio_maintained;
	double       m_translation_x;
	double       m_translation_y;
	bool         m_updating;

private:
	QDoubleSpinBox*      m_scaling_x_spin_box;
	QSlider*             m_scaling_x_slider;
	QDoubleSpinBox*      m_scaling_y_spin_box;
	QSlider*             m_scaling_y_slider;
	QDoubleSpinBox*      m_translation_x_spin_box;
	QSlider*             m_translation_x_slider;
	QDoubleSpinBox*      m_translation_y_spin_box;
	QSlider*             m_translation_y_slider;
};

} // namespace

#endif // end-of-include-guard
