/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for EditableNodeItem
 */

#include "editor/EditableNodeItem.h"

#include "bio/Cell.h"
#include "bio/Wall.h"

#include <QEvent>
#include <QStyleOptionGraphicsItem>

namespace SimPT_Editor {

const QColor EditableNodeItem::DEFAULT_HIGHLIGHT_COLOR(230,0,0,255);//(51, 102, 0, 255)

EditableNodeItem::EditableNodeItem(SimPT_Sim::Node* node, double radius)
		: QGraphicsEllipseItem(-radius, -radius, 2*radius, 2*radius), m_node(node),
		  m_highlight_color(DEFAULT_HIGHLIGHT_COLOR), m_prev_pos((*node)[0], (*node)[1]), m_radius(radius)
{
	setPos((*node)[0], (*node)[1]);
	setFlag(QGraphicsItem::ItemSendsGeometryChanges);
	setAcceptHoverEvents(true);
}

EditableNodeItem::~EditableNodeItem() {}

bool EditableNodeItem::IsAtBoundary() const
{
	return m_node->IsAtBoundary();
}

bool EditableNodeItem::Contains(SimPT_Sim::Node* node) const
{
	return m_node == node;
}

void EditableNodeItem::Highlight(bool highlighted)
{
	QPen pen(this->pen());
	if (highlighted) {
		pen.setWidthF(0.3);
		setZValue(5); //Draw selected node on top of other items.
		setBrush(m_highlight_color);
	}
	else {
		pen.setWidthF(0.1);
		setZValue(4); //Draw nodes on top of other items.
	}
	setPen(pen);
}

void EditableNodeItem::SetHighlightColor(const QColor& color)
{
	m_highlight_color = color;
}

void EditableNodeItem::SetToolTip(std::list<SimPT_Sim::Cell*> cells, std::list<SimPT_Sim::Wall*> walls) {
	QString toolTip;
	toolTip.append(QString("ID: ") + QString::number(m_node->GetIndex()) + "\n\n");
	toolTip.append(QString("fixed: ") + (m_node->IsFixed() ? "true" : "false") + '\n');
	toolTip.append(QString("sam: ") + (m_node->IsFixed() ? "true" : "false") + "\n\n");
	toolTip.append("cells: ");
	for (SimPT_Sim::Cell* c : cells) {
		toolTip.append(QString::number(c->GetIndex()) + ' ');
	}
	toolTip.append('\n');
	toolTip.append("walls: ");
	for (SimPT_Sim::Wall* w : walls) {
		toolTip.append(QString::number(w->GetIndex()) + ' ');
	}
	setToolTip(toolTip);
}

SimPT_Sim::Node* EditableNodeItem::Node() const
{
	return m_node;
}

void EditableNodeItem::Revert()
{
	setPos(m_prev_pos);
	emit Moved();
}

void EditableNodeItem::Update()
{
	setPos((*m_node)[0], (*m_node)[1]);
	emit Moved();
}

QVariant EditableNodeItem::itemChange(GraphicsItemChange change, const QVariant &value)
{
	if (change == QGraphicsItem::ItemPositionChange) {
		m_prev_pos = pos();

		emit Moved();
	}

	return QGraphicsItem::itemChange(change, value);
}

void EditableNodeItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
	if (flags().testFlag(QGraphicsItem::ItemIsSelectable)) {
		setBrush(QBrush(QColor(220,220,220,255)));
		setRect(-m_radius, -m_radius, 2*m_radius, 2*m_radius);
	}
	else {
		setBrush(QBrush(QColor(255,255,255,255)));
		setRect(-0.5, -0.5, 1, 1);
	}
	Highlight(isSelected());

	QStyleOptionGraphicsItem noRectBorder(*option);
	noRectBorder.state &= !QStyle::State_Selected;
	QGraphicsEllipseItem::paint(painter, &noRectBorder, widget);
}

} // namespace
