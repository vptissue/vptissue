/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for PTreePanels
 */

#include "../editor/PTreePanels.h"

#include "bio/Node.h"
#include "bio/Wall.h"
#include "bio/Cell.h"
#include "util/misc/Exception.h"

#include <boost/lexical_cast.hpp>
#include <boost/property_tree/ptree.hpp>
#include <QAction>
#include <QDockWidget>
#include <QMessageBox>

using namespace boost::property_tree;
using namespace SimShell;
using namespace SimShell::Gui;

namespace SimPT_Editor {

PTreePanels::PTreePanels(QWidget* parent)
	: m_attribute_panel(new Gui::PTreeContainer("Attribute panel", parent)),
	  m_attribute_panel_toggled(true),
	  m_geometric_panel(new Gui::PTreeContainer("Geometric panel", parent)),
	  m_geometric_panel_toggled(true),
	  m_parameters_panel(new Gui::PTreeContainer("Parameters panel", parent)),
	  m_parameters_panel_toggled(true)
{
	connect(m_attribute_panel->GetDock()->toggleViewAction(), SIGNAL(toggled(bool)),
							this, SLOT(ToggleAttributePanel(bool)));
	connect(m_geometric_panel->GetDock()->toggleViewAction(), SIGNAL(toggled(bool)),
							this, SLOT(ToggleGeometricPanel(bool)));
	connect(m_parameters_panel->GetDock()->toggleViewAction(), SIGNAL(toggled(bool)),
							this, SLOT(ToggleParametersPanel(bool)));

	connect(m_attribute_panel, SIGNAL(Applied(const boost::property_tree::ptree&)),
			this, SLOT(ApplyAttributePanel(const boost::property_tree::ptree&)));
	connect(m_geometric_panel, SIGNAL(Applied(const boost::property_tree::ptree&)),
			this, SLOT(ApplyGeometricPanel(const boost::property_tree::ptree&)));
	connect(m_parameters_panel, SIGNAL(Applied(const boost::property_tree::ptree&)),
			this, SLOT(ApplyParametersPanel(const boost::property_tree::ptree&)));

	m_geometric_panel->SetOnlyEditData(true);
	m_attribute_panel->SetOnlyEditData(true);
}

PTreePanels::~PTreePanels() {}

void PTreePanels::ApplyAttributePanel(const ptree& pt)
{
	try {
		m_tissue->ChangeAttributes(pt);
	}
	catch (SimPT_Sim::Util::Exception& e) {
		QMessageBox messageBox(m_attribute_panel->GetDock());
		messageBox.critical(m_attribute_panel->GetDock(),
			"Attribute parsing error", "This value isn't allowed for this attribute.");
		UpdateAttributePanel();
	}
}

void PTreePanels::ApplyGeometricPanel(const ptree& pt)
{
	try {
		if (m_tissue->SelectedNodes().size() == 1) {
			if (!m_tissue->MoveNode(m_tissue->SelectedNodes().front(),
					pt.get<double>("x"), pt.get<double>("y"))) {
				UpdateGeometricPanel();
			} else if (m_tissue->SelectedNodes().front()->IsAtBoundary() != pt.get<bool>("boundary")) {
				UpdateGeometricPanel();
			}
		} else {
			UpdateGeometricPanel();
		}
	}
	catch (ptree_bad_data& e) {
		QMessageBox messageBox(m_geometric_panel->GetDock());
		messageBox.critical(m_geometric_panel->GetDock(),
			"Geometry parsing error", "This value isn't allowed for this property.");
		UpdateGeometricPanel();
	}
}

void PTreePanels::ApplyParametersPanel(const ptree& pt)
{
	m_tissue->SetParameters(pt);
}

Gui::PTreeContainer* PTreePanels::AttributePanel() const
{
	return m_attribute_panel;
}

void PTreePanels::Initialize(std::shared_ptr<EditControlLogic> tissue)
{
	m_tissue = tissue;
	connect(m_tissue.get(), SIGNAL(SelectionChanged()), this, SLOT(UpdatePanels()));
	connect(m_tissue.get(), SIGNAL(StoppedMoving()), this, SLOT(UpdateGeometricPanel()));
}

Gui::PTreeContainer* PTreePanels::GeometricPanel() const
{
	return m_geometric_panel;
}

ptree PTreePanels::MergeJoin(ptree pt1, ptree pt2)
{
	ptree pt;
	if (pt1.data() != pt2.data()) {
		pt = ptree("?");
	} else {
		pt = ptree(pt1.data());
	}

	if (pt1.size() == 0) {
		return pt;
	} else {
		ptree::const_iterator it1 = pt1.begin();
		ptree::const_iterator it2 = pt2.begin();
		while (it1 != pt1.end() && it2 != pt2.end()) {
			if (it1->first == it2->first) {
				pt.add_child(it1->first, MergeJoin(it1->second, it2->second));
				++it1;
				++it2;
			}
			else if (it1->first < it2->first) {
				pt.add_child(it1->first, SetDataToUnknown(it1->second));
				++it1;
			}
			else { //it1->first > it2->first
				pt.add_child(it2->first, SetDataToUnknown(it2->second));
				++it2;
			}
		}
		for (ptree::const_iterator it = it1; it != pt1.end(); ++it) {
			pt.add_child(it->first, SetDataToUnknown(it->second));
		}
		for (ptree::const_iterator it = it2; it != pt2.end(); ++it) {
			pt.add_child(it->first, SetDataToUnknown(it->second));
		}

		return pt;
	}
}

Gui::PTreeContainer* PTreePanels::ParametersPanel() const
{
	return m_parameters_panel;
}

ptree PTreePanels::SetDataToUnknown(const ptree& pt)
{
	ptree new_pt("?");
	for (const auto& v : pt) {
		new_pt.add_child(v.first, SetDataToUnknown(v.second));
	}
	return new_pt;
}

void PTreePanels::ToggleAttributePanel(bool toggled)
{
	m_attribute_panel_toggled = toggled;
	UpdateAttributePanel();
}

void PTreePanels::ToggleGeometricPanel(bool toggled)
{
	m_geometric_panel_toggled = toggled;
	UpdateGeometricPanel();
}

void PTreePanels::ToggleParametersPanel(bool toggled)
{
	m_parameters_panel_toggled = toggled;
	UpdateParametersPanel();
}

void PTreePanels::UpdateAttributePanel()
{
	if (!m_attribute_panel_toggled) {
		return;
	}

	std::string title = "Attribute panel";
	if (m_tissue->SelectedNodes().size() > 0) {
		auto nodes = m_tissue->SelectedNodes();
		ptree pt = nodes.front()->ToPtree();
		for (auto it = std::next(nodes.begin()); it != nodes.end(); ++it) {
			pt = MergeJoin(pt, (*it)->ToPtree());
		}
		pt.erase("id");
		pt.erase("x");
		pt.erase("y");
		pt.erase("boundary");
		m_attribute_panel->Open(pt.get_child("attributes"));

		if (nodes.size() == 1) {
			title += "   -   node "
				+ boost::lexical_cast<std::string>(nodes.front()->GetIndex());
		} else {
			title += "   -   nodes";
		}
	} else if (!m_tissue->SelectedWalls().empty()) {
		auto walls = m_tissue->SelectedWalls();
		ptree pt = walls.front()->WallAttributes::ToPtree();
		for (auto it = std::next(walls.begin()); it != walls.end(); ++it) {
			pt = MergeJoin(pt, (*it)->WallAttributes::ToPtree());
		}
		m_attribute_panel->Open(pt);

		if (walls.size() == 1) {
			title += "   -   wall "
				+ boost::lexical_cast<std::string>(walls.front()->GetIndex());
		} else {
			title += "   -   walls";
		}
	} else if (m_tissue->SelectedCells().size() > 0) {
		auto cells = m_tissue->SelectedCells();
		ptree pt = cells.front()->CellAttributes::ToPtree();
		//pt.sort();
		for (auto it = std::next(cells.begin()); it != cells.end(); ++it) {
			pt = MergeJoin(pt, (*it)->CellAttributes::ToPtree());
		}
		m_attribute_panel->Open(pt);

		if (cells.size() == 1) {
			title += "   -   cell "
				+ boost::lexical_cast<std::string>(cells.front()->GetIndex());
		} else {
			title += "   -   cells";
		}
	} else {
		m_attribute_panel->Open(m_tissue->ToPTree().get_child("mesh"));
	}

	m_attribute_panel->GetDock()->setWindowTitle(QString::fromStdString(title));
}

void PTreePanels::UpdateGeometricPanel()
{
	if (!m_geometric_panel_toggled) {
		return;
	}

	std::string title = "Geometric panel";
	if (m_tissue->SelectedNodes().size() > 0) {
		auto nodes = m_tissue->SelectedNodes();

		ptree pt;
		pt.add("x", (*nodes.front())[0]);
		pt.add("y", (*nodes.front())[1]);
		pt.add("boundary", nodes.front()->IsAtBoundary());
		for (auto it = std::next(nodes.begin()); it != nodes.end(); ++it) {
			ptree node_pt = (*it)->ToPtree();
			if (pt.get<std::string>("x") != "?") {
				if (pt.get<double>("x") != node_pt.get<double>("x")) {
					pt.put("x", "?");
				}
			}
			if (pt.get<std::string>("y") != "?") {
				if (pt.get<double>("y") != node_pt.get<double>("y")) {
					pt.put("y", "?");
				}
			}
			if (pt.get<std::string>("boundary") != "?") {
				if (pt.get<bool>("boundary") != node_pt.get<bool>("boundary")) {
					pt.put("boundary", "?");
				}
			}
		}
		m_geometric_panel->Open(pt);

		if (nodes.size() == 1) {
			title += "   -   node " + boost::lexical_cast<std::string>(nodes.front()->GetIndex());
		} else {
			title += "   -   nodes";
		}
	} else if (m_tissue->SelectedWalls().size() > 0) {
		auto walls = m_tissue->SelectedWalls();

		ptree pt = walls.front()->ToPtree();
		//pt.sort();
		for (auto it = std::next(walls.begin()); it != walls.end(); ++it) {
			pt = MergeJoin(pt, (*it)->ToPtree());
		}
		pt.erase("id");
		pt.erase("attributes");
		m_geometric_panel->Open(pt);

		if (walls.size() == 1) {
			title += "   -   wall " + boost::lexical_cast<std::string>(walls.front()->GetIndex());
		}
		else {
			title += "   -   walls";
		}
	} else if (m_tissue->SelectedCells().size() > 0) {
		auto cells = m_tissue->SelectedCells();

		ptree pt = cells.front()->GeometryToPtree();
		for (auto it = std::next(cells.begin()); it != cells.end(); ++it) {
			pt = MergeJoin(pt, (*it)->GeometryToPtree());
		}
		pt.erase("id");
		m_geometric_panel->Open(pt);

		if (cells.size() == 1) {
			title += "   -   cell " + boost::lexical_cast<std::string>(cells.front()->GetIndex());
		}
		else {
			title += "   -   cells";
		}
	} else {
		m_geometric_panel->Open(m_tissue->ToPTree().get_child("mesh"));
	}

	m_geometric_panel->GetDock()->setWindowTitle(QString::fromStdString(title));
}

void PTreePanels::UpdateParametersPanel()
{
	m_parameters_panel->Open(m_tissue->GetParameters());
}

void PTreePanels::UpdatePanels()
{
	UpdateAttributePanel();
	UpdateGeometricPanel();
}

} // namespace
