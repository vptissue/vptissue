#ifndef SIMPT_EDITOR_EDIT_CONTROL_LOGIC_H_INCLUDED
#define SIMPT_EDITOR_EDIT_CONTROL_LOGIC_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for LeafControlLogic
 */

#include "EditableMesh.h"

#include "bio/Cell.h"
#include "bio/Edge.h"
#include "bio/Mesh.h"
#include "bio/Node.h"
#include "bio/Wall.h"

#include <QObject>
#include <boost/property_tree/ptree_fwd.hpp>
#include <list>
#include <memory>
#include <string>
#include <vector>

class QPointF;
class QPolygonF;

namespace SimPT_Editor {

using namespace SimPT_Sim;

/**
 * Class for the composition of a tissue, containing its properties.
 */
class EditControlLogic : public QObject
{
	Q_OBJECT
public:
	/**
	 * Constructs a default tissue, which is an upward triangle.
	 * @param	chemical_count		The chemical count of the mesh.
	 */
	EditControlLogic(unsigned int chemical_count);

	/**
	 * Constructs a tissue from a ptree.
	 * @param	pt			Ptree containing parameters and mesh subtrees.
	 * @throws	ptree_error		When ptree doesn't contain a valid tissue.
	 */
	EditControlLogic(const boost::property_tree::ptree& pt);

	/**
	 * Destructor.
	 */
	virtual ~EditControlLogic();

	/**
	 * Change the attributes of the selected items to the attributes in the given ptree.
	 * The values that should not be changed, should be marked with a '?'.
	 */
	void ChangeAttributes(const boost::property_tree::ptree& pt);

	/// Copy the given attributes to the selected cells.
	void CopyAttributes(const boost::property_tree::ptree& pt);

	/// See EditableMesh::CreateCell for more information.
	Cell* CreateCell(Node* node1, Node* node2, const QPointF& newPoint);

	/**
	 * Delete the given cell in the mesh.
	 * A cell can only be deleted if it's at the boundary of the mesh.
	 * @param	cell		The given cell.
	 * @return	True if the cell can be deleted.
	 */
	bool DeleteCell(Cell* cell);

	/**
	 * Delete the given cells in the mesh.
	 * The mesh should stay consistent, so use this function with caution.
	 * @param	cells		The cells that will be deleted.
	 */
	void DeleteCells(const std::list<Cell*>& cells);

	/**
	 * Delete the given node in the mesh. A node can only be deleted if it is part of
	 * cells with more than three corners and when it only is part of two edges.
	 * @param	node		The given node.
	 * @return	True if the node can be deleted.
	 */
	bool DeleteNode(Node* node);

	/// Deselect the selected items.
	void Deselect();

	/// Get all cells.
	const std::vector<Cell*>& GetCells() const {return m_mesh->GetMesh()->GetCells();}

	/// Get the mesh
	EditableMesh* GetMesh() {return m_mesh;}

	// Get parameters ptree.
	const boost::property_tree::ptree& GetParameters() const;

	/// Get all nodes.
	const std::vector<Node*>& GetNodes() const {return m_mesh->GetMesh()->GetNodes();}

	/// Get all walls
	const std::vector<Wall*>& GetWalls() const {return m_mesh->GetMesh()->GetWalls();}

	/// See EditableMesh::DisplaceCell for more information.
	bool MoveNode(Node* node, double x, double y);

	/// Get the PTree associated with the mesh.
	const boost::property_tree::ptree& ToPTree();

	/// See EditableMesh::ReplaceCell for more information.
	void ReplaceCell(Cell* cell, std::list<QPolygonF> newCells);

	/// Get the selected cells.
	const std::list<Cell*>& SelectedCells() const {return m_selected_cells;}

	/// Get the selected nodes.
	const std::list<Node*>& SelectedNodes() const {return m_selected_nodes;}

	/// Get the selected walls.
	const std::list<Wall*>& SelectedWalls() const {return m_selected_walls;}

	/// Select the given cells.
	void SelectCells(const std::list<Cell*>& cells);

	/// Select the given edges.
	void SelectEdges(const std::list<Edge>& edges);

	/// Select the given nodes.
	void SelectNodes(const std::list<Node*>& nodes);

	/// Set parameters ptree.
	void SetParameters(const boost::property_tree::ptree&);

	/// Stop moving a node after moving it.
	void StopMoveNode();

	/// See EditableMesh::SplitCell for more information.
	std::vector<Cell*> SplitCell(Cell* cell,
						Node* node1, Node* node2, bool modified = false);

	/// See EditableMesh::SplitCell for more information.
	std::vector<Cell*> SplitCell(Node* node1, Node* node2);

	/**
	 * Split a given edge at the center.
	 * @param	edge		The given edge.
	 * @return	The newly created node at the middle of the edge.
	 */
	Node* SplitEdge(const Edge& edge);

signals:
	/// Emitted when data has been modified.
	void Modified();

	/**
	 * Emitted when the given node has been moved to the given coordinates.
	 * @param	node		The given node
	 * @param	x		The x-coordinate.
	 * @param	y		The y-coordinate.
	 */
	void Moved(Node* node, double x, double y);

	/// Emitted when selection has changed.
	void SelectionChanged();

	/**
	 * Emitted when certain info about the logic has changed.
	 * This includes the IDs of the selected items.
	 * @param	info		The information in string form.
	 */
	void StatusInfoChanged(const std::string& info);

	/// Emitted when data has been modified.
	void StoppedMoving();

private:
	/// Build mesh used to initialise NewLeaf tissues.
	Mesh BuildTwoSquaresMesh(unsigned int chemical_count);

	/**
	 * Update the first ptree with the second ptree.
	 * Each tree should have the exact same structure.
	 * When the data of second ptree is equal to a '?', the data of the first one will be kept, otherwise it'll be overwritten.
	 *
	 * @param	pt1			The first ptree (the ptree that will be updated).
	 * @param	pt2			The second ptree.
	 * @return	The updated version of the first ptree.
	 */
	static boost::property_tree::ptree UpdatePTree(boost::property_tree::ptree pt1, boost::property_tree::ptree pt2);

private:
	EditableMesh*                  m_mesh;
	bool                           m_moving_node;
	boost::property_tree::ptree    m_ptree;
	bool                           m_ptree_dirty;
	std::list<Cell*>               m_selected_cells;
	std::list<Node*>               m_selected_nodes;
	std::list<Wall*>               m_selected_walls;

};

} // namespace

#endif // end_of_include_guard
