/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for BackgroundDialog.
 */

#include "BackgroundDialog.h"

#include "editor/TransformationWidget.h"

#include <cmath>
#include <QBoxLayout>
#include <QCheckBox>
#include <QDir>
#include <QFileDialog>
#include <QGraphicsPixmapItem>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QString>

class QWidget;

namespace SimPT_Editor {

BackgroundDialog::BackgroundDialog(QGraphicsPixmapItem *item, QWidget *parent)
	: QDialog(parent), m_item(item)
{
	SetupGui();
}

BackgroundDialog::~BackgroundDialog()
{
}

void BackgroundDialog::BrowseImage()
{
	QString lastFile = m_file_path->text().isEmpty() ? QDir::homePath() : QFileInfo(m_file_path->text()).absolutePath();
	//Native dialog has been disabled, due to a bug when an item is selected.
	QString filePath = QFileDialog::getOpenFileName(this, "Select the background image file to load", lastFile, "Images (*.png *.xpm *.jpg *.bmp);;All files (*.*)", nullptr, QFileDialog::DontUseNativeDialog | QFileDialog::ReadOnly);

	if (!filePath.isEmpty() && filePath != m_file_path->text()) {
		if (m_pixmap.load(filePath)) {
			m_item->setPixmap(m_pixmap);
			UpdateTransformation();
			m_file_path->setText(filePath);
			m_visible->setEnabled(true);
			m_visible->setChecked(true);
			m_transformation->setEnabled(true);
		} else {
			QMessageBox::critical(this, "Error", "Couldn't load the image", QMessageBox::Ok);
			m_visible->setEnabled(false);
			m_transformation->setEnabled(false);
		}
	}
}

void BackgroundDialog::UpdateVisibility(bool visible)
{
	m_item->setVisible(visible);
	m_transformation->setEnabled(visible);
}

void BackgroundDialog::UpdateTransformation()
{
	QTransform transformation;

	QPointF center = m_item->boundingRect().center();

	transformation.translate(m_transformation->GetTranslationX(), m_transformation->GetTranslationY());
	transformation.rotate(m_transformation->GetRotation());
	transformation.scale(m_transformation->GetScalingX(), m_transformation->GetScalingY());

	transformation.translate(-center.x(), -center.y());

	m_item->setTransform(transformation);
}

void BackgroundDialog::SetupGui()
{
	setWindowTitle("Set background image");
	setMinimumWidth(350);

	// > GUI layout
	QVBoxLayout *layout = new QVBoxLayout();

	// 	> File selection
	QHBoxLayout *fileLayout = new QHBoxLayout();

	m_file_path = new QLineEdit();
	m_file_path->setReadOnly(true);
	fileLayout->addWidget(m_file_path);

	QPushButton *browseButton = new QPushButton("Browse...");
	connect(browseButton, SIGNAL(clicked()), this, SLOT(BrowseImage()));
	fileLayout->addWidget(browseButton);

	layout->addLayout(fileLayout);
	// 	< File selection

	// 	> Image visibility
	m_visible = new QCheckBox("Display image");
	m_visible->setEnabled(false);
	m_visible->setChecked(false);
	connect(m_visible, SIGNAL(toggled(bool)), this, SLOT(UpdateVisibility(bool)));
	layout->addWidget(m_visible);
	// 	< Image visibility

	// 	> Transformations
	m_transformation = new TransformationWidget(10, 3 * std::max(m_item->boundingRect().width(), 200.0), 3 * std::max(m_item->boundingRect().height(), 200.0));
	m_transformation->setEnabled(false);
	connect(m_transformation, SIGNAL(TransformationChanged()), this, SLOT(UpdateTransformation()));
	layout->addWidget(m_transformation);
	//	< Transformations

	setLayout(layout);  // < GUI layout
}

} // namespace
