#ifndef SIMPT_EDITOR_UNDO_STACK_H_INCLUDED
#define SIMPT_EDITOR_UNDO_STACK_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for UndoStack.
 */

#include "EditControlLogic.h"

#include <boost/property_tree/ptree.hpp>

namespace SimPT_Editor {

/**
 * Undo Stack for actions performed on a tissue.
 */
class UndoStack
{
public:
	/**
	 * Constructor.
	 */
	UndoStack();

	/**
	 * Destructor.
	 */
	virtual ~UndoStack();

	/**
	 * Initialize the undo stack with a given tissue.
	 *
	 * @param	tissue		The given tissue.
	 */
	void Initialize(const boost::property_tree::ptree& tissue);

	/**
	 * Push a tissue to the stack.
	 *
	 * @param	tissue		The given tissue.
	 */
	void Push(const boost::property_tree::ptree& tissue);

	/**
	 * True if the current action can be undone.
	 */
	bool CanUndo() const;

	/**
	 * True if the current action can be redone.
	 */
	bool CanRedo() const;

	/**
	 * Undo an action.
	 *
	 * @return	The previous tissue.
	 */
	const boost::property_tree::ptree& Undo();

	/**
	 * Redo an action.
	 *
	 * @return	The next tissue.
	 */
	const boost::property_tree::ptree& Redo();

private:
	std::list<boost::property_tree::ptree> m_stack;
	std::list<boost::property_tree::ptree>::iterator m_current;

private:
	const unsigned int m_max_tissues = 25;
};

} // namespace

#endif // end_of_include_guard
