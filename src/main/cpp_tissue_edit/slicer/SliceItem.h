#ifndef SIMPT_EDITOR_SLICE_ITEM_H_INCLUDED
#define SIMPT_EDITOR_SLICE_ITEM_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for SliceItem.
 */

#include "editor/EditControlLogic.h"

#include <QGraphicsPolygonItem>
#include <QGraphicsScene>
#include <QObject>

namespace SimPT_Editor {

/**
 * Represents a slice of a cell complex.
 */
class SliceItem : public QObject, public QGraphicsPolygonItem
{
	Q_OBJECT
public:
	/// Constructor.
	SliceItem(const QPolygonF& polygon, const QLineF& cut, std::shared_ptr<EditControlLogic> tissue, QGraphicsScene* scene);

	/// Destructor.
	virtual ~SliceItem();

signals:
	void Truncated();

private:
	virtual QVariant itemChange(GraphicsItemChange change, const QVariant& value);

	virtual void hoverEnterEvent(QGraphicsSceneHoverEvent* event);

	virtual void hoverLeaveEvent(QGraphicsSceneHoverEvent* event);

	void TruncateLeaf();

private:
	QLineF                               m_cut;
	std::shared_ptr<EditControlLogic>    m_tissue;
	QGraphicsScene*                      m_scene;
private:
	static constexpr double g_accuracy = 1.0e-12;
};

} // namespace

#endif // end_of_include_guard
