/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for HexagonalTile.
 */

#include "HexagonalTile.h"

#include <cmath>

namespace SimPT_Editor {

const double HexagonalTile::g_side_length = 5.0;
const double HexagonalTile::g_half_height = std::sqrt(3.0) * g_side_length / 2;
const QPolygonF HexagonalTile::g_start_polygon(QPolygonF() << QPointF(0, 0) << QPointF(g_side_length / 2, g_half_height) << QPointF(3 * g_side_length / 2, g_half_height) << QPointF(2 * g_side_length, 0) << QPointF(3 * g_side_length / 2, -g_half_height) << QPointF(g_side_length / 2, -g_half_height));

namespace {

QPainterPath PolygonToPath(const QPolygonF &polygon)
{
	QPainterPath path;
	path.moveTo(polygon.first());
	path.lineTo(polygon.at(1));
	path.lineTo(polygon.at(2));
	path.lineTo(polygon.at(3));
	return path;
}

}

HexagonalTile::HexagonalTile()
	: HexagonalTile(g_start_polygon, nullptr)
{
}

HexagonalTile::~HexagonalTile()
{
}

HexagonalTile *HexagonalTile::Left() const
{
	return new HexagonalTile(m_polygon.translated(-3 * g_side_length, 0), parentItem());
}

HexagonalTile *HexagonalTile::Right() const
{
	return new HexagonalTile(m_polygon.translated(3 * g_side_length, 0), parentItem());
}

HexagonalTile *HexagonalTile::Up() const
{
	return new HexagonalTile(m_polygon.translated(-3 * g_side_length / 2, -g_half_height), parentItem());
}

HexagonalTile *HexagonalTile::Down() const
{
	return new HexagonalTile(m_polygon.translated(3 * g_side_length / 2, g_half_height), parentItem());
}

HexagonalTile::HexagonalTile(const QPolygonF &polygon, QGraphicsItem *parentItem)
	: Tile(polygon, PolygonToPath(polygon))
{
	setParentItem(parentItem);
}

} // namespace
