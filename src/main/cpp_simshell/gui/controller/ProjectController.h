#ifndef GUI_PROJECTCONTROLLER_H_INCLUDED
#define GUI_PROJECTCONTROLLER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * ProjectController header.
 */

#include "SessionController.h"
#include "gui/project_actions/ExportActions.h"
#include "gui/project_actions/ViewerActions.h"
#include "gui/EnabledActions.h"
#include "gui/HasUnsavedChangesPrompt.h"
#include "gui/IHasPTreeState.h"
#include "workspace/IProject.h"

#include <memory>
#include <QWidget>

class QDockWidget;

namespace SimShell {
namespace Gui {

class PTreeContainer;
class PTreeContainerPreferencesObserver;

namespace Controller {

class AppController;

/**
 * Project controller.
 * Simple state machine that has 2 states: "opened" and "closed".
 * In opened state, it keeps a pointer to a project in the workspace and
 * becomes owner of menus, actions and viewers concerning the opened project.
 * The "opened" state also has a sub-state machine called RunningController.
 */
class ProjectController : public QWidget,
                          public IHasPTreeState,
                          public HasUnsavedChangesPrompt,
                          public SimPT_Sim::ClockMan::Timeable<>
{
	Q_OBJECT
public:
	ProjectController(AppController* m);

	QAction* GetActionClose() const;

	QMenu* GetExportMenu() const;

	const std::string& GetOpenedFileName() const;

	const std::string& GetOpenedProjectName() const;

	QDockWidget* GetParametersDock() const;

	QMenu* GetParametersMenu() const;

	QDockWidget* GetPreferencesDock() const;

	QMenu* GetPreferencesMenu() const;

	/// @see IHasPTreeState
	boost::property_tree::ptree GetPTreeState() const;

	QMenu* GetSimulationMenu() const;

	virtual Timings GetTimings() const;

	QMenu* GetViewersMenu() const;

	/// @see HasUnsavedChanges
	virtual bool IsOpened() const;

	/// @return Whether simulation is running.
	bool IsRunning() const;

	bool Set(const std::string& project_name, const std::shared_ptr<Ws::IProject>& project);

	/// @see IHasPTreeState
	void SetPTreeState(const boost::property_tree::ptree&);

	/// Set simulation running.
	void SetRunning(bool);

	/// @return whether we are in opened state
	operator bool() const;

protected:
	/// @see HasUnsavedChanges
	virtual void InternalForceClose();

	/// @see HasUnsavedChanges
	virtual bool InternalIsClean() const;

	/// @see HasUnsavedChanges
	virtual void InternalPreForceClose();

	/// @see HasUnsavedChanges
	virtual bool InternalSave();

private slots:
	void SLOT_ApplyParameters(const boost::property_tree::ptree&);
	void SLOT_ApplyPreferences(const boost::property_tree::ptree&);
	void SLOT_Close();

private:
	void InitActions();
	void InitBaseClasses();
	void InitWidgets();

private:
	AppController*                                      m_main_controller;

	// Timings of all simulations
	Timings                                             m_timings;

	// These shared ptrs will only contain objects if we are in opened state.
	std::string                                         m_project_name;
	std::string                                         m_file_name;
	std::shared_ptr<Ws::IProject>                       m_project;
	std::shared_ptr<Viewer::IViewerNode>                m_root_viewer;
	std::shared_ptr<ProjectActions::ExportActions>      m_export_actions;
	std::shared_ptr<ProjectActions::ViewerActions>      m_viewer_actions;

	// Widgets
	std::shared_ptr<PTreeContainer>                     m_container_parameters;
	std::shared_ptr<PTreeContainerPreferencesObserver>  m_container_preferences;

	// Actions and menus
	QAction*                                            m_a_close_project;
	QMenu*                                              m_m_export;
	QMenu*                                              m_m_viewers;
	QMenu*                                              m_m_simulation;

	// Sub-state
	SessionController                                   m_running_controller;

	EnabledActions                                      m_enabled_actions; ///< Actions to be enabled when we enter 'opened' state, disabled when entering 'closed' state.
};

} // namespace
} // namespace
} // namespace

#endif // end_of_inclde_guard
