#ifndef PTREE_MENU_H_INCLUDED
#define PTREE_MENU_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for PTreeMenu.
 */

#include "gui/HasUnsavedChanges.h"

#include <boost/property_tree/ptree.hpp>
#include <QMenu>

class QAction;
class QSignalMapper;

namespace SimShell {
namespace Gui {

/**
 * A menu reflecting the structure of a ptree.
 *
 * Given a ptree, this class constructs a hierarchical menu reflecting the ptree structure.
 * Each subtree can be selected, causing the menu to emit the ItemTriggered() signal.
 */
class PTreeMenu : public QMenu, public HasUnsavedChanges
{
	Q_OBJECT
public:
	/**
	 * @param title   Menu title. Will also be used as dock widget title.
	 * @param parent  Parent widget.
	 */
	PTreeMenu(QString const& title, QString const& path_prefix = QString(), QWidget* parent = 0);

	/// Destructor does not do a thing, except be virtual.
	virtual ~PTreeMenu();

	/// Test whether menu is in opened state.
	virtual bool IsOpened() const;

	/**
	 * Open a ptree. Menu will reflect hierarchical structure of the ptree.
	 * @param pt     PTree to display in menu.
	 * @param depth  Maximum depth of submenu hierarchy.
	 *               If depth = -1, there is no maximum depth.
	 */
	bool OpenPTree(boost::property_tree::ptree const& pt, int depth = -1);

signals:
	void ItemTriggered(QString const& subtree = QString());

protected:
	/// @see IHasUnsavedChanges
	virtual void InternalForceClose();

	/// @see IHasUnsavedChanges
	virtual bool InternalIsClean() const;

	/// @see IHasUnsavedChanges
	virtual bool InternalSave();

private:
	///
	void AddAllAction();

	/// Recursive implementation of OpenPTree().
	void OpenPTreeInternal(boost::property_tree::ptree const& pt, int remaining_depth);

private:
	bool               m_opened;
	QAction*           m_action_all;        // the "All..."-action
	QString            m_path;
	QSignalMapper*     m_signal_mapper;

};

} // namespace
} // namespace

#endif // end_of_inclde_guard
