#ifndef WORKSPACE_WIZARD_H_INCLUDED
#define WORKSPACE_WIZARD_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for WorkspaceWizard.
 */

#include <QWizard>
#include <QFileDialog>
#include <QListWidget>
#include <QComboBox>
#include <QLabel>
#include <QCheckBox>
#include <memory>

namespace SimShell {

namespace Ws {
	class IWorkspaceFactory;
}

namespace Gui {

/**
 * A wizard that assists the user either specifying an existing workspace,
 * or creating a new workspace.
 */
class WorkspaceWizard : public QWizard
{
	Q_OBJECT
public:
	WorkspaceWizard(const std::shared_ptr<Ws::IWorkspaceFactory>&, QWidget * parent = 0);

	/**
	 * After WorkspaceWizard::exec() returns QDialog::Accepted,
	 * this function will return workspace directory selected by user.
	 * @return Path to workspace selected by user.
	 */
	std::string GetWorkspaceDir() const;

private:
	class PathPage;
	class ExistingPage;
	class InitPage;
	class DonePage;

	enum { Page_Path, Page_Existing, Page_Init, Page_Done };

	std::shared_ptr<Ws::IWorkspaceFactory>  m_workspace_factory;
};

class WorkspaceWizard::PathPage : public QWizardPage
{
	Q_OBJECT
public:
	PathPage(const std::shared_ptr<Ws::IWorkspaceFactory>&);
	virtual ~PathPage() {}

	virtual int nextId() const;

private slots:
	void showBrowseDialog();
	void pathSelected(const QString &);

private:
	QFileDialog* dialog;

	std::shared_ptr<Ws::IWorkspaceFactory>  m_workspace_factory;

};

/// Page shown when user specified an existing workspace
class WorkspaceWizard::ExistingPage : public QWizardPage
{
	Q_OBJECT
public:
	ExistingPage(const std::shared_ptr<Ws::IWorkspaceFactory>&);
	virtual ~ExistingPage() {}

	virtual void initializePage();
	virtual int nextId() const;

private:
	QListWidget* projects;

	std::shared_ptr<Ws::IWorkspaceFactory>  m_workspace_factory;
};

/// Page shown when user specified a path that doesn't contain a workspace already
class WorkspaceWizard::InitPage : public QWizardPage
{
	Q_OBJECT
public:
	InitPage(const std::shared_ptr<Ws::IWorkspaceFactory>&);
	virtual ~InitPage() {}

	virtual void initializePage();
	virtual int nextId() const;

protected:
	virtual bool validatePage();

private:
	QLabel* directory_label;

	std::shared_ptr<Ws::IWorkspaceFactory>  m_workspace_factory;
};

class WorkspaceWizard::DonePage : public QWizardPage
{
	Q_OBJECT
public:
	DonePage(const std::shared_ptr<Ws::IWorkspaceFactory>&);
	virtual ~DonePage() {}

	virtual void initializePage();
	virtual int nextId() const;

signals:
	void done(std::string const & workspace_dir);

protected:
	virtual bool validatePage();

private:
	QCheckBox* check_default;

	std::shared_ptr<Ws::IWorkspaceFactory>  m_workspace_factory;
};

} // namespace
} // namespace

#endif // end_of_inclde_guard
