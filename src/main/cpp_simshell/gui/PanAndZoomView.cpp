/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for PanAndZoomView
 */

#include "PanAndZoomView.h"

#include <QApplication>
#include <QEvent>
#include <QKeyEvent>
#include <QWheelEvent>

#include <cmath>

namespace SimShell {
namespace Gui {

PanAndZoomView::PanAndZoomView(double zoomMin, double zoomMax, QWidget *parent)
	: QGraphicsView(parent), m_zoom_min(zoomMin), m_zoom_max(zoomMax)
{
	setTransformationAnchor(QGraphicsView::AnchorViewCenter);
	setResizeAnchor(QGraphicsView::AnchorViewCenter);
}

PanAndZoomView::~PanAndZoomView()
{
}

void PanAndZoomView::ScaleView(double factor)
{
	double zoomX = transform().m11() * factor;
	double zoomY = transform().m22() * factor;

	if ((zoomX >= m_zoom_min) && (zoomY >= m_zoom_min) && (zoomX <= m_zoom_max) && (zoomY <= m_zoom_max)) {
		scale(factor, factor);
	}
}

void PanAndZoomView::ResetZoom() {
	scale(1 / transform().m11(), 1 / transform().m22());
}

void PanAndZoomView::keyPressEvent(QKeyEvent *event) {
	if (event->key() == Qt::Key_Control) {
		setDragMode(QGraphicsView::ScrollHandDrag);
	}

	QGraphicsView::keyPressEvent(event);
}

void PanAndZoomView::keyReleaseEvent(QKeyEvent *event) {
	if (event->key() == Qt::Key_Control) {
		setDragMode(QGraphicsView::NoDrag);
	}

	QGraphicsView::keyReleaseEvent(event);
}


void PanAndZoomView::enterEvent(QEvent *event) {
	if (QApplication::keyboardModifiers().testFlag(Qt::ControlModifier)) {
		setDragMode(QGraphicsView::ScrollHandDrag);
	}
	else {
		setDragMode(QGraphicsView::NoDrag);
	}

	setFocus();

	QGraphicsView::enterEvent(event);
}

double PanAndZoomView::Scaling()
{
	return transform().m11();
}

void PanAndZoomView::wheelEvent(QWheelEvent *event)
{
	setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
	ScaleView(std::pow(2, -event->delta() / 240.0));
	setTransformationAnchor(QGraphicsView::AnchorViewCenter);
}

} // namespace Gui
} // namespace SimShell
