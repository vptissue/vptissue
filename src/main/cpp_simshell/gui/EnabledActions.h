#ifndef GUI_ENABLEDACTIONS_H_INCLUDED
#define GUI_ENABLEDACTIONS_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * EnabledActions interface.
 */

#include <vector>

class QAction;
class QMenu;

namespace SimShell {
namespace Gui {

/**
 * Helper class to enable or disable a collection of QAction and QMenu objects at once.
 */
class EnabledActions
{
public:
	/// Add action object to collection.
	void Add(QAction* a);

	/// Add menu to collection.
	void Add(QMenu* m);

	/// Set action object collection.
	void Set(std::vector<QAction*> actions);

	/// Enable all actions and menus in collection.
	void Enable();

	/// Disable all actions and menus in collection.
	void Disable();

private:
	std::vector<QAction*>  actions;
	std::vector<QMenu*>    menus;
};

} // namespace
} // namespace

#endif // end_of_inclde_guard
