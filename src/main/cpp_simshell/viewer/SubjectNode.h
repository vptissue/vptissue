#ifndef VIEWER_SUBJECTNODE_H_INCLUDED
#define VIEWER_SUBJECTNODE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for SubjectNode.
 */

#include "IViewerNode.h"
#include "workspace/MergedPreferences.h"

#include <iostream>
#include <type_traits>

namespace SimShell {
namespace Viewer {

/**
 * A viewer node that does not represent a viewer at all.
 * It doesn't create and delete a viewer when being enabled/disabled.
 * It owns a subject (in the observer pattern) with which its children are registered.
 * Usually it serves as a base type for the root of the tree of viewers.
 *
 * Template parameters:
 * 	- SubjectType: The type of the subject this node owns and registers its children nodes with.
 */
template <class SubjectType>
class SubjectNode : public IViewerNode
{
public:
	SubjectNode(std::shared_ptr<Ws::MergedPreferences> p,
	            std::shared_ptr<SubjectType> s,
	            IViewerNode::ChildrenMap&& c);

	SubjectNode(std::shared_ptr<Ws::MergedPreferences> p,
	            std::shared_ptr<SubjectType> s);

	virtual ~SubjectNode() {}

	virtual void Disable();

	virtual void Enable();

	virtual bool IsEnabled() const;

	virtual bool IsParentEnabled() const;

	virtual IViewerNode::ChildrenMap::const_iterator begin() const;

	virtual IViewerNode::ChildrenMap::const_iterator end() const;

private:
	std::shared_ptr<Ws::MergedPreferences>        m_preferences;
	std::shared_ptr<SubjectType>                  m_subject;
	IViewerNode::ChildrenMap                      m_children;
	bool                                          m_enabled;
};

template <class FakeSubjectType>
class SubjectViewerNodeWrapper : public IViewerNodeWithParent<FakeSubjectType>
{
public:
	SubjectViewerNodeWrapper(std::shared_ptr<IViewerNode> node)
		: m_node(node) {}

	virtual ~SubjectViewerNodeWrapper() {}

	virtual void Disable() { m_node->Disable(); }

	virtual void Enable() { m_node->Enable(); }

	virtual bool IsEnabled() const { return m_node->IsEnabled(); }

	virtual bool IsParentEnabled() const { return m_node->IsParentEnabled(); }

	virtual IViewerNode::ChildrenMap::const_iterator begin() const { return m_node->begin(); }

	virtual IViewerNode::ChildrenMap::const_iterator end() const { return m_node->end(); }

protected:
	/// @see IViewerNodeWithParent
	virtual void ParentDisabled() {}

	/// @see IViewerNodeWithParent
	virtual void ParentEnabled(std::shared_ptr<FakeSubjectType>) {}

private:
	std::shared_ptr<IViewerNode> m_node;
};


template <class SubjectType>
SubjectNode<SubjectType>::SubjectNode(std::shared_ptr<Ws::MergedPreferences> p,
                                      std::shared_ptr<SubjectType> s,
                                      IViewerNode::ChildrenMap&& c)
	: m_preferences(p),
	  m_subject(s),
	  m_children(c),
	  m_enabled(m_preferences->Get<bool>("enabled_at_startup"))
{
	if (m_enabled)
		Enable();
}

template <class SubjectType>
SubjectNode<SubjectType>::SubjectNode(std::shared_ptr<Ws::MergedPreferences> p,
                                      std::shared_ptr<SubjectType> s)
	: m_preferences(p),
	  m_subject(s),
	  m_enabled(m_preferences->Get<bool>("enabled_at_startup"))
{
	if (m_enabled)
		Enable();
}

template <class SubjectType>
void SubjectNode<SubjectType>::Disable()
{
	// Disable children.
	for (auto& child: m_children) {
		std::static_pointer_cast<IViewerNodeWithParent<SubjectType>>
			(child.second)->ParentDisabled();
	}
	m_enabled = false;
}

template <class SubjectType>
void SubjectNode<SubjectType>::Enable()
{
	m_enabled = true;
	// Enable children.
	for (auto& child: m_children) {
		std::static_pointer_cast<IViewerNodeWithParent<SubjectType>>
			(child.second)->ParentEnabled(m_subject);
	}
}

template <class SubjectType>
bool SubjectNode<SubjectType>::IsEnabled() const
{
	return m_enabled;
}

template <class SubjectType>
bool SubjectNode<SubjectType>::IsParentEnabled() const
{
	return true;
}

template <class SubjectType>
IViewerNode::ChildrenMap::const_iterator SubjectNode<SubjectType>::begin() const
{
	return m_children.begin();
}

template <class SubjectType>
IViewerNode::ChildrenMap::const_iterator SubjectNode<SubjectType>::end() const
{
	return m_children.end();
}

} // namespace
} // namespace

#endif // end_of_inclde_guard
