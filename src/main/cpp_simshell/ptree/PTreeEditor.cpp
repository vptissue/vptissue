/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for PTreeEditor.
 */

#include "PTreeEditor.h"

#include "gui/PTreeView.h"
#include "gui/qtmodel/PTreeModel.h"
#include "util/misc/XmlWriterSettings.h"

#include <QCloseEvent>
#include <QFileDialog>
#include <QInputDialog>
#include <QMenu>
#include <QMenuBar>
#include <QMessageBox>
#include <QStatusBar>
#include <QToolBar>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

using namespace std;
using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;
using namespace SimShell::Gui;
using namespace SimPT_Sim::Util;

namespace SimPT_Shell {
namespace Gui {

QString const PTreeEditor::g_caption("PTree Editor");
QString const PTreeEditor::g_caption_with_file("PTree Editor: %1");

PTreeEditor::PTreeEditor(QWidget* parent)
	: QMainWindow(parent),
	  HasUnsavedChangesPrompt("Editor")
{
	setWindowTitle(g_caption);

	/* tree view */
	ptree_view = new PTreeView(this);
	setCentralWidget(ptree_view);

	/* no model until file is opened */
	ptree_model = 0;

	/* status bar */
	statusbar = new QStatusBar(this);
	statusbar->clearMessage();
	setStatusBar(statusbar);

	/* actions */
	action_new             = new QAction(QIcon::fromTheme("document-new"), "&New...", this);
	action_open            = new QAction(QIcon::fromTheme("document-open"), "&Open...", this);
	action_save            = new QAction(QIcon::fromTheme("document-save"), "&Save", this);
	action_save_as         = new QAction(QIcon::fromTheme("document-save-as"), "Save &As...", this);
	action_close           = new QAction("&Close", this);
	action_quit            = new QAction(QIcon::fromTheme("application-exit"), "&Quit", this);
	action_view_toolbar    = new QAction("&Toolbar", this);
	action_view_statusbar  = new QAction("&Statusbar", this);
	action_fullscreen      = new QAction(QIcon::fromTheme("view-fullscreen"), "&Fullscreen", this);
	action_about           = new QAction("&About", this);

	action_save->setEnabled(false);
	action_save_as->setEnabled(false);
	action_close->setEnabled(false);

	action_view_toolbar->setCheckable(true);
	action_view_statusbar->setCheckable(true);
	action_fullscreen->setCheckable(true);
	action_view_toolbar->setChecked(true);
	action_view_statusbar->setChecked(true);
	action_fullscreen->setChecked(false);

	action_new->setShortcut(QKeySequence("Ctrl+N"));
	action_open->setShortcut(QKeySequence("Ctrl+O"));
	action_save->setShortcut(QKeySequence("Ctrl+S"));
	action_save_as->setShortcut(QKeySequence("Shift+Ctrl+S"));
	action_close->setShortcut(QKeySequence("Ctrl+W"));
	action_quit->setShortcut(QKeySequence("Ctrl+Q"));
	action_fullscreen->setShortcut(QKeySequence("F11"));

	/* borrow some actions from view and model */
	QAction* action_undo            = ptree_view->GetUndoAction();
	QAction* action_redo            = ptree_view->GetRedoAction();
	QAction* action_cut             = ptree_view->GetCutAction();
	QAction* action_copy            = ptree_view->GetCopyAction();
	QAction* action_paste           = ptree_view->GetPasteAction();
	QAction* action_find            = ptree_view->GetFindDialogAction();
	QAction* action_clear_highlight = ptree_view->GetClearHighlightAction();
	QAction* action_insert_before   = ptree_view->GetInsertBeforeAction();
	QAction* action_insert_child    = ptree_view->GetInsertChildAction();
	QAction* action_move_up         = ptree_view->GetMoveUpAction();
	QAction* action_move_down       = ptree_view->GetMoveDownAction();
	QAction* action_remove          = ptree_view->GetRemoveAction();
	QAction* action_expand_all      = ptree_view->GetExpandAllAction();
	QAction* action_expand_none     = ptree_view->GetExpandNoneAction();


	/* menus */
	QMenuBar* menu = menuBar();

	QMenu* menu_file = new QMenu("&File", menu);
	menu_file->addAction(action_new);
	menu_file->addAction(action_open);
	menu_file->addSeparator();
	menu_file->addAction(action_save);
	menu_file->addAction(action_save_as);
	menu_file->addSeparator();
	menu_file->addAction(action_close);
	menu_file->addAction(action_quit);
	menu->addMenu(menu_file);

	QMenu* menu_edit = new QMenu("&Edit", menu);
	menu_edit->addAction(action_undo);
	menu_edit->addAction(action_redo);
	menu_edit->addSeparator();
	menu_edit->addAction(action_cut);
	menu_edit->addAction(action_copy);
	menu_edit->addAction(action_paste);
	menu->addMenu(menu_edit);

	QMenu* menu_item = new QMenu("&Item", menu);
	menu_item->addAction(action_insert_before);
	menu_item->addAction(action_insert_child);
	menu_item->addSeparator();
	menu_item->addAction(action_move_up);
	menu_item->addAction(action_move_down);
	menu_item->addSeparator();
	menu_item->addAction(action_remove);
	menu->addMenu(menu_item);

	QMenu* menu_view = new QMenu("&View", menu);
	menu_view->addAction(action_expand_all);
	menu_view->addAction(action_expand_none);
	menu_view->addSeparator();
	menu_view->addAction(action_view_toolbar);
	menu_view->addAction(action_view_statusbar);
	menu_view->addSeparator();
	menu_view->addAction(action_fullscreen);
	menu->addMenu(menu_view);

	QMenu* menu_search = new QMenu("&Search", menu);
	menu_search->addAction(action_find);
	menu_search->addSeparator();
	menu_search->addAction(action_clear_highlight);
	menu->addMenu(menu_search);

	QMenu* menu_help = new QMenu("&Help", menu);
	menu_help->addAction(action_about);
	menu->addMenu(menu_help);

	/* toolbar */
	toolbar = new QToolBar(this);
	toolbar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
	toolbar->setMovable(false);
	toolbar->addAction(action_new);
	toolbar->addAction(action_open);
	toolbar->addAction(action_save);
	toolbar->addSeparator();
	toolbar->addAction(action_undo);
	toolbar->addAction(action_redo);
	toolbar->addSeparator();
	toolbar->addAction(action_find);
	toolbar->addSeparator();
	toolbar->addAction(action_fullscreen);
	addToolBar(toolbar);

	connect(action_new, SIGNAL(triggered()), this, SLOT(SLOT_New()));
	connect(action_open, SIGNAL(triggered()), this, SLOT(SLOT_OpenDialog()));
	connect(action_save, SIGNAL(triggered()), this, SLOT(SLOT_Save()));
	connect(action_save_as, SIGNAL(triggered()), this, SLOT(SLOT_SaveAsDialog()));
	connect(action_close, SIGNAL(triggered()), this, SLOT(SLOT_PromptClose()));
	connect(action_quit, SIGNAL(triggered()), this, SLOT(close()));
	connect(action_view_toolbar, SIGNAL(toggled(bool)), toolbar, SLOT(setVisible(bool)));
	connect(action_view_statusbar, SIGNAL(toggled(bool)), statusbar, SLOT(setVisible(bool)));
	connect(action_fullscreen, SIGNAL(toggled(bool)), this, SLOT(SLOT_SetFullscreenMode(bool)));
	connect(action_about, SIGNAL(triggered()), this, SLOT(SLOT_AboutDialog()));
	connect(ptree_view, SIGNAL(CleanChanged(bool)), this, SLOT(SLOT_SetClean(bool)));
	connect(ptree_view, SIGNAL(StatusChanged(QString const &)), statusbar, SLOT(showMessage(QString const &)));
}

void PTreeEditor::closeEvent(QCloseEvent *event)
{
	if (PromptClose(this)) {
		event->accept();
	} else {
		event->ignore();
	}
}

void PTreeEditor::InternalForceClose() {
	if (ptree_model) {
		ptree_view->setModel(nullptr);
		delete ptree_model;
		ptree_model = nullptr;

		action_save->setEnabled(false);
		action_save_as->setEnabled(false);
		action_close->setEnabled(false);
		setWindowTitle(g_caption);
	}
}

bool PTreeEditor::InternalIsClean() const
{
	return !IsOpened() || ptree_view->IsClean();
}

bool PTreeEditor::InternalSave()
{
	if (opened_path.empty())
		return SLOT_SaveAsDialog();
	else
		return SavePath(opened_path);
}

bool PTreeEditor::IsOpened() const {
	return ptree_model;
}

bool PTreeEditor::OpenPath(string const& path)
{
	if (ptree_model) {
		// another file was already opened
		if (!PromptClose()) { // close file first, showing a "save changes?" dialog if necessary
			return false;
		}
	}

	QString filename = QString::fromStdString(path).split("/").last();
	ptree edit_pt;
	try {
		read_xml(path, root_pt, trim_whitespace);

		// ask for subtree
		bool ok;
		if (root_pt.empty()) {
			QMessageBox::warning(this, QString("Could not open file ") + filename, QString("File is empty!"));
			return false;
		}
		string default_subtree;
		for (auto child : root_pt) {
			if (child.first == "<xmlcomment>")
				continue;
			default_subtree = child.first;
		}
		string const subtree = QInputDialog::getText(this, "", "Please specify path of subtree to edit:",
				QLineEdit::Normal, QString::fromStdString(default_subtree), &ok).toStdString();
		if (ok) {
			edit_pt = root_pt.get_child(subtree);
			// Create model and view
			opened_path = path;
			setWindowTitle(g_caption_with_file.arg(QString::fromStdString(path)));
			subtree_key = subtree;
			ptree_model = new PTreeModel(edit_pt);
			ptree_view->setModel(ptree_model);

			action_close->setEnabled(true);
			action_save_as->setEnabled(true);
			return true;
		} else {
			return false;
		}
	}
	catch (xml_parser_error& e) {
		QMessageBox::warning(this, QString("Could not open file ") + filename,
				QString("Exception xml_parser_error ") + e.what());
	}
	catch (ptree_bad_path& e) {
		QMessageBox::warning(this, QString("Could not open file ") + filename,
				QString("Exception ptree_bad_path ") + e.what());
	}
	catch (ptree_bad_data& e) {
		QMessageBox::warning(this, QString("Could not open file ") + filename,
				QString("Exception ptree_bad_data ") + e.what());
	}
	return false;
}

void PTreeEditor::SLOT_AboutDialog()
{
	const QString app_name = "PTree Editor";
	const QString about = "<h3>PTree editor</h3>\
	                       <p>(c) 2012, <a href=\"http://www.comp.ua.ac.be/\">J. Broeckhove</a> <i>et al.</i><br>\
                               <a href=\"http://www.comp.ua.ac.be\">Computational Modeling and Programming</a><br>\
                               Universiteit Antwerpen, Belgium.</p>";

	QMessageBox::about(this,app_name, about);
}

void PTreeEditor::SLOT_New()
{
	if (ptree_model)
		// another file was already opened
		if (!PromptClose()) // close file first, showing a "save changes?" dialog if necessary
			return;

	bool ok;
	string const subtree =
			QInputDialog::getText(this, "New...", "Root element:",
				QLineEdit::Normal, "vleaf_sim", &ok).toStdString();
	if (ok) {
		ptree edit_pt;
		edit_pt.add_child("<xmlcomment>", ptree("File created by simPT Ptree Editor."));
		root_pt.clear();

		// Create model and view
		opened_path = "";
		setWindowTitle(g_caption_with_file.arg("Untitled"));
		subtree_key = subtree;
		ptree_model = new PTreeModel(edit_pt);
		ptree_view->setModel(ptree_model);

		action_close->setEnabled(true);
		action_save->setEnabled(true);
		action_save_as->setEnabled(true);
	}
}

void PTreeEditor::SLOT_OpenDialog()
{
	QFileDialog* fd = new QFileDialog(this, "Open", QString(), "Ptree files (*.xml)");
	fd->setFilter(QDir::Hidden);
	fd->setFileMode(QFileDialog::AnyFile);
	fd->setAcceptMode(QFileDialog::AcceptOpen);

	if (fd->exec() == QDialog::Accepted) {
		QStringList files = fd->selectedFiles();
		QString filename;
		if (!files.empty()) {
			filename = files.first();
		} else {
			return;
		}
		QFileInfo fi(filename);
		if (!QFile::exists(filename) && QFile::exists(filename + ".xml")) {
			filename += ".xml";
		}

		OpenPath(filename.toStdString());
	}
}

void PTreeEditor::SLOT_PromptClose()
{
	PromptClose();
	/*if (ptree_model) {
		if (!ptree_view->IsClean()) {
			QString filename = QString::fromStdString(opened_path).split("/").last();
			if (filename.length() == 0) {
				filename = "Untitled";
			}
			switch (QMessageBox::question(this, "Save changes?", filename + " has been modified.",
					QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel, QMessageBox::Save)) {
			case QMessageBox::Save:
				if (!Save()) {
					return false;
				}
				break;
			case QMessageBox::Discard:
				break;
			case QMessageBox::Cancel:
				return false;
			default:
				break;
			}
		}
	}
	ForceClose();
	return true;*/
}

void PTreeEditor::SLOT_Save()
{
	InternalSave();
}

bool PTreeEditor::SLOT_SaveAsDialog()
{
	QFileDialog* fd = new QFileDialog(this, "Save As", QString(), "XML files (*.xml)");
	fd->setFileMode(QFileDialog::AnyFile);
	fd->setAcceptMode(QFileDialog::AcceptSave);

	if (fd->exec() == QDialog::Accepted) {
		QStringList files = fd->selectedFiles();
		QString filename;
		if (!files.empty())
			filename = files.first();
		QFileInfo fi(filename);
		if (fi.suffix().isEmpty())
			filename += ".xml";

		return SavePath(filename.toStdString());
	} else {
		return false;
	}
}

bool PTreeEditor::SavePath(std::string const& path)
{
	if (ptree_model) {
		try {
			ptree param_pt = ptree_model->Store();
			root_pt.put_child(subtree_key, param_pt);
			write_xml(path, root_pt, std::locale(), XmlWriterSettings::GetTab());
		}
		catch (xml_parser_error & e) {
			QMessageBox::warning(this, "Could not save to file", QString("Exception xml_parser_error ") + e.what());
			return false;
		}

		ptree_view->SetClean();
		opened_path = path;
		setWindowTitle(g_caption_with_file.arg(QString::fromStdString(opened_path)));
		return true;
	}
	return false;
}

void PTreeEditor::SLOT_SetClean(bool clean)
{
	if (!opened_path.empty()) {
		action_save->setEnabled(!clean);
	}
}

void PTreeEditor::SLOT_SetFullscreenMode(bool checked)
{
	if (checked) {
		setWindowState(windowState() | Qt::WindowFullScreen);
	} else {
		setWindowState(windowState() & ~Qt::WindowFullScreen);
	}
}

} // namespace
} // namespace
