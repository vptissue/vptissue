/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for Preferences.
 */

#include "Preferences.h"

#include "util/misc/XmlWriterSettings.h"

#include <boost/property_tree/xml_parser.hpp>

using namespace std;
using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;
using namespace SimPT_Sim::Util;

namespace SimShell {
namespace Ws {

Preferences::Preferences(const std::string& file)
	: m_file(file)
{
	ptree p;
	try {
		read_xml(m_file, p, trim_whitespace);
		m_preferences = p.get_child("preferences");
	}
	// Ignore errors, if preferences can't be found, leave them empty.
	catch (xml_parser_error&) {}
	catch (ptree_bad_path&) {}
}

Preferences::Preferences(Preferences&& other)
	: m_preferences(move(other.m_preferences)),
	  m_file(move(other.m_file))
{
}

const ptree& Preferences::GetPreferences() const
{
	return m_preferences;
}

void Preferences::SetPreferences(const ptree& prefs)
{
	m_preferences = prefs;
	ptree p;
	p.put_child("preferences", m_preferences);
	write_xml(m_file, p, std::locale(), XmlWriterSettings::GetTab());
	Notify({m_preferences});
}

} // namespace Ws
} // namespace SimShell
