#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################

#============================================================================
# Build (objects get used building, no install).
#============================================================================

#---------------------------- set variables ---------------------------------
set( LIB	simshell )
set( SRC
#---------
	common/InstallDirs.cpp
	common/PTreeQtState.cpp
#---------
	gui/qtmodel/CheckableTreeModel.cpp
	gui/qtmodel/PTreeModel.cpp
	gui/qtmodel/PTreeUndoCommands.cpp
	gui/qtmodel/WorkspaceQtModel.cpp
#---------	
	gui/controller/AppController.cpp
	gui/controller/ProjectController.cpp
	gui/controller/SessionController.cpp
	gui/controller/WorkspaceController.cpp
#---------
	gui/project_actions/ExportActions.cpp
	gui/project_actions/ViewerActions.cpp
#---------
	gui/EnabledActions.cpp
	gui/HasUnsavedChanges.cpp
	gui/HasUnsavedChangesPrompt.cpp
	gui/LogWindow.cpp
	gui/MyDockWidget.cpp
	gui/MyFindDialog.cpp
	gui/MyGraphicsView.cpp
	gui/MyTreeView.cpp
	gui/NewProjectDialog.cpp
	gui/PanAndZoomView.cpp
	gui/PTreeContainer.cpp
	gui/PTreeContainerPreferencesObserver.cpp
	gui/PTreeEditorWindow.cpp
	gui/PTreeMenu.cpp
	gui/PTreeView.cpp
	gui/SaveChangesDialog.cpp
	gui/ViewerDockWidget.cpp
	gui/ViewerWindow.cpp
	gui/WorkspaceView.cpp
	gui/WorkspaceWizard.cpp
#---------
	ptree/PTreeComparison.cpp
	ptree/PTreeEditor.cpp
	ptree/PTreeUtils.cpp
#---------
	workspace/MergedPreferences.cpp
	workspace/Preferences.cpp
#---------
)

set( MOC_HEADERS
#---------
	gui/controller/AppController.h
	gui/controller/ProjectController.h
	gui/controller/SessionController.h
	gui/controller/WorkspaceController.h
#---------		
	gui/project_actions/ExportActions.h
	gui/project_actions/ViewerActions.h
#---------
	gui/qtmodel/CheckableTreeModel.h
	gui/qtmodel/PTreeModel.h
	gui/qtmodel/WorkspaceQtModel.h
#---------		
	gui/MyDockWidget.h
	gui/MyFindDialog.h
	gui/MyGraphicsView.h
	gui/MyTreeView.h
	gui/NewProjectDialog.h
	gui/PTreeContainer.h
	gui/PTreeContainerPreferencesObserver.h
	gui/PTreeEditorWindow.h
	gui/PTreeMenu.h
	gui/PTreeView.h
	gui/SaveChangesDialog.h
	gui/ViewerDockWidget.h
	gui/ViewerWindow.h
	gui/WorkspaceView.h
	gui/WorkspaceWizard.h
#---------		
	ptree/PTreeEditor.h
#---------		
	session/ISession.h
#---------
	workspace/util/FileSystemWatcher.h
)
set( MOC_OUTFILES )

#------------------------------ build ---------------------------------------
if ( ${SIMPT_QT_VERSION} EQUAL 4 )
	qt4_wrap_cpp( MOC_OUTFILES ${MOC_HEADERS} )
elseif( ${SIMPT_QT_VERSION} EQUAL 5 )
	qt5_wrap_cpp( MOC_OUTFILES ${MOC_HEADERS} )
endif()
add_library( ${LIB} OBJECT ${SRC} ${MOC_OUTFILES} )

#------------------------------ unset ---------------------------------------
unset( MOC_HEADERS  )
unset( MOC_OUTFILES )
unset( SRC          )
unset( LIB          )

#############################################################################
