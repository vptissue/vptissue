#ifndef EXPORTERS_GRAPHICS_PREFERENCES_H_INCLUDED
#define EXPORTERS_GRAPHICS_PREFERENCES_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface/Implementation for GraphicsPreferences.
 */

#include "workspace/MergedPreferences.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/optional.hpp>

#include <array>

namespace SimPT_Shell
{

/// Preferences for graphics viewer.
struct GraphicsPreferences
{
	std::string            m_arrow_color;           ///<
	double                 m_arrow_size;            ///<
	std::string            m_background_color;      ///< Qt color or "transparent"
	std::string            m_cell_color;            ///<
	int                    m_cell_number_size;      ///<
	std::string            m_cell_outline_color;    ///<
	double                 m_mesh_magnification;    ///<
	std::array<double, 3>  m_mesh_offset;           ///<
	double                 m_node_magnification;    ///<
	int                    m_node_number_size;      ///<
	double                 m_outline_width;         ///<
	std::string            m_text_color;            ///<
	bool                   m_window_preset;         ///<
	double                 m_window_x_min;          ///<
	double                 m_window_x_max;          ///<
	double                 m_window_y_min;          ///<
	double                 m_window_y_max;          ///<

	bool                   m_border_cells;          ///< Switch for drawing borders.
	bool                   m_cells;                 ///< Switch for drawing cells.
	bool                   m_cell_axes;             ///< Switch for drawing cell axes.
	bool                   m_cell_centers;	        ///< Switch for drawing cell centers.
	bool                   m_cell_numbers;	        ///< Switch for drawing cell numbers.
	bool                   m_cell_strain;	        ///< Switch for drawing cell strain.
	bool                   m_fluxes;	        ///< Switch for drawing fluxes.
	bool                   m_nodes;		        ///< Switch for drawing nodes.
	bool                   m_node_numbers;	        ///< Switch for drawing node numbers.
	bool                   m_only_tissue_boundary;    ///< Switch for drawing boundary.
	bool                   m_tooltips;	        ///< Switch for drawing tool tips.
	bool                   m_walls;	                ///< Switch for drawing wall.

	GraphicsPreferences()
		: m_mesh_offset{{0.0, 0.0, 0.0}}
	{
		// Dummy values; actual values set via a Preferences ptree.
		m_arrow_color          = "darkGreen";
		m_arrow_size           = 10.0;
		m_background_color     = "white";
		m_cell_color           = "size";
		m_cell_number_size     = 1;
		m_cell_outline_color   = "forestgreen";
		m_mesh_magnification   = 1.0;
		m_node_magnification   = 1.0;
		m_node_number_size     = 1.0;
		m_outline_width        = 0.0;
		m_text_color           = "red";
		m_window_preset        = false;
		m_window_x_min         = 0.0;
		m_window_x_max         = 0.0;
		m_window_y_min         = 0.0;
		m_window_y_max         = 0.0;

		m_border_cells         = false;
		m_cells                = true;
		m_cell_axes            = false;
		m_cell_centers         = false;
		m_cell_numbers         = false;
		m_cell_strain          = false;
		m_fluxes               = false;
		m_nodes                = false;
		m_node_numbers         = false;
		m_only_tissue_boundary = false;
		m_tooltips             = false;
		m_walls                = false;
	}

	virtual ~GraphicsPreferences() {}

	virtual void Update(const SimShell::Ws::Event::MergedPreferencesChanged& e)
	{
		auto g = e.source->GetGlobal()->GetChild("graphics");
		m_arrow_color 		= g->Get<std::string>("colors_sizes.arrow_color");
		m_arrow_size 		= g->Get<double>("colors_sizes.arrow_size");
		m_background_color      = g->Get<std::string>("colors_sizes.background_color");
		m_cell_color            = g->Get<std::string>("colors_sizes.cell_color");
		m_cell_number_size 	= g->Get<int>("colors_sizes.cell_number_size");
		m_cell_outline_color 	= g->Get<std::string>("colors_sizes.cell_outline_color");
		m_mesh_magnification    = g->Get<double>("colors_sizes.mesh_magnification", 1.0);
		m_mesh_offset[0]        = g->Get<double>("colors_sizes.mesh_offset_x", 0.0);
		m_mesh_offset[1]        = g->Get<double>("colors_sizes.mesh_offset_x", 0.0);
		m_node_magnification 	= g->Get<double>("colors_sizes.node_magnification");
		m_node_number_size 	= g->Get<int>("colors_sizes.node_number_size");
		m_outline_width 	= g->Get<double>("colors_sizes.outline_width");
		m_text_color 		= g->Get<std::string>("colors_sizes.text_color");

		m_window_preset = false;
		try {
			auto w = e.source->GetChild("source_window");
			m_window_x_min = w->Get<double>("x_min");
			m_window_x_max = w->Get<double>("x_max");
			m_window_y_min = w->Get<double>("y_min");
			m_window_y_max = w->Get<double>("y_max");
			m_window_preset = true;
		} catch (boost::property_tree::ptree_error& e) {}

		m_border_cells 		= g->Get<bool>("visualization.border_cells");
		m_cells 		= g->Get<bool>("visualization.cells");
		m_cell_axes 		= g->Get<bool>("visualization.cell_axes");
		m_cell_centers 		= g->Get<bool>("visualization.cell_centers");
		m_cell_numbers 		= g->Get<bool>("visualization.cell_numbers");
		m_cell_strain 		= g->Get<bool>("visualization.cell_strain");
		m_fluxes 		= g->Get<bool>("visualization.fluxes");
		m_nodes 		= g->Get<bool>("visualization.nodes");
		m_node_numbers 		= g->Get<bool>("visualization.node_numbers");
		m_only_tissue_boundary 	= g->Get<bool>("visualization.only_leaf_boundary");
		m_tooltips              = g->Get<bool>("visualization.tooltips");
		m_walls 		= g->Get<bool>("visualization.walls");
	}
};

} // namespace

#endif
