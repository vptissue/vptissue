#ifndef LOGWINDOWVIEWER_H_INCLUDED
#define LOGWINDOWVIEWER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Header for LogWindowViewer.
 */

#include "gui/controller/AppController.h"
#include "gui/ViewerDockWidget.h"
#include "sim/CoupledSim.h"
#include "sim/event/CoupledSimEvent.h"
#include "sim/event/SimEvent.h"

#include <functional>
#include <string>

class QPlainTextEdit;
class QWidget;

namespace SimPT_Shell {

/**
 * A viewer that displays sim events in a log in a dock window.
 */
class LogWindowViewer
{
public:
	LogWindowViewer(const std::shared_ptr<SimShell::Ws::MergedPreferences>&,
	                SimShell::Gui::Controller::AppController*, std::function<void()> on_close);

	virtual ~LogWindowViewer();

	template <typename EventType>
	void Update(const EventType&);

private:
	std::shared_ptr<SimShell::Ws::MergedPreferences>  m_preferences;
	SimShell::Gui::Controller::AppController*         m_app;
};

template<typename EventType>
inline void LogWindowViewer::Update(const EventType& e)
{
	switch (e.GetType()) {
	case SimPT_Sim::Event::SimEventType::Initialized:
		{
			m_app->Log("Initialized> path = " + m_preferences->GetPath());
			break;
		}
	case SimPT_Sim::Event::SimEventType::Stepped:
		{
			m_app->Log("Stepped> " + e.GetSource()->GetStatusMessage());
			break;
		}
	case SimPT_Sim::Event::SimEventType::Done:
		{
			m_app->Log("Done> path = " + m_preferences->GetPath());
			break;
		}
	case SimPT_Sim::Event::SimEventType::Forced:
		{
			m_app->Log("Forced> " + e.GetSource()->GetStatusMessage());
			break;
		}
	}
}

template<>
inline void LogWindowViewer::Update(const SimPT_Sim::Event::CoupledSimEvent& e)
{
	switch (e.GetType()) {
	case SimPT_Sim::Event::SimEventType::Initialized:
		{
			m_app->Log("Initialized> path = " + m_preferences->GetPath());
			break;
		}
	case SimPT_Sim::Event::SimEventType::Stepped:
		{
			m_app->Log("Stepped> " + std::to_string(e.GetSource()->GetSimStep()));
			break;
		}
	case SimPT_Sim::Event::SimEventType::Done:
		{
			m_app->Log("Done> path = " + m_preferences->GetPath());
			break;
		}
	case SimPT_Sim::Event::SimEventType::Forced:
		{
			m_app->Log("Forced> " + std::to_string(e.GetSource()->GetSimStep()));
			break;
		}
	}
}

} // namespace

#endif // end_of_include_guard
