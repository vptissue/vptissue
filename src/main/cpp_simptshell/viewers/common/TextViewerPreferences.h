#ifndef VIEWERS_TEXT_VIEWER_PREFERENCES_H_INCLUDED
#define VIEWERS_TEXT_VIEWER_PREFERENCES_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface/Implementation for TextViewerPreferences.
 */

#include "workspace/MergedPreferences.h"

namespace SimPT_Shell {

/**
 * Preferences for a text viewer.
 */
struct TextViewerPreferences
{
	TextViewerPreferences() : m_stride{0}, m_gzip(false) {}

	void Update(const SimShell::Ws::Event::MergedPreferencesChanged& e)
	{
		m_stride = e.source->Get<int>("stride");
		m_gzip   = e.source->Get<bool>("gzip", false);
	}

	int   m_stride;
	bool  m_gzip;
};

} // namespace

#endif // end_of_include_guard
