#ifndef VIEW_HDF5_VIEWER_H_INCLUDED
#define VIEW_HDF5_VIEWER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for Hdf5Viewer.
 */

#include "viewers/common/FileViewer.h"
#include "fileformats/Hdf5File.h"

namespace SimPT_Shell {

/**
 * A .cpp file with instantiation is included.
 */
extern template class FileViewer<Hdf5File>;

/**
 * Class definition for Hdf5Viewer. It assumes (@see implementation)
 * preferences hdf5_enabled, hdf5_stride and hdf5_file_name to be available. The
 * latter is the logical file name (no extension, not the full path to the
 * project directory).
 *
 * @note The definition does not use a typedef because that
 * clashes with forward definitions of the same class in client.
 */
class Hdf5Viewer: public FileViewer<Hdf5File>
{
public:
	Hdf5Viewer(const std::shared_ptr<SimShell::Ws::MergedPreferences>& p)
		: FileViewer<Hdf5File>(p) {}
};

} // namespace

#endif // end_of_include_guard
