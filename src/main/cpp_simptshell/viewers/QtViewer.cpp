/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for QtViewer.
 */

#include "viewers/QtViewer.h"

#include "gui/MyGraphicsView.h"
#include "sim/event/SimEvent.h"

using namespace std;
using namespace SimPT_Sim;
using namespace SimPT_Sim::Event;

namespace SimPT_Shell {

QtViewer::QtViewer(const std::shared_ptr<SimShell::Ws::MergedPreferences>& p,
                               QWidget* w, std::function<void()> on_close)
	: SimShell::Gui::ViewerWindow(p, w, on_close),
	  m_preferences(prefs_type::Create(p)),
	  m_canvas(make_shared<QGraphicsScene>()),
	  m_drawer(m_preferences)
{
	setCentralWidget(new SimShell::Gui::MyGraphicsView(m_canvas, this));
	setWindowTitle("SimPT_Sim: Qt Viewer: " + QString::fromStdString(p->GetPath()));

	m_canvas->setBackgroundBrush(QBrush(QColor(QString::fromStdString(m_preferences->m_background_color)).rgb()));
	//string s = std::string("background-color:") + m_preferences->m_background_color + ";";
	//setStyleSheet(s.c_str());

	show();
	raise();
}

QtViewer::~QtViewer()
{
}

} // namespace
