#ifndef SIMPT_SHELL_GUI_APP_CUSTOMIZER_H_INCLUDED
#define SIMPT_SHELL_GUI_APP_CUSTOMIZER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for AppCustomizer.
 */

#include "gui/factory/IFactory.h"

#include <memory>

namespace SimPT_Shell {
namespace Gui {

using namespace SimShell::Gui;
using namespace SimShell::Ws;

/**
 * Provides customization elements for the gui shell.
 *
 * @see IFactory
 */
class AppCustomizer : public IFactory
{
public:
	/// @see IFactory.
	virtual ~AppCustomizer() {}

	/// @see IFactory.
	virtual std::shared_ptr<IWorkspaceFactory> CreateWorkspaceFactory() const;

	/// @see IFactory.
	virtual std::string GetAbout() const;

	/// @see IFactory.
	virtual std::string GetApplicationName() const;

	/// @see IFactory.
	virtual std::string GetOrganizationName() const;
};

} // namespace
} // namespace

#endif // end_of_include_guard
