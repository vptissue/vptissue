#ifndef STEP_FILTER_PROXY_MODEL_H_INCLUDED
#define STEP_FILTER_PROXY_MODEL_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for StepFilterProxyModel
 */

#include "converter/StepSelection.h"

#include <QSortFilterProxyModel>

namespace SimPT_Shell {

/**
 * Custom proxy model to filter a QStandardModel with file names with time steps.
 * The source model is supposed to have a first column with an int as data (for the Qt::DisplayData role)
 */
class StepFilterProxyModel : public QSortFilterProxyModel
{
	Q_OBJECT

public:
	/// Constructor
	explicit StepFilterProxyModel(QObject* parent = nullptr);

	/// Destructor
	virtual ~StepFilterProxyModel();

public slots:
	/**
	 * Sets the ranges of the accepted steps of the first column
	 *
	 * @param	ranges		The ranges-string, complying to the StepSelection::m_repeated_regex regex
	 */
	void SetStepRanges(const QString& ranges);

protected:
	bool filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const;

private:
	StepSelection m_step_selection;
};

} // namespace

#endif // end-of-include-guard
