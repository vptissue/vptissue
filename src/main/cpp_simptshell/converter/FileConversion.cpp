/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of file conversion manager.
 */

#include "FileConversion.h"

#include <QDir>

using namespace std;
using namespace SimShell::Ws;

namespace SimPT_Shell {

FileConversion::FileConversion(
		Ws::Project* project,
		vector<int> timesteps,
		shared_ptr<SimShell::Ws::MergedPreferences> prefs,
		IConverterFormat* output_format,
		string output_path,
		string output_prefix)
	: m_project(project),
	  m_timesteps(timesteps),
	  m_preferences(prefs),
	  m_output_format(output_format),
	  m_output_path(output_path),
	  m_output_prefix(output_prefix)
{
	for (auto& file : *m_project) {
		auto sim_file = static_pointer_cast<Ws::StartupFileBase>(file.second);
		for (auto step : sim_file->GetTimeSteps()) {
			m_step_to_file_map[step] = sim_file;
		}
	}
}

void FileConversion::Run(function<void(int)> progress_callback)
{
	// Pause filesystem watcher
	m_project->SetWatchingDirectory(false);

	string path = m_output_path;
	bool use_temp_dir = false;
	QDir project_dir(QString::fromStdString(path));
	if (path == m_project->GetPath()) {
		project_dir.mkdir("temp");
		path += "/temp";
		use_temp_dir = true;
	}
	// begin batch conversion
	m_output_format->PreConvert(path + '/' + m_output_prefix, m_preferences);
	int i = 0;
	for (auto timestep : m_timesteps) {
		if (progress_callback)
			progress_callback(i);
		auto sim_state = m_step_to_file_map[timestep]->GetSimState(timestep);
		m_output_format->Convert(sim_state);
		i++;
	}
	// remove temp dir if there was one
	if (use_temp_dir) {
		QDir d(QString::fromStdString(path));
		for (auto& entry : d.entryList()) {
			QFile f(QString::fromStdString(m_project->GetPath()) + '/' + entry);
			f.remove();
			d.rename(entry, "../" + entry);
		}
		project_dir.rmdir("temp");
	}
	// end batch conversion
	m_output_format->PostConvert();
	if (progress_callback)
		progress_callback(i);

	// Resume filesystem watcher
	m_project->Refresh();
	m_project->SetWatchingDirectory(true);
}

} // namespace
