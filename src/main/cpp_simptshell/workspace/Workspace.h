#ifndef SIMPT_SHELL_WORKSPACE_H_INCLUDED
#define SIMPT_SHELL_WORKSPACE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for workspace.
 */

#include "../../cpp_simshell/workspace/Workspace.h"

#include "Project.h"
#include "WorkspaceFactory.h"

namespace SimShell {
namespace Ws {
	extern template class Workspace<SimPT_Shell::Ws::Project,
		SimPT_Shell::Ws::WorkspaceFactory::g_workspace_index_file>;
} // namespace
} // namespace


namespace SimPT_Shell {
namespace Ws {

/**
 * Abstraction for workspace info in the file system.
 */
class Workspace : public SimShell::Ws::Workspace<Project, WorkspaceFactory::g_workspace_index_file>
{
public:
        Workspace(const std::string& path, const std::string& prefs_file);

        virtual ~Workspace() {}

        static void Init(const std::string& path);
};

} // namepsace
} // namespace

#endif // end_of_include_guard
