/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for WorkspaceFactory.
 */

#include "WorkspaceFactory.h"

#include "workspace/CliWorkspace.h"
#include "workspace/GuiWorkspace.h"
#include "workspace/Workspace.h"

#include "common/InstallDirs.h"

namespace SimPT_Shell {
namespace Ws {

using namespace std;
using namespace SimShell;
using namespace SimShell::Ws;

shared_ptr<IWorkspace> WorkspaceFactory::CreateCliWorkspace(const string& path)
{
	return make_shared<CliWorkspace>(path);
}

shared_ptr<IWorkspace> WorkspaceFactory::CreateWorkspace(const string& path)
{
	return make_shared<GuiWorkspace>(path);
}

string WorkspaceFactory::GetWorkspaceTemplatePath() const
{
	return InstallDirs::GetWorkspaceDir();
}

string WorkspaceFactory::GetDefaultWorkspaceName() const
{
	return "simPT_Default_workspace";
}

void WorkspaceFactory::InitWorkspace(const string& p)
{
	Workspace::Init(p);
}

const string WorkspaceFactory::g_project_index_file(".simPT-project.xml");

const string WorkspaceFactory::g_workspace_index_file(".simPT-workspace.xml");

} // namespace
} // namespace
