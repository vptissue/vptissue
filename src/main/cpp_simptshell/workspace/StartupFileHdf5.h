#ifndef SIMPT_WS_SIMPT_FILE_HDF5_H_INCLUDED
#define SIMPT_WS_SIMPT_FILE_HDF5_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for Hdf5 tissue file format.
 */

#include "StartupFileBase.h"

namespace SimPT_Shell {
namespace Ws {

/**
 * A file containing Sim history information in HDF5 format in a project.
 */
class StartupFileHdf5 : public StartupFileBase
{
public:
	/// Constructor.
	/// @param path  Path to file.
	StartupFileHdf5(const std::string& path);

	/// @see SimShell::Ws::IFile::CreateSession()
	virtual std::shared_ptr<SimShell::Session::ISession>
	CreateSession(
		std::shared_ptr<SimShell::Ws::IProject> proj,
		std::shared_ptr<SimShell::Ws::IWorkspace> ws) const;

	/// @see IFile
	virtual std::vector<QAction*> GetContextMenuActions() const { return {}; }

	/// @see StartupFileBase
	virtual SimPT_Sim::SimState GetSimState(int timestep) const;

	/// @see StartupFileBase
	virtual std::vector<int> GetTimeSteps() const;

private:
	void InitializeReverseIndex() const;

	mutable bool                m_reverse_index_initialized;
	mutable std::map<int, int>  m_reverse_index;
};

} // namespace
} // namespace

#endif // end_of_include_guard
