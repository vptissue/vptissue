/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for Compressor.
 */

#include "../../../cpp_simptshell/workspace/util/Compressor.h"

#include "fileformats/PTreeFile.h"

#include <QAction>
#include <QFile>

namespace SimPT_Shell{
namespace Ws {
namespace Util {

using namespace std;
using namespace SimPT_Sim::Util;

Compressor::Compressor(const string& path)
	: m_path(path)
{
	m_a_compress = new QAction("Compress", this);
	m_a_decompress = new QAction("Decompress", this);

	connect(m_a_compress, SIGNAL(triggered()), this, SLOT(SLOT_Compress()));
	connect(m_a_decompress, SIGNAL(triggered()), this, SLOT(SLOT_Decompress()));
}

QAction* Compressor::GetActionCompress() const
{
	return m_a_compress;
}

QAction* Compressor::GetActionDecompress() const
{
	return m_a_decompress;
}

void Compressor::SLOT_Compress()
{
	string new_path = m_path + ".gz";
	PTreeFile::Compress(m_path, new_path);

	// delete this file - will be sensed by project, and cause this object to be deleted.
	QFile::remove(QString::fromStdString(m_path));
}

void Compressor::SLOT_Decompress()
{
	string new_path = m_path.substr(0, m_path.length()-3); // strip .gz extension
	PTreeFile::Decompress(m_path, new_path);

	// delete this file - will be sensed by project, and cause this object to be deleted.
	QFile::remove(QString::fromStdString(m_path));
}

} // namespace
} // namespace
} // namespace
