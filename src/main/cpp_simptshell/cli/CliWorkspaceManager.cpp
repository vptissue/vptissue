/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for CliWorkspacemanager.
 */

#include "CliWorkspaceManager.h"

#include "../../cpp_simshell/common/InstallDirs.h"
#include "../workspace/WorkspaceFactory.h"
#include <QDir>
#include <QFile>
#include <iostream>

using namespace std;
using namespace SimShell;
using namespace SimPT_Shell;
using namespace SimPT_Shell::Ws;

namespace SimPT_Shell {

SimShell::Ws::IProject::FileIterator
CliWorkspaceManager::CreateLeaf(
	const shared_ptr<SimShell::Ws::IProject>& project,
	const string& ref_file_path, const string& file)
{
	QFile::copy(QString::fromStdString(ref_file_path),
		QString::fromStdString(project->GetPath() + '/' + file));
	const auto it = project->Add(file);

	assert((it != project->end()) && "Iterator cannot be end().");
	return it;
}

SimShell::Ws::IWorkspace::ProjectIterator
CliWorkspaceManager::CreateProject(
	const shared_ptr<SimShell::Ws::IWorkspace>& workspace, const string& name)
{
	const auto it = workspace->New("project", name);

	assert( (it != workspace->end()) && "Project iterator cannot be end().");
	return it;
}

shared_ptr<Ws::CliWorkspace>
CliWorkspaceManager::CreateWorkspace(const string& base_path, const string& workspace)
{
	const string workspace_path = (base_path == "") ? workspace : base_path + "/" + workspace;
	shared_ptr<Ws::CliWorkspace> ws_ptr;

	if (!QDir(QString::fromStdString(workspace_path)).exists()) {
		QDir(QString::fromStdString(base_path)).mkdir(QString::fromStdString(workspace));
	}
	Ws::CliWorkspace::Init(workspace_path);
	ws_ptr = make_shared<Ws::CliWorkspace>(workspace_path);

	assert((ws_ptr != nullptr) && "Workspace pointer cannot be nullptr.");
	return ws_ptr;
}

SimShell::Ws::IProject::FileIterator
CliWorkspaceManager::OpenLeaf(
	shared_ptr<SimShell::Ws::IProject> project, const string& file,
	bool create_if_not_exists, std::shared_ptr<SimShell::Ws::IFile> ref_file)
{
	assert((project != nullptr) && "Project cannot be nullptr.");
	assert((ref_file != nullptr) && "reference file cannot be nullptr.");

	SimShell::Ws::IProject::FileIterator it = project->Find(file);

	if (it == project->end()) {
		if (create_if_not_exists) {
			it = CreateLeaf(project, ref_file->GetPath(), file);
		}
	}

	assert((it != project->end()) && "File iterator cannot be end().");
	return it;
}

SimShell::Ws::IProject::FileIterator
CliWorkspaceManager::OpenLeaf(
	shared_ptr<SimShell::Ws::IProject> project, const string& file,
	bool create_if_not_exists,
	const string& ref_ws_path, const string& ref_project, const string& ref_file)
{
	assert((project != nullptr) && "project cannot be nullptr.");

	SimShell::Ws::IProject::FileIterator it = project->Find(file);

	if (it == project->end()) {
		if (create_if_not_exists) {
			CliWorkspace ws_template(ref_ws_path);
			it = CreateLeaf(project,
				ws_template.Get(ref_project)->Get(ref_file)->GetPath(), file);
		}
	}

	assert((it != project->end()) && "Leaf iterator cannot be end().");
	return it;
}

SimShell::Ws::IWorkspace::ProjectIterator
CliWorkspaceManager::OpenProject(
	shared_ptr<CliWorkspace> workspace, const string& project,
	bool create_if_not_exists)
{
	SimShell::Ws::IWorkspace::ProjectIterator it = workspace->Find(project);

	if (it == workspace->end()) {
		if (create_if_not_exists) {
			it  = CreateProject(workspace, project);
		}
	}
	assert((it != workspace->end()) && "Project iterator cannot be end().");
	return it;
}

shared_ptr<CliWorkspace>
CliWorkspaceManager::OpenWorkspace(
	const string& base_path, const string& workspace,
	bool create_if_not_exists)
{
	const string workspace_path = (base_path == "") ? workspace : base_path + "/" + workspace;
	shared_ptr<CliWorkspace> ws_ptr;

	if (!QDir(QString::fromStdString(workspace_path)).exists()
		|| !QFile::exists(QString::fromStdString(workspace_path+ "/" + WorkspaceFactory::g_workspace_index_file))) {
		if (create_if_not_exists) {
			ws_ptr = CreateWorkspace(base_path, workspace);
		}
	} else {
		ws_ptr = make_shared<CliWorkspace>(workspace_path);
	}

	assert((ws_ptr != nullptr) && "Workspace pointer cannot be nullptr.");
	return ws_ptr;
}

} // namespace
