#ifndef SESSION_SIM_WORKER_H_INCLUDED
#define SESSION_SIM_WORKER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Header for SimWorker.
 */

#include "sim/SimInterface.h"
#include "sim/Sim.h"

#include <QObject>
#include <QString>
#include <memory>

namespace SimPT_Shell {
namespace Session {

/**
 * Utility for running simulation in a separate thread.
 */
class SimWorker : public QObject
{
	Q_OBJECT
public:
	SimWorker(std::shared_ptr<SimPT_Sim::SimInterface>);

public slots:
	void Work();

signals:
	/// Argument is status message produced by simulation step.
	void Worked(QString);

private:
	std::shared_ptr<SimPT_Sim::SimInterface>  m_sim;
};

} // namespace
} // namespace

#endif // end_of_include_guard
