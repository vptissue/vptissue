#ifndef NODEITEM_H_
#define NODEITEM_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for NodeItem.
 */

#include "bio/Node.h"
#include "bio/Cell.h"

#include <QGraphicsScene>
#include <QGraphicsItem>

namespace SimPT_Shell {

/**
 * Graphic wrapper for Node.
 */
class NodeItem: public QGraphicsItem
{
public:
	NodeItem(SimPT_Sim::Node* n, double node_magnification);

	~NodeItem() {}

	SimPT_Sim::Node& getNode() const { return *m_node; }

	QRectF boundingRect() const;

	void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget);

	QPainterPath shape() const;

	void setBrush(QBrush newbrush) { brush = newbrush; }

	void setColor();

protected:
	QBrush brush;
	QRectF ellipsesize;

private:
	SimPT_Sim::Node* m_node;
};

} // namespace

#endif // end_of_include_guard
