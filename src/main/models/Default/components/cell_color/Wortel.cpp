/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for CellColor::WortelColor.
 */

#include "Wortel.h"

#include "bio/Cell.h"

#include <boost/property_tree/ptree.hpp>
#include <array>

namespace SimPT_Default {
namespace CellColor {

using namespace std;
using boost::property_tree::ptree;
using SimPT_Sim::Cell;

Wortel::Wortel(const boost::property_tree::ptree&)
{
}

array<double, 3> Wortel::operator()(SimPT_Sim::Cell* cell)
{
	const double c0 = (cell->GetChemicals().size() >= 1) ? (cell->GetChemical(0)/cell->GetArea()) : 0.0;
	return (c0 <= 0.05) ? array<double, 3> {{1.0, 1.0, 1.0}} : array<double, 3> {{1.0, c0/(400.0+c0), 0.0}};
}

} // namespace
} // namespace


