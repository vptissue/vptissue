#ifndef DEFAULT_CELL_COLOR_WORTELCOLOR_H_
#define DEFAULT_CELL_COLOR_WORTELCOLOR_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for CellColor::WortelColor.
 */

#include <boost/property_tree/ptree_fwd.hpp>
#include <array>

namespace SimPT_Sim { class Cell; }

namespace SimPT_Default {
namespace CellColor {

/**
 * Implements chemical dependent cell color for Wortel.
 */
class Wortel
{
public:
	/// Straight initialization.
	Wortel(const boost::property_tree::ptree& pt);

	/// Return color value.
	std::array<double, 3> operator()(SimPT_Sim::Cell* cell);
};

} // namespace
} // namespace

#endif // end_of_include_guard
