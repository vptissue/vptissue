/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * ChemBlue colorizer.
 */

#include "ChemBlue.h"

#include "bio/Cell.h"
#include "util/color/hsv.h"
#include "util/color/Hsv2Rgb.h"

#include <boost/property_tree/ptree.hpp>

namespace SimPT_Default {
namespace CellColor {

using namespace std;
using namespace SimPT_Sim::Color;

ChemBlue::ChemBlue(const boost::property_tree::ptree& , unsigned int index)
	: m_index(index)
{
}

std::array<double, 3> ChemBlue::operator()(SimPT_Sim::Cell* cell)
{
	const double c0    = (cell->GetChemicals().size() > m_index) ? cell->GetChemical(m_index) : 0.0;
	const double conc  = max(c0, 0.0) / cell->GetArea();

	// ------------------------------------------------------------------
	// log10 of concentration with 1.0 as reference concentration
	// and a range of four orders of magnitude.
	// -----------------------------------------------------------------
	const double x = log10(conc);

	const double x_white = -5.0;
	const double x_cyan  = -4.5;
	const double x_teal  = -4.0;
	const double x_navy  = -3.0;

	hsv_t t_color;
	if(x < x_white) {
		t_color = white;
	}
	else if (x < x_cyan) {
		t_color = interpolate(x, white, x_white, cyan, x_cyan);
	}
	else if (x < x_teal) {
		t_color = interpolate(x, cyan, x_cyan, teal, x_teal);
	}
	else if (x < x_navy) {
		t_color = interpolate(x, teal, x_teal, navy, x_navy);
	}
	else {
		t_color = navy;
	}

	return Hsv2Rgb(get<0>(t_color), get<1>(t_color), get<2>(t_color));
}

} // namespace
} // namespace


