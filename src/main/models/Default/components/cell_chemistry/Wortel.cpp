/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellChemistry for Wortel model.
 */

#include "Wortel.h"

#include "bio/BoundaryType.h"
#include "bio/Cell.h"
#include "bio/ReduceCellWalls.h"


namespace SimPT_Default {
namespace CellChemistry {

using namespace std;
using boost::property_tree::ptree;

Wortel::Wortel(const CoreData& cd)
{
	Initialize(cd);
}

void Wortel::Initialize(const CoreData& cd)
{
        m_cd = cd;
        //const auto& p = m_cd.m_parameters->get_child("auxin_transport");
        const auto& p = m_cd.m_parameters->get_child("wortel");

        //m_aux1prod        = p.get<double>("aux1prod");
        //m_aux_breakdown   = p.get<double>("aux_breakdown");
        m_aux_breakdown         = p.get<double>("aux_breakdown");
        m_aux_production        = p.get<double>("aux_production");
        m_aux_shy2_breakdown    = p.get<double>("aux_shy2_breakdown");
        m_aux_sink              = p.get<double>("aux_sink");
        m_aux_source            = p.get<double>("aux_source");
        m_ck_breakdown          = p.get<double>("ck_breakdown");
        m_ck_shy2_production    = p.get<double>("ck_shy2_production");
        m_ck_sink               = p.get<double>("ck_sink");
        m_ck_source             = p.get<double>("ck_source");
        m_ga_breakdown          = p.get<double>("ga_breakdown");
        m_ga_production         = p.get<double>("ga_production");
        m_km_aux_ck             = p.get<double>("km_aux_ck");
        m_km_aux_shy2           = p.get<double>("km_aux_shy2");
        m_shy2_breakdown        = p.get<double>("shy2_breakdown");
        m_shy2_production       = p.get<double>("shy2_production");
        m_vm_aux_ck             = p.get<double>("vm_aux_ck");


}

void Wortel::operator()(Cell* cell, double* dchem)
{
	/// START FIRST CK-AUX-SHY2
	/// auxin and cytokinin dynamics
	//double kmauxCK = 100.;
	if (cell->GetIndex() >= 2 && cell->GetIndex() <= 9 )
	{
		dchem[0] = m_aux_source + (cell->GetArea()) * m_aux_production - m_aux_breakdown * (cell->GetChemical(0)) ;//LEVELS!
		dchem[1] = m_ck_source + ( m_vm_aux_ck * (cell->GetArea()) * m_km_aux_ck / ( m_km_aux_ck + (cell->GetChemical(0) / (cell->GetArea())) ) )  - m_ck_breakdown * (cell->GetChemical(1)) ;//LEVELS!
	}
	else if ( (cell->GetIndex() >= 0 && cell->GetIndex() <= 1) || (cell->GetIndex() >= 10 && cell->GetIndex() <= 11) )
	{
		dchem[0] = -m_aux_sink + (cell->GetArea()) * m_aux_production - m_aux_breakdown * (cell->GetChemical(0)) ;//LEVELS!
		dchem[1] = -m_ck_sink + ( m_vm_aux_ck * (cell->GetArea()) * m_km_aux_ck / ( m_km_aux_ck + (cell->GetChemical(0) / (cell->GetArea())) ) ) - m_ck_breakdown * (cell->GetChemical(1)) ;//LEVELS!
	}
	else
	{
		dchem[0] = (cell->GetArea()) * m_aux_production - m_aux_breakdown * (cell->GetChemical(0)) ;//LEVELS!
		dchem[1] = ( m_vm_aux_ck * (cell->GetArea()) * m_km_aux_ck / ( m_km_aux_ck + (cell->GetChemical(0) / (cell->GetArea())) ) ) - m_ck_breakdown * (cell->GetChemical(1)) ;//LEVELS!
	}

	/////SHY2 dynamics

	if( ( cell->GetChemical(1) / (cell->GetArea()) )  > m_ck_shy2_production )
	{
		dchem[2] = m_shy2_production * (cell->GetArea()) - (cell->GetChemical(2)) * ( m_shy2_breakdown + m_aux_shy2_breakdown * ( (cell->GetChemical(0) / cell->GetArea()) / ( m_km_aux_shy2 + cell->GetChemical(0) / cell->GetArea()) ) );
	}
	else
	{
		dchem[2] = - m_shy2_breakdown * (cell->GetChemical(2));
	}

	///GA dynamics
	if( cell->GetCellType() > 2 && cell->GetCellType() < 8 )
	{
		dchem[3] = m_ga_production - m_ga_breakdown * cell->GetChemical(3);
	}
	else
	{
		dchem[3] = 2 * m_ga_production - m_ga_breakdown * cell->GetChemical(3);//wider cells have proportionally more GA production
	}
}

} // namespace
} // namespace
