/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellChemistry for SmithPhyllotaxis model.
 */

#include "SmithPhyllotaxis.h"

#include "bio/BoundaryType.h"
#include "bio/Cell.h"
#include "bio/CellAttributes.h"
#include "bio/ReduceCellWalls.h"
#include "bio/Wall.h"
#include "bio/WallAttributes.h"
#include "sim/CoreData.h"

using namespace std;
using boost::property_tree::ptree;

namespace SimPT_Default {
namespace CellChemistry {

SmithPhyllotaxis::SmithPhyllotaxis(const CoreData& cd)
{
        Initialize(cd);
}

void SmithPhyllotaxis::Initialize(const CoreData& cd)
{
        // Keep a reference to core data
        m_cd = cd;

        // Assign parameters from core data to this model
        const auto& p = m_cd.m_parameters->get_child("smith_phyllotaxis");
        m_rho_IAA = p.get<double>("rho_IAA");
        m_mu_IAA = p.get<double>("mu_IAA");
        m_k_IAA = p.get<double>("k_IAA");
        m_rho_PIN = p.get<double>("rho_PIN");
        m_rho_PIN0 = p.get<double>("rho_PIN0");
        m_mu_PIN = p.get<double>("mu_PIN");
        m_k_PIN = p.get<double>("k_PIN");
}

void SmithPhyllotaxis::operator()(Cell* cell, double* dchem)
{
        const double IAA = cell->GetChemical(0);
        const double PIN = cell->GetChemical(1);
        const double dIAAdt = m_rho_IAA / (1.0 + m_k_IAA * IAA) - m_mu_IAA * IAA;
        const double dPINdt = (m_rho_PIN0 + m_rho_PIN * IAA) / (1.0 + m_k_PIN * PIN) - m_mu_PIN * PIN;

        dchem[0] = dIAAdt;
        dchem[1] = dPINdt;
}

} // namespace
} // namespace

