/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellDaughters component for Auxin model.
 */

#include "Auxin.h"

#include "bio/Cell.h"

namespace SimPT_Default {
namespace CellDaughters {

using namespace std;
using namespace boost::property_tree;

Auxin::Auxin(const CoreData& cd)
{
	Initialize(cd);
}

void Auxin::Initialize(const CoreData& cd)
{
        m_cd = cd;
        auto& p = m_cd.m_parameters;

        ptree const& arr_initval = p->get_child("auxin_transport.initval.value_array");
        m_x_initval = (++arr_initval.begin())->second.get_value<double>();
}

void Auxin::operator()(Cell* daughter1, Cell* daughter2)
{
	const double area1 = daughter1->GetArea();
	const double area2 = daughter2->GetArea();
	const double tot_area = area1 + area2;

	// Auxin distributes between parent and daughter according to area
	daughter1->SetChemical(0, daughter1->GetChemical(0) * (area1 / tot_area));
	daughter2->SetChemical(0, daughter2->GetChemical(0) * (area2 / tot_area));

	// After divisions, parent and daughter cells get a standard stock of PINs.
	daughter1->SetChemical(1, m_x_initval);
	daughter2->SetChemical(1, m_x_initval);
}

} // namespace
} // namespace
