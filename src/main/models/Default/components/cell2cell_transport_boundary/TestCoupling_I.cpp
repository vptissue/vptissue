/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellToCellTransportBoundary component for the TestCoupling model.
 */

#include "TestCoupling_I.h"

#include "bio/Cell.h"
#include "bio/Wall.h"

namespace SimPT_Default {
namespace CellToCellTransportBoundary{

using namespace std;
using namespace SimPT_Sim;
using namespace boost::property_tree;
using namespace SimPT_Sim::Util;

TestCoupling_I::TestCoupling_I(const CoreData& cd)
{
	Initialize(cd);
}

void TestCoupling_I::Initialize(const CoreData& cd)
{
        m_cd    = cd;
}

void TestCoupling_I::operator()(Wall* w, double* dchem_c1, double* dchem_c2)
{
	auto& transfer_map    = m_cd.m_coupled_sim_transfer_data;
	double transfer_value = 0.0;

	if (w->GetC1()->IsBoundaryPolygon()) {
		auto it = transfer_map->find(w->GetC2()->GetIndex());
		if (it != transfer_map->end()) {
			transfer_value     = get<0>(it->second);
			double D_coupling  = get<1>(it->second);
			dchem_c2[0] += w->GetLength() * D_coupling *
			        ( transfer_value - ( w->GetC2()->GetChemical(0)) / w->GetC2()->GetArea() ) ;
		}
	} else if (w->GetC2()->IsBoundaryPolygon())  {
		auto it = transfer_map->find(w->GetC1()->GetIndex());
		if (it != transfer_map->end()) {
			transfer_value     = get<0>(it->second);
			double D_coupling  = get<1>(it->second);
			dchem_c1[0] += w->GetLength() * D_coupling *
			        ( transfer_value - ( w->GetC1()->GetChemical(0)) / w->GetC1()->GetArea() ) ;
		}
	}
}

} // namespace
} // namespace
