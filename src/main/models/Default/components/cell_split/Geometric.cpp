/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellSplit for Geometric model.
 */

#include "Geometric.h"

#include "bio/Cell.h"
#include "math/RandomEngine.h"
#include "sim/CoreData.h"

#include <cmath>
#include <iostream>
#include <trng/uniform01_dist.hpp>

namespace SimPT_Default {
namespace CellSplit {

using namespace std;
using namespace boost::property_tree;

Geometric::Geometric(const CoreData& cd)
{
	Initialize(cd);
}

void Geometric::Initialize(const CoreData& cd)
{
        const trng::uniform01_dist<double> dist;
        m_uniform_generator = cd.m_random_engine->GetGenerator(dist);

        auto& p                = cd.m_parameters;
        m_cell_base_area       = p->get<double>("cell_mechanics.base_area");
        m_division_ratio       = p->get<double>("cell_mechanics.division_ratio");
        m_fixed_division_axis  = p->get<bool>("cell_mechanics.fixed_division_axis", false);
        m_div_area             = m_division_ratio * m_cell_base_area;
}

std::tuple<bool, bool, std::array<double, 3>> Geometric::operator()(Cell* cell)
{
	const double CCnoise = 1 + 0.*(m_uniform_generator() - 0.5) / 5;
	const bool must_divide        = (cell->GetArea() > m_div_area * CCnoise);
	return std::make_tuple(must_divide, m_fixed_division_axis, std::array<double, 3> {{1.0, 0.0, 0.0}});
}

} // namespace
} // namespace
