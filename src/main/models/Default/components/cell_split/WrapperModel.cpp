/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellSplit for Wrapper model.
 */

#include "WrapperModel.h"

#include "bio/Cell.h"
#include "math/RandomEngine.h"
#include "sim/CoreData.h"

#include <boost/property_tree/ptree.hpp>
#include <trng/uniform01_dist.hpp>
#include <array>

namespace SimPT_Default {
namespace CellSplit {

using namespace std;
using namespace boost::property_tree;

WrapperModel::WrapperModel(const CoreData& cd)
{
	Initialize(cd);
}

void WrapperModel::Initialize(const CoreData& cd)
{
	const trng::uniform01_dist<double> dist;
	m_uniform_generator = cd.m_random_engine->GetGenerator(dist);

	const auto& p                 = cd.m_parameters;
	m_cell_division_threshold     = p->get<double>("wrapper_model.cell_division_threshold");
}

std::tuple<bool, bool, std::array<double, 3>> WrapperModel::operator()(Cell* cell)
{
	bool must_divide = false;
	double CCnoise2 = 1 /*+ m_cell_division_noise * (m_uniform_generator() - 0.5)*/;
	if(/*(cell->GetChemical(9) / cell->GetArea()) > m_CDK_threshold ) &&*/ cell->GetArea() >= (  m_cell_division_threshold * CCnoise2 )) {
		must_divide = true;
	}

	return std::make_tuple(must_divide, true, array<double, 3> {{1.0, 0.0, 0.0}});
}

} // namespace
} // namespace
