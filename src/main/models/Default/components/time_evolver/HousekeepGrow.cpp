/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Time evolver for Housekeep plus Grow.
 */

#include "HousekeepGrow.h"

#include "Housekeep.h"
#include "Grow.h"

#include <cassert>
#include <boost/property_tree/ptree.hpp>

namespace SimPT_Default  {
namespace TimeEvolver {

using namespace std;
using namespace boost::property_tree;

HousekeepGrow::HousekeepGrow(const CoreData& cd)
{
	Initialize(cd);
}

void HousekeepGrow::Initialize(const CoreData& cd)
{
	assert( cd.Check() && "CoreData not ok.");
	m_cd = cd;
}

std::tuple<SimTimingTraits::CumulativeTimings, bool> HousekeepGrow::operator()(double /*time_slice*/, SimPhase /*phase*/)
{
	// -------------------------------------------------------------------------------
	// Cell housekeeping a.o. division.
	// -------------------------------------------------------------------------------
	Housekeep  housekeeper(m_cd);
	auto tup1 = housekeeper();
	CumulativeTimings timings(get<0>(tup1));
	bool is_stationary = get<1>(tup1);

	// -------------------------------------------------------------------------------
	// Metropolis loop until relaxation.
	// -------------------------------------------------------------------------------
	Grow  grower(m_cd);
	auto tup2 = grower();
	timings.Merge(get<0>(tup2));
	is_stationary = is_stationary && get<1>(tup2);

	// -------------------------------------------------------------------------------
	// We are done ...
	// -------------------------------------------------------------------------------
	return make_tuple(timings, is_stationary);
}

} // namespace
} // namespace
