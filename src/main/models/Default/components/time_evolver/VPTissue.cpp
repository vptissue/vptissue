/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for SimPT time evolver.
 */

#include "VPTissue.h"
#include "HousekeepGrow.h"
#include "Transport.h"

#include "util/misc/Exception.h"
#include "util/misc/log_debug.h"

#include <cassert>
#include <boost/property_tree/ptree.hpp>

namespace SimPT_Default  {
namespace TimeEvolver {

using namespace std;
using namespace SimPT_Sim::ClockMan;

VPTissue::VPTissue(const CoreData& cd)
{
        Initialize(cd);
}

void VPTissue::Initialize(const CoreData& cd)
{
        assert( cd.Check() && "CoreData not ok.");
        m_cd = cd;
}

std::tuple<SimTimingTraits::CumulativeTimings, bool> VPTissue::operator()(double time_slice, SimPhase phase)
{
        CumulativeTimings timings;
        bool is_stationary = true;

        if (phase == SimPhase::_1 || phase == SimPhase::NONE) {
                // -------------------------------------------------------------------------------
                // Reaction and transport dynamics.
                // -------------------------------------------------------------------------------
                Transport  transporter(m_cd);
                const auto tup1 = transporter(time_slice);
                timings.Merge(get<0>(tup1));
                is_stationary &= get<1>(tup1);
        }

        if (phase == SimPhase::_2 || phase == SimPhase::NONE) {
                // -------------------------------------------------------------------------------
                // Housekeep followed by Metropolis loop until relaxation.
                // -------------------------------------------------------------------------------
                HousekeepGrow  grow_housekeep(m_cd);
                const auto tup2 = grow_housekeep();
                timings.Merge(get<0>(tup2));
                is_stationary &= get<1>(tup2);
        }

        // -------------------------------------------------------------------------------
        // We are done ...
        // -------------------------------------------------------------------------------
        return make_tuple(timings, is_stationary);
}

} // namespace
} // namespace
