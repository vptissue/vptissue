#ifndef SIMPT_DEFAULT_TIME_EVOLVER_RDAT_H_INCLUDED
#define SIMPT_DEFAULT_TIME_EVOLVER_RDAT_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Time evolver for diffusion and active transport.
 */

#include "sim/CoreData.h"
#include "sim/SimPhase.h"
#include "sim/SimTimingTraits.h"

namespace SimPT_Default  {
namespace TimeEvolver {

using namespace SimPT_Sim;

/**
 * Time evolution of reaction, diffusion and active transport process.
 */
class Transport : public SimTimingTraits
{
public:
	/// Initializing constructor.
	Transport(const CoreData& cd);

	/// Initialize or re-initialize.
	void Initialize(const CoreData& cd);

	/// Take a time step.
	std::tuple<SimTimingTraits::CumulativeTimings, bool>
	operator()(double time_slice, SimPhase phase = SimPhase::NONE);

private:
	CoreData            m_cd;   ///< Core data (mesh, params, sim_time,...).
        double              m_abs_tolerance;               ///<
        bool                m_is_stationary;               ///<
        std::string         m_ode_solver;                  ///<
        double              m_rel_tolerance;               ///<
        double              m_small_time_increment;        ///<
        bool                m_stationarity_check;          ///<
};

} // namespace
} // namespace

#endif // end-of-include-guard
