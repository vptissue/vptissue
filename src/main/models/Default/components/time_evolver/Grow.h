#ifndef SIMPT_DEFAULT_TIME_EVOLVER_GROW_H_INCLUDED
#define SIMPT_DEFAULT_TIME_EVOLVER_GROW_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Time evolver for Grow only.
 */

#include "sim/CoreData.h"
#include "sim/SimPhase.h"
#include "sim/SimTimingTraits.h"

#include <functional>

namespace SimPT_Default {
namespace TimeEvolver {

using namespace SimPT_Sim;

/**
 * Growth and division time evolution algorithm.
 */
class Grow : public SimTimingTraits
{
public:
	/// Initializing constructor.
	Grow(const CoreData& cd);

	/// Initialize or re-initialize.
	void Initialize(const CoreData& cd);

	/// Take a time evolution step.
	std::tuple<SimTimingTraits::CumulativeTimings, bool>
	operator()(double time_slice = 0.0, SimPhase phase = SimPhase::NONE);

private:
	CoreData           m_cd;                    ///< Core data (mesh, params, sim_time,...).
	double             m_mc_cell_step_size;     ///<
	double             m_mc_abs_tolerance;      ///< Absolute tolerance for MC sweep termination.
	unsigned int       m_mc_retry_limit;        ///< Limit of MC retries in case og energy increase.
	unsigned int       m_mc_sliding_window;     ///<
	double             m_mc_step_size;          ///<
	unsigned int       m_mc_sweep_limit;        ///< Limit for number of MC sweeps.
	double             m_mc_temperature;        ///<
	bool               m_stationarity_check;    ///<

private:
	std::function<bool (double, double)>    m_within_tolerance;    ///<
};

} // namespace
} // namespace

#endif // end-of-include-guard
