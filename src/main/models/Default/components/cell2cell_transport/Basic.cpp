/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Basic CellToCellTransport component.
 */

#include "Basic.h"

#include "bio/Cell.h"
#include "bio/Wall.h"

namespace SimPT_Default {
namespace CellToCellTransport {

using namespace std;
using namespace boost::property_tree;

Basic::Basic(const CoreData& cd)
{
	Initialize(cd);
}

void Basic::Initialize(const CoreData& cd)
{
        m_cd                = cd;
        auto& p             = m_cd.m_parameters;

        ptree const& arr_D = p->get_child("auxin_transport.D.value_array");
        m_d = arr_D.begin()->second.get_value<double>();

        m_ka                = p->get<double>("auxin_transport.ka");
        m_tip_source   = p->get<double>("auxin_transport.leaf_tip_source", 0.0);
        m_transport         = p->get<double>("auxin_transport.transport");
}

void Basic::operator()(Wall* w, double* dchem_c1, double* dchem_c2)
{
	double const phi = w->GetLength() * m_d
		* (w->GetC2()->GetChemical(0) - w->GetC1()->GetChemical(0));
	dchem_c1[0] += phi;
	dchem_c2[0] -= phi;

	// directed transport: efflux from cell 1 to cell 2
	double const trans12 = m_transport * w->GetTransporters1(1) * w->GetC1()->GetChemical(0)
		                			/ (m_ka + w->GetC1()->GetChemical(0));

	// directed transport: efflux from cell 2 to cell 1
	double const trans21 = m_transport * w->GetTransporters2(1) * w->GetC2()->GetChemical(0)
		                			/ (m_ka + w->GetC2()->GetChemical(0));

	dchem_c1[0] += trans21 - trans12;
	dchem_c2[0] += trans12 - trans21;

	// Influx at leaf "AuxinSource" (as specified in initial condition)
	if (w->IsAuxinSource()) {
		double const aux_flux = m_tip_source * w->GetLength();
		dchem_c1[0] += aux_flux;
		dchem_c2[0] += aux_flux;
	}
}

} // namespace
} // namespace
