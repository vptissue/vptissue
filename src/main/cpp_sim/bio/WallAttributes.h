#ifndef WALL_ATTRIBUTES_H_INCLUDED
#define WALL_ATTRIBUTES_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for WallAttributes.
 */

#include "WallType.h"

#include <iosfwd>
#include <string>
#include <vector>
#include <boost/property_tree/ptree.hpp>

namespace SimPT_Sim {

class Node;
class Cell;
class Mesh;

/**
 * Attributes associated with a wall.
 */
class WallAttributes
{
public:
	WallAttributes(unsigned int num_chem);

	WallAttributes(double rest_length, double rest_length_init, double strength,
		const std::vector<double>& transporters1, const std::vector<double>& transporters2,
		WallType::Type wall_type);

	virtual ~WallAttributes() {}

	void CycleWallType();

	double GetRestLength() const
	{
		return m_rest_length;
	}

	double GetRestLengthInit() const
	{
		return m_rest_length_init;
	}

	double GetStrength() const
	{
		return m_strength;
	}

	std::vector<double> GetTransporters1() const
	{
		return m_transporters1;
	}

	double GetTransporters1(unsigned int ch) const
	{
		return m_transporters1[ch];
	}

	std::vector<double> GetTransporters2() const
	{
		return m_transporters2;
	}

	double GetTransporters2(unsigned int ch) const
	{
		return m_transporters2[ch];
	}

	WallType::Type GetType() const
	{
		return m_wall_type;
	}

	bool IsAuxinSource() const
	{
		return m_wall_type == WallType::Type::AuxinSource;
	}

	bool IsAuxinSink() const { return m_wall_type == WallType::Type::AuxinSink; }

	virtual void ReadPtree(boost::property_tree::ptree const& wall_pt);

	void SetRestLength(double l) { m_rest_length = l; }

	void SetRestLengthInit(double l) { m_rest_length_init = l; }

	void SetStrength(double strength) { m_strength = strength; }

	void SetTransporters1(unsigned int ch, double val)
	{
		assert( ch < m_transporters1.size() && "Index out of bounds!");
		m_transporters1[ch] = val;
	}

	void SetTransporters1(const std::vector<double>& transporter) { m_transporters1 = transporter; }

	void SetTransporters2(unsigned int ch, double val)
	{
		assert( m_transporters2.size() && "Index out of bounds!");
		m_transporters2[ch] = val;
	}

	void SetTransporters2(const std::vector<double>& transporter)
	{
		m_transporters2 = transporter;
	}

	void SetType(WallType::Type type)
	{
		m_wall_type = type;
	}

	void Swap(WallAttributes& src);

	/**
 	 * Convert the wall attributes to a ptree.
	 * The format of a wall attributes ptree is as follows:
	 *
	 * <type>[the wall type]</type>
	 * <transporter1_array>
	 * 	(for every transporter1 in the wall)
	 * 	<transporter1>[the value of the transporter1]</transporter1>
	 * </transporter1_array>
	 * <transporter2_array>
	 * 	(for every transporter2 in the wall)
	 * 	<transporter2>[the value of the transporter2]</transporter2>
	 * </transporter2_array>
	 */
	virtual boost::property_tree::ptree ToPtree() const;

protected:
	double                m_rest_length;        ///<
        double                m_rest_length_init;   ///<
	double                m_strength;           ///<
	std::vector<double>   m_transporters1;      ///<
	std::vector<double>   m_transporters2;      ///<
	WallType::Type        m_wall_type;          ///<

protected:

};

}

#endif // end_of_include_guard
