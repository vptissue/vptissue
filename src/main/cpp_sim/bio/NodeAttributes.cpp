/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for NodeAttributes.
 */

#include "NodeAttributes.h"

#include "util/misc/Exception.h"
#include "util/misc/log_debug.h"

namespace SimPT_Sim {

using namespace std;
using namespace boost::property_tree;
using namespace SimPT_Sim::Util;

void NodeAttributes::ReadPtree(const ptree& node_pt)
{
	try {
		m_fixed  = node_pt.get<bool>("fixed");
		m_sam    = node_pt.get<bool>("sam");
	}
	catch(exception& e) {
		const string here = string(VL_HERE) + " exception:\n";
		throw Exception(here + e.what());
	}
}

ptree NodeAttributes::ToPtree() const
{
	ptree ret;
	ret.put("fixed", IsFixed());
	ret.put("sam", IsSam());
	return ret;
}

}
