#ifndef MESH_CHECK_H_INCLUDED
#define MESH_CHECK_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for MeshCheck.
 */

namespace SimPT_Sim {

class Mesh;

/**
 * Checks mesh concistency.
 */
class MeshCheck
{
public:
	/**
	 *
	 */
	MeshCheck(const Mesh& mesh);

	/**
	 * Runs all of the of checks to verify consistency of the mesh.
	 */
	bool CheckAll() const;

	/**
	 * Verifies that sum of all cell areas equals the area of
	 * the boundary polygon. The difference should be smaller
	 * than 10e-7 (due to floating-point errors).
	 */
	bool CheckAreas() const;

	/**
	 * Verifies Node::IsAtBoundary is true iff node is part
	 * of boundary polygon, for every node in the mesh.
	 */
	bool CheckAtBoundaryNodes() const;

	/**
	 * Verifies that for each cell, each boundary wall (other cell
	 * owning the wall is the boundary polygon) the nodes of the wall
	 * edges are AtBoundary nodes (i.e. owned by boundary polygon also).
	 */
	bool CheckCellBoundaryWallNodes() const;

	/**
	 * Verifies that for all cells the Cell::HasBoundaryWall agrees
	 * with Mesh::IsWallNeighbor for boundary polygon.
	 */
	bool CheckCellBoundaryWalls() const;

	/**
	 * Verifies that cell ids correspond to storage sequence.
	 */
	bool CheckCellIdsSequence() const;

	/**
	 * Verifies that cell identifiers are unique.
	 */
	bool CheckCellIdsUnique() const;

	/**
	 * Verifies that every edge has exactly two
	 * neighbor cells by using Mesh::GetEdgeOwners.
	 */
	bool CheckEdgeOwners() const;

	/**
	 * Verifies that Cell::IsWallNeighbor is a symmetric relationship.
	 */
	bool CheckMutuallyNeighbors() const;

	/**
	 * Verifies that node ids correspond to storage sequence.
	 */
	bool CheckNodeIdsSequence() const;

	/**
	 * Verifies that node identifiers are unique.
	 */
	bool CheckNodeIdsUnique() const;

	/**
	 * Verifies that for each node its NeighborNodes constructs appear
	 * once and only once in the NodeOwningNeighbors map entry for the node.
	 */
	bool CheckNodeOwningNeighbors() const;

	/**
	 * Verifies that the owning walls of every node
	 * are exactly those walls to which the node belongs.
	 */
	bool CheckNodeOwningWalls() const;

	/**
	 * Verifies whether every cell has exactly one entry in the wall neighbor
	 * list (except when there is only one cell in the mesh). This entry should
	 * also contain the cells which are located next to the cell being tested
	 * (except for the boundary polygon).
	 */
	bool CheckWallNeighborsList() const;

	/**
	 * Verifies that wall ids correspond to storage sequence.
	 */
	bool CheckWallIdsSequence() const;

	/**
	 * Verifies that wall identifiers are unique.
	 */
	bool CheckWallIdsUnique() const;

private:
	MeshCheck(const MeshCheck& );
	MeshCheck&  operator=(const MeshCheck& );

private:
	/// Outer boundary of the mesh.
	const Mesh&   m_mesh;
};

}
#endif // end_of_include_guard

