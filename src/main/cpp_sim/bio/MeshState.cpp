/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of MeshState.
 */
#include "MeshState.h"

#include <iostream>
#include <iomanip>

using namespace std;
using boost::property_tree::ptree;

namespace SimPT_Sim {

MeshState::MeshState()
{
	SetNumNodes(0);
	SetNumCells(0);
	SetNumWalls(0);
}

////////////////////////////////////////////////////////////////////////////////
// Nodes
////////////////////////////////////////////////////////////////////////////////

size_t MeshState::GetNumNodes() const
{
	return m_num_nodes;
}

void MeshState::SetNumNodes(size_t n)
{
	// Make sure that all containers are resized to hold n nodes
	m_num_nodes = n;
	m_nodes_id.resize(n);
	m_nodes_x.resize(n);
	m_nodes_y.resize(n);
	m_nodes_attr.SetNum(n);
}

int MeshState::GetNodeID(size_t node_idx) const
{
	return m_nodes_id.at(node_idx);
}

double MeshState::GetNodeX(size_t node_idx) const
{
	return m_nodes_x.at(node_idx);
}

double MeshState::GetNodeY(size_t node_idx) const
{
	return m_nodes_y.at(node_idx);
}

void MeshState::SetNodeID(size_t node_idx, int value)
{
	m_nodes_id.at(node_idx) = value;
}

void MeshState::SetNodeX(size_t node_idx, double value)
{
	m_nodes_x.at(node_idx) = value;
}

void MeshState::SetNodeY(size_t node_idx, double value)
{
	m_nodes_y.at(node_idx) = value;
}

vector<int> MeshState::GetNodesID() const
{
	return m_nodes_id;
}

vector<double> MeshState::GetNodesX() const
{
	return m_nodes_x;
}

vector<double> MeshState::GetNodesY() const
{
	return m_nodes_y;
}

void MeshState::SetNodesID(vector<int> values)
{
	if (values.size() != m_num_nodes) {
		throw runtime_error("MeshState::SetNodesID(): Number of IDs doesn't match number of nodes.");
	} else {
		m_nodes_id = values;
	}
}

void MeshState::SetNodesX(vector<double> values)
{
	if (values.size() != m_num_nodes) {
		throw runtime_error("MeshState::SetNodesX(): Number of X coords doesn't match number of nodes.");
	} else {
		m_nodes_x = values;
	}
}

void MeshState::SetNodesY(vector<double> values)
{
	if (values.size() != m_num_nodes) {
		throw runtime_error("MeshState::SetNodesY(): Number of Y coords doesn't match number of nodes.");
	} else {
		m_nodes_y = values;
	}
}

////////////////////////////////////////////////////////////////////////////////
// Cells
////////////////////////////////////////////////////////////////////////////////

size_t MeshState::GetNumCells() const
{
	return m_num_cells;
}

void MeshState::SetNumCells(size_t n)
{
	// Make sure that all containers are resized to hold n cells
	m_num_cells = n;
	m_cells_id.resize(n);
	m_cells_nodes.resize(n);
	m_cells_walls.resize(n);
	m_cells_attr.SetNum(n);
}

int MeshState::GetCellID(size_t cell_idx) const
{
	return m_cells_id.at(cell_idx);
}

size_t MeshState::GetCellNumNodes(size_t cell_idx) const
{
	return m_cells_nodes.at(cell_idx).size();
}

vector<int> MeshState::GetCellNodes(size_t cell_idx) const
{
	return m_cells_nodes.at(cell_idx);
}

size_t MeshState::GetCellNumWalls(size_t cell_idx) const
{
	return m_cells_walls.at(cell_idx).size();
}

vector<int> MeshState::GetCellWalls(size_t cell_idx) const
{
	return m_cells_walls.at(cell_idx);
}

void MeshState::SetCellID(size_t cell_idx, int value)
{
	m_cells_id.at(cell_idx) = value;
}

void MeshState::SetCellNodes(size_t cell_idx, vector<int> node_ids)
{
	m_cells_nodes.at(cell_idx) = node_ids;
}

void MeshState::SetCellWalls(size_t cell_idx, vector<int> wall_ids)
{
	m_cells_walls.at(cell_idx) = wall_ids;
}

vector<int> MeshState::GetCellsID() const
{
	return m_cells_id;
}

vector<size_t> MeshState::GetCellsNumNodes() const
{
	vector<size_t> cells_num_nodes(m_num_cells);
	size_t cell_idx = 0;
	for (vector<int> cell_nodes : m_cells_nodes) {
		cells_num_nodes[cell_idx++] = cell_nodes.size();
	}
	return cells_num_nodes;
}

vector<size_t> MeshState::GetCellsNumWalls() const
{
	vector<size_t> cells_num_walls(m_num_cells);
	size_t cell_idx = 0;
	for (vector<int> cell_walls : m_cells_walls) {
		cells_num_walls[cell_idx++] = cell_walls.size();
	}
	return cells_num_walls;
}

void MeshState::SetCellsID(vector<int> values)
{
	if (values.size() != m_num_cells) {
		throw runtime_error("MeshState::SetCellsID(): Number of IDs doesn't match number of cells.");
	} else {
		m_cells_id = values;
	}
}

vector<int> MeshState::GetBoundaryPolygonNodes() const
{
	return m_boundary_polygon_nodes;
}

vector<int> MeshState::GetBoundaryPolygonWalls() const
{
	return m_boundary_polygon_walls;
}

void MeshState::SetBoundaryPolygonNodes(vector<int> node_ids)
{
	m_boundary_polygon_nodes = node_ids;
}

void MeshState::SetBoundaryPolygonWalls(vector<int> wall_ids)
{
	m_boundary_polygon_walls = wall_ids;
}

////////////////////////////////////////////////////////////////////////////////
// Walls
////////////////////////////////////////////////////////////////////////////////

size_t MeshState::GetNumWalls() const
{
	return m_num_walls;
}

void MeshState::SetNumWalls(size_t n)
{
	// Make sure that all containers are resized to hold n walls
	m_num_walls = n;
	m_walls_id.resize(n);
	m_walls_cells.resize(n);
	m_walls_nodes.resize(n);
	m_walls_attr.SetNum(n);
}

int MeshState::GetWallID(size_t wall_idx) const
{
	return m_walls_id.at(wall_idx);
}

pair<int, int> MeshState::GetWallNodes(size_t wall_idx) const
{
	return m_walls_nodes.at(wall_idx);
}

pair<int, int> MeshState::GetWallCells(size_t wall_idx) const
{
	return m_walls_cells.at(wall_idx);
}

void MeshState::SetWallID(size_t wall_idx, int value)
{
	m_walls_id.at(wall_idx) = value;
}

void MeshState::SetWallNodes(size_t wall_idx, pair<int, int> nodes_ids)
{
	m_walls_nodes.at(wall_idx) = nodes_ids;
}

void MeshState::SetWallCells(size_t wall_idx, pair<int, int> cells_ids)
{
	m_walls_cells.at(wall_idx) = cells_ids;
}

vector<int> MeshState::GetWallsID() const
{
	return m_walls_id;
}

void MeshState::SetWallsID(vector<int> values)
{
	if (values.size() != m_num_walls) {
		throw runtime_error("setWallsID(): Number of IDs doesn't match number of walls.");
	} else {
		m_walls_id = values;
	}
}

void MeshState::PrintToStream(ostream& out) const
{
	out << "MeshState:" << endl;

	////////////////////////////////////////////////////////////////////////////
	out << "Nodes:" << endl;
	out << setw(6) << "ID" << setw(16) << "X" << setw(16) << "Y";
	for (string name : NodeAttributeContainer().GetNames<int>()) {
		out << setw(20) << (name + " [i]");
	}
	for (string name : NodeAttributeContainer().GetNames<double>()) {
		out << setw(20) << (name + " [d]");
	}
	for (string name : NodeAttributeContainer().GetNames<string>()) {
		out << setw(20) << (name + " [s]");
	}
	out << endl;
	for (size_t idx = 0; idx < NodeAttributeContainer().GetNum(); ++idx) {
		out << setw(6) << m_nodes_id[idx];
		out << setw(16) << m_nodes_x[idx];
		out << setw(16) << m_nodes_y[idx];
		for (string name : NodeAttributeContainer().GetNames<int>()) {
			out << setw(20) << NodeAttributeContainer().Get<int>(idx, name);
		}
		for (string name : NodeAttributeContainer().GetNames<double>()) {
			out << setw(20) << NodeAttributeContainer().Get<double>(idx, name);
		}
		for (string name : NodeAttributeContainer().GetNames<string>()) {
			out << setw(20) << NodeAttributeContainer().Get<string>(idx, name);
		}
		out << endl;
	}

	////////////////////////////////////////////////////////////////////////////
	out << "Cells:" << endl;
	out << setw(6) << "ID";
	for (string name : CellAttributeContainer().GetNames<int>()) {
		out << setw(20) << (name + " [i]");
	}
	for (string name : CellAttributeContainer().GetNames<double>()) {
		out << setw(20) << (name + " [d]");
	}
	for (string name : CellAttributeContainer().GetNames<string>()) {
		out << setw(20) << (name + " [s]");
	}
	out << setw(8) << "Nodes / Walls" << endl;
	for (size_t idx = 0; idx < CellAttributeContainer().GetNum(); ++idx) {
		out << setw(6) << m_cells_id[idx];
		for (string name : CellAttributeContainer().GetNames<int>()) {
			out << setw(20) << CellAttributeContainer().Get<int>(idx, name);
		}
		for (string name : CellAttributeContainer().GetNames<double>()) {
			out << setw(20) << CellAttributeContainer().Get<double>(idx, name);
		}
		for (string name : CellAttributeContainer().GetNames<string>()) {
			out << setw(20) << CellAttributeContainer().Get<string>(idx, name);
		}
		out << "   [ ";
		for (const int node_id : m_cells_nodes[idx]) {
			out << node_id << " ";
		}
		out << "] / [ ";
		for (const int wall_id : m_cells_walls[idx]) {
			out << wall_id << " ";
		}
		out << "]" << endl;
	}

	////////////////////////////////////////////////////////////////////////////
	out << "Boundary polygon : Nodes / Walls" << endl;
	out << "   [ ";
	for (const int node_id : m_boundary_polygon_nodes) {
		out << node_id << " ";
	}
	out << "] / [ ";
	for (const int wall_id : m_boundary_polygon_walls) {
		out << wall_id << " ";
	}
	out << "]" << endl;

	////////////////////////////////////////////////////////////////////////////
	out << "Walls:" << endl;
	out << setw(6) << "ID" <<
		setw(8) << "cell_1" << setw(8) << "cell_2"
		<< setw(8) << "node_1" << setw(8) << "node_2";
	for (string name : WallAttributeContainer().GetNames<int>()) {
		out << setw(20) << (name + " [i]");
	}
	for (string name : WallAttributeContainer().GetNames<double>()) {
		out << setw(20) << (name + " [d]");
	}
	for (string name : WallAttributeContainer().GetNames<string>()) {
		out << setw(20) << (name + " [s]");
	}
	out << endl;
	for (size_t idx = 0; idx < WallAttributeContainer().GetNum(); ++idx) {
		out << setw(6) << m_walls_id[idx];
		out << setw(8) << m_walls_cells[idx].first;
		out << setw(8) << m_walls_cells[idx].second;
		out << setw(8) << m_walls_nodes[idx].first;
		out << setw(8) << m_walls_nodes[idx].second;
		for (string name : WallAttributeContainer().GetNames<int>()) {
			out << setw(20) << WallAttributeContainer().Get<int>(idx, name);
		}
		for (string name : WallAttributeContainer().GetNames<double>()) {
			out << setw(20) << WallAttributeContainer().Get<double>(idx, name);
		}
		for (string name : WallAttributeContainer().GetNames<string>()) {
			out << setw(20) << WallAttributeContainer().Get<string>(idx, name);
		}
		out << endl;
	}
}

}
