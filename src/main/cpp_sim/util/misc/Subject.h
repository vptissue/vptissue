#ifndef SUBJECT_H_INCLUDED
#define SUBJECT_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface/Implementation of Subject.
 */

#include <functional>
#include <map>
#include <memory>
#include <string>

namespace SimPT_Sim {
namespace Util {

/**
 * Subject in Observer pattern.
 */
template <typename E, typename K = void>
class Subject {
public:
	typedef E                                      EventType;
	typedef const K*                               KeyType;
	typedef std::function<void(const EventType&)>  CallbackType;

	virtual ~Subject()
	{
		UnregisterAll();
	}

	template<typename U>
	void Register(const U*, CallbackType);

	template <typename U>
	void Unregister(const U*);

	void UnregisterAll();

	void Notify(const EventType&);

private:
	typedef std::less<KeyType>  Compare;

	std::map<KeyType, CallbackType, Compare> m_observers;
};


//template<typename E, typename K>
//template<typename U>
//void Subject<E, K>::Register(const U* u, CallbackType f)
//{
//	m_observers.insert(make_pair(static_cast<KeyType>(u), f));
//}
//
//template <typename E, typename K>
//template <typename U>
//void Subject<E, K>::Unregister(const U* u)
//{
//	m_observers.erase(static_cast<KeyType>(u));
//}
//
//template<typename E, typename K>
//void Subject<E, K>::UnregisterAll()
//{
//	m_observers.clear();
//}
//
//template<typename E, typename K>
//void Subject<E, K>::Notify(const EventType& e)
//{
//	for (auto const& o : m_observers) {
//		(o.second)(e);
//	}
//}


/**
 * Partial specialization for weak_ptr<void> key type.
 */
template <typename E>
class Subject<E, std::weak_ptr<const void>>
{
public:
	typedef E                                      EventType;
	typedef std::weak_ptr<const void>              KeyType;
	typedef std::function<void(const EventType&)>  CallbackType;


	virtual ~Subject()
	{
		UnregisterAll();
	}

	template <typename U>
	void Register(const std::shared_ptr<U>&, CallbackType);

	template <typename U>
	void Unregister(const std::shared_ptr<U>&);

	void UnregisterAll();

	void Notify(const EventType&);

private:
	typedef std::owner_less<KeyType>  Compare;

	std::map<KeyType, CallbackType, Compare> m_observers;
};


template<typename E>
template<typename U>
void Subject<E, std::weak_ptr<const void>>::Register(const std::shared_ptr<U>& u, CallbackType f)
{
	m_observers.insert(make_pair(std::static_pointer_cast<const void>(u), f));
}

template <typename E>
template <typename U>
void Subject<E, std::weak_ptr<const void>>::Unregister(const std::shared_ptr<U>& u)
{
	m_observers.erase(std::static_pointer_cast<const void>(u));
}

template<typename E>
void Subject<E, std::weak_ptr<const void>>::UnregisterAll()
{
	m_observers.clear();
}

template<typename E>
void Subject<E, std::weak_ptr<const void>>::Notify(const EventType& e)
{
	for (auto const& o : m_observers) {
		auto spt = o.first.lock();
		if (spt) {
			(o.second)(e);
		} else {
			// JB the statement below causes frequent crashes on Mac OS X
			// As it's just a clean up, I've commented it out
			//auto it = m_observers.erase(o.first);
		}
	}
}

} // namespace
} // namespace

#endif
