#ifndef INC_UTIL_EXCEPTION_H
#define INC_UTIL_EXCEPTION_H
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Header file for Exception class.
 */
#include <string>
#include <stdexcept>

namespace SimPT_Sim {
namespace Util {

/// Extremely simple Exception root class.
class Exception: public std::exception
{
public:
	/// Virtual destructor for abstract class.
	virtual ~Exception() noexcept {}

	/// Constructor initializes message for the exception.
	Exception(std::string const& m) : m_msg(m) { }

	/// Return error message
	virtual const char* what() const noexcept { return m_msg.c_str(); }

private:
	std::string m_msg;
};

} // namespace
} // namespace

#endif // end-of-include-guard

