#ifndef GNUC_VERSION_H_INCLUDED
#define GNUC_VERSION_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Macros to determine GCC major, minor and patch version.
 */

#if defined(__GNUC__)
# if defined(__GNUC_PATCHLEVEL__)
#  define GNUC_VERSION (__GNUC__ * 10000 \
                            + __GNUC_MINOR__ * 100 \
                            + __GNUC_PATCHLEVEL__)
# else
#  define GNUC_VERSION (__GNUC__ * 10000 \
                            + __GNUC_MINOR__ * 100)
# endif
#endif

#endif // end-of-include-guard
