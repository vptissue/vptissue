#ifndef UTIL_CLOCK_MAN_CLOCK_TRAITS_H_INCLUDED
#define UTIL_CLOCK_MAN_CLOCK_TRAITS_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for ClockTraits.
 */

#include "ClockCLib.h"
#include "CumulativeRecords.h"
#include "IndividualRecords.h"
#include "Stopwatch.h"

#include <chrono>

namespace SimPT_Sim {
namespace ClockMan {

/**
 * Types related to timekeeping.
 */
template<typename C = std::chrono::system_clock, typename D = typename C::duration>
class ClockTraits
{
public:
	/// Type for clock.
	using Clock = C;

	/// Stopwatch to measure time durations.
	using Stopclock = SimPT_Sim::ClockMan::Stopwatch<C>;

	/// Type for time duration units.
	using Duration = D;

	/// Records with cumulative timing info.
	using CumulativeTimings = SimPT_Sim::ClockMan::CumulativeRecords<D>;

	/// Records with individual timing info.
	using IndividualTimings = SimPT_Sim::ClockMan::IndividualRecords<D>;

};

} // namespace
} // namespace

#endif // end-of-include-guard
