/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Rgb color def to Hsv color def: r,g,b values are from 0 to 1
 * and  h = [0,360], s = [0,1], v = [0,1] (if s == 0, then h = -1)
 */

#include "Rgb2Hsv.h"

#include <array>
#include <cmath>

namespace SimPT_Sim {
namespace Color {

using namespace std;

array<double, 3> Rgb2Hsv(double r, double g, double b)
{
        array<double, 3> ret_val {{0.0, 0.0, 0.0}};
        const auto mini = min( r, min(g, b) );
        const auto maxi = max( r, max(g, b) );
        const auto delta = maxi - mini;

        if ( maxi != 0.0 ) {
                const auto v = maxi;
                const auto s = delta / maxi;
                auto h = -1.0;

                if( r == maxi ) {
                        h = ( g - b ) / delta;           // between yellow & magenta
                } else if( g == maxi ) {
                        h = 2.0 + ( b - r ) / delta;     // between cyan & yellow
                } else {
                        h = 4.0 + ( r - g ) / delta;     // between magenta & cyan
                }
                h *= 60.0;                               // degrees
                if( h < 0.0 ) {
                        h += 360.0;
                }
                ret_val = {{ h, s, v}};
        } else {
                ret_val = {{-1.0, 0.0, 0.0}};
        }

        return ret_val;
}

} // namespace
} // namespace

