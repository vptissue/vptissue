#ifndef ODEINT_TRAITS_H_
#define ODEINT_TRAITS_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Odeint traits header file.
 */

#include <functional>
#include <vector>

namespace SimPT_Sim {

/**
 * Contains an odeint system, a state and a solver for it.
 */
template<typename S = std::vector<double>>
class OdeintTraits
{
public:
	using  State  = S;
	using  System = std::function<void (const State&, State&, const double)>;
	using  Solver = std::function<void (System, State&, double, double, double)>;
};

} // namespace

#endif // end_of_include_guard
