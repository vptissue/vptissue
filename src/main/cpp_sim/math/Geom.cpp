/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of Geom.
 */

#include "Geom.h"

#include "math/math.h"
#include <cmath>

namespace {

inline constexpr double tiny() { return 1.0e-5; }

}

using namespace std;

namespace SimPT_Sim {

tuple<double, double, double> Geom::CircumCircle(double x1, double y1, double x2, double y2, double x3, double y3)
{
	double xc;
	double yc;

	if (abs(y2 - y1) < tiny()) {
		const double m2   = -(x3 - x2) / (y3 - y2);
		const double mx2  = (x2 + x3) / 2.0;
		const double my2  = (y2 + y3) / 2.0;
		xc   = (x2 + x1) / 2.0;
		yc   = m2 * (xc - mx2) + my2;
	} else if (abs(y3 - y2) < tiny()) {
		const double m1   = -(x2 - x1) / (y2 - y1);
		const double mx1  = (x1 + x2) / 2.0;
		const double my1  = (y1 + y2) / 2.0;
		xc   = (x3 + x2) / 2.0;
		yc   = m1 * (xc - mx1) + my1;
	} else {
		const double m1   = -(x2 - x1) / (y2 - y1);
		const double m2   = -(x3 - x2) / (y3 - y2);
		const double mx1  = (x1 + x2) / 2.0;
		const double mx2  = (x2 + x3) / 2.0;
		const double my1  = (y1 + y2) / 2.0;
		const double my2  = (y2 + y3) / 2.0;
		xc   = (m1 * mx1 - m2 * mx2 + my2 - my1) / (m1 - m2);
		yc   = m1 * (xc - mx1) + my1;
	}

	const double dx = x2 - xc;
	const double dy = y2 - yc;
	const double r = sqrt(dx * dx + dy * dy);

	return make_tuple(xc, yc, r);
}

}
