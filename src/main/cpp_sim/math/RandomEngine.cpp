/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of RandomEngine.
 */

#include "RandomEngine.h"

#include "util/clock_man/TimeStamp.h"

using namespace std;
using namespace SimPT_Sim::ClockMan;
using namespace SimPT_Sim::RandomEngineType;
using namespace boost::property_tree;
using boost::optional;

namespace SimPT_Sim {

RandomEngine::RandomEngine()
	: m_seed(1U), m_type_id(TypeId::mrg2)
{
	// Create as many random number generators of each type as the number of
	// threads; or 1 if no OpenMP support.
	const size_t nthreads = 1;

	this->m_lcg64.resize(nthreads);
	this->m_lcg64_shift.resize(nthreads);
	this->m_mrg2.resize(nthreads);
	this->m_mrg3.resize(nthreads);
	this->m_yarn2.resize(nthreads);
	this->m_yarn3.resize(nthreads);

	SetState(m_seed);
}

RandomEngineType::Info RandomEngine::GetInfo() const
{
	Info info;
	stringstream ss;

	switch (m_type_id) {
	case TypeId::lcg64 :
		ss << m_lcg64[0]; break;
	case TypeId::lcg64_shift  :
		ss << m_lcg64_shift[0]; break;
	case TypeId::mrg2 :
		ss << m_mrg2[0]; break;
	case TypeId::mrg3 :
		ss << m_mrg3[0]; break;
	case TypeId::yarn2 :
		ss << m_yarn2[0]; break;
	case TypeId::yarn3 :
		ss << m_yarn3[0]; break;
	}

	info.seed   = m_seed;
	info.state  = ss.str();
	info.type   = ToString(m_type_id);
	return info;
}

bool RandomEngine::Reinitialize()
{
	m_type_id = TypeId::mrg2;
	m_seed    = 1U;
	SetState(m_seed);
	return true;
}

bool RandomEngine::Reinitialize(const ptree& pt)
{
	bool status = false;
	const string type = pt.get<string>("type");
	if(IsExisting(type)) {
		m_type_id = FromString(type);
		optional<unsigned long> seed = pt.get_optional<unsigned long>("seed");
		if (seed) {
			m_seed = seed.get();
		} else {
			// Apparently std::random_device not non-deterministic on
			// some platforms, so we use a reading of the wall-clock.
			m_seed = static_cast<unsigned long>(TimeStamp().ToTimeT());
		}
		optional<const ptree&> re_pt = pt.get_child_optional("state");
		if (re_pt) {
			SetState(re_pt.get().get_value<string>());
		} else {
			SetState(m_seed);
		}
		status = true;
	}
	return status;
}

/**
 * Seed a vector of random number generators and splits in 'n' parallel streams
 * using the leapfrog scheme, where 'n' is the size of the vector 'rngs'.
 * This function is used as a quick utility routine in RandomEngine::SetState
 * and might be replaced in the future with a C++14 polymorphic lambda.
 */
template <typename T>
void para_seed(std::vector<T>& rngs, const unsigned long seed)
{
	// n is equal to the number of OpenMP worker threads, i.e: the number
	// of parallel RNG streams we require
	const size_t n = rngs.size();
	// Seed all RNGs with 'seed' & divide in 'n' streams
	for (size_t i = 0; i < n; ++i) {
		rngs[i].seed(seed);
		rngs[i].split(n, i);
	}
}

void RandomEngine::SetState(unsigned long seed)
{
	switch (m_type_id) {
	case TypeId::lcg64 :
		para_seed(m_lcg64, seed); break;
	case TypeId::lcg64_shift  :
		para_seed(m_lcg64_shift, seed); break;
	case TypeId::mrg2 :
		para_seed(m_mrg2, seed); break;
	case TypeId::mrg3 :
		para_seed(m_mrg3, seed); break;
	case TypeId::yarn2 :
		para_seed(m_yarn2, seed); break;
	case TypeId::yarn3 :
		para_seed(m_yarn3, seed); break;
	}
}

/**
 * Seed a vector of random number generators and splits in 'n' parallel streams
 * using the leapfrog scheme, where 'n' is the size of the vector 'rngs'.
 * This function is used as a quick utility routine in RandomEngine::SetState
 * and might be replaced in the future with a C++14 polymorphic lambda.
 */
template <typename T>
void para_seed(std::vector<T>& rngs, const std::string& state)
{
	stringstream ss(state);
	// n is equal to the number of OpenMP worker threads, i.e: the number
	// of parallel RNG streams we require
	const size_t n = rngs.size();
	// Seed all RNGs with 'seed' & divide in 'n' streams
	for (size_t i = 0; i < n; ++i) {
		ss >> rngs[i];
		rngs[i].split(n, i);
	}
}

void RandomEngine::SetState(const string& state)
{
	switch (m_type_id) {
	case TypeId::lcg64 :
		para_seed(m_lcg64, state); break;
	case TypeId::lcg64_shift  :
		para_seed(m_lcg64_shift, state); break;
	case TypeId::mrg2 :
		para_seed(m_mrg2, state); break;
	case TypeId::mrg3 :
		para_seed(m_mrg3, state); break;
	case TypeId::yarn2 :
		para_seed(m_yarn2, state); break;
	case TypeId::yarn3 :
		para_seed(m_yarn3, state); break;
	}
}

} // namespace




