#ifndef NODE_MOVER_H_INCLUDED
#define NODE_MOVER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for NodeMover.
 */

//#include "math/CSRMatrix.h"
#include "model/ComponentInterfaces.h"
#include "sim/CoreData.h"

#include <boost/property_tree/ptree.hpp>
#include <array>
#include <map>
#include <memory>
#include <set>
#include <unordered_set>
#include <vector>

//#define EDGE_BASED_INC

namespace SimPT_Sim {

class Cell;
class Mesh;
class Node;
class Sim;

/**
 * Cell mechanics by displacement and insertion of nodes.
 */
class NodeMover
{
public:
        /**
         * Information gathered to evaluat effects of Metropolis Algorithm.
         */
	struct MetropolisInfo
	{
		MetropolisInfo(unsigned int dc = 0U, unsigned int uc = 0U, double dd = 0.0, double ud = 0.0)
			: down_count(dc), up_count(uc), down_dh(dd), up_dh(ud)
		{}
		unsigned int   down_count;
		unsigned int   up_count;
		double         down_dh;
		double         up_dh;

		MetropolisInfo& operator+=(const MetropolisInfo& other)
                {
		        down_count   += other.down_count;
		        up_count     += other.up_count;
		        down_dh      += other.down_dh;
		        up_dh        += other.up_dh;
		        return *this;
                }
	};

	/**
	 * Information related to (potential) move of a node in the mesh.
	 */
	struct MoveInfo
	{
	        MoveInfo(Node* no = nullptr, bool da = false, bool ma = false,
	                double sd = 0.0, std::array<double, 3> d = {{0.0, 0.0, 0.0}})
	                : node(no), dh_accept(da), mc_accept(ma), dh(sd), displacement(d)
	        {}
		Node*                   node;
		bool                    dh_accept;
		bool                    mc_accept;
		double                  dh;
		std::array<double,3>    displacement;
	};

public:
	/// Straight initialization of empty object.
	NodeMover();

	/// Initializing constructor.
	NodeMover(const CoreData& cd);

	/// Get current energy for the whole tissue.
	double GetTissueEnergy() const { return m_tissue_energy; }

	/// Initializes based on values in property tree.
	void Initialize(const CoreData& cd);

	/// Sweep over all nodes, do metropolis move for each of them.
	MetropolisInfo SweepOverNodes(double step_size, double temperature);

//        /// Sweep over all nodes, do metropolis move for each of them.
//	/// Uses parallization based on node-cell-node incidence.
//        MetropolisInfo SweepOverNodes(const CSRMatrix& nci, double step_size, double temperature);
//
//	/// Sweep over all nodes, do metropolis move for each of them.
//	/// Uses parallelization based on node-edge-node incidence and
//        /// can only be used with the DeltaHamiltonian approach.
//	MetropolisInfo SweepOverNodes(const CSRMatrix& nci, const CSRMatrix& nei, double step_size, double temperature);
//
	/// Using the delta-hamiltonian.
	inline bool UsesDeltaHamiltonian() const { return m_delta_hamiltonian != nullptr; }

private:
	/// Calculate cell energies (situation where full Hamiltonian will be used).
	void CalculateCellEnergies();

	/// Calculate cell energies (situation where DeltaHamiltonian will be used).
	void CalculateCellEnergiesDelta();

//        /// Calculate lookahead blocks with node-cell-node incidence.
//        std::vector<size_t> GetLookaheadsByCells(const std::vector<unsigned int>& indices, const CSRMatrix& nci) const;
//
//        /// Calculate lookahead blocks with node-edge-node incidence.
//        std::vector<size_t> GetLookaheadsByEdges(const std::vector<unsigned int>& indices, const CSRMatrix& nni) const;

	/// Is move non self-intersecting
	bool IsNonIntersecting(Node* node, const std::array<double, 3>& move) const;

	/// Move a single node using hamiltonian.
	MetropolisInfo MoveNode(Node* n, double step_size, double temperature);

	/// Move a single node using delta_hamiltonian.
	MetropolisInfo MoveNodeDelta(Node* n, double step_size, double temperature);

	/// Compute the MoveInfo (data related to criteria for accepting the move).
	MoveInfo MoveAcceptance(Node* node, double step_size, double temperature);

	/// Execute move if acceptable for Metropolis and geometry constraints.
	MetropolisInfo MoveExecution(const MoveInfo& move);

private:
	CoreData                        m_cd;                 ///< Core data (mesh, params, sim_time,...).
	std::map<Cell*, double>         m_cell_energies;      ///<
	HamiltonianComponent            m_hamiltonian;        ///<
	DeltaHamiltonianComponent       m_delta_hamiltonian;  ///<
	Mesh*                           m_mesh;               ///< Bare pointer to mesh for convenience.
	double                          m_tissue_energy;      ///<

private:
	/// Generates moves for MC algorithm.
	/// A vector to enable parallelism (in sequential calculation only first element is used).
	std::vector<std::function<std::array<double,3>()> >  m_mc_move_generator;

	/// Used to determine displacement vector.
	/// A vector to enable parallelism (in sequential calculation only first element is used).
	std::vector<std::function<double()> >                m_normal_generator;

	///< Used in Metropolis displacement acceptance criterion.
	/// A vector to enable parallelism (in sequential calculation only first element is used).
	std::vector<std::function<double()> >                m_uniform_generator;
};

} // namespace

#endif // end_of_include_guard
