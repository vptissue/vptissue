#ifndef CELL_HOUSEKEEPER_H_INCLUDED
#define CELL_HOUSEKEEPER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for CellHousekeeper.
 */

#include "model/ComponentInterfaces.h"
#include "sim/CoreData.h"

namespace SimPT_Sim {

class Sim;

/**
 * Class for handling maintenance of cell mechanics (cell housekeeping).
 */
class CellHousekeeper
{
public:
	/// Default constructor
	CellHousekeeper();

	/// Default constructor
	CellHousekeeper(const CoreData& cd);

	/// Housekeeping rules.
	boost::property_tree::ptree Exec() const;

	/// Initializes.
	void Initialize(const CoreData& cd);

private:
	CoreData                      m_cd;                ///< Core data (mesh, params, sim_time,...).
	CellHousekeepComponent        m_cell_housekeep;
};

} // namespace

#endif // end_of_include_guard
