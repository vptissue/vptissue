#ifndef ICOUPLER_H_INCLUDED
#define ICOUPLER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for ICoupler.
 */

#include <memory>
#include <boost/property_tree/ptree.hpp>

namespace SimPT_Sim {

class Sim;
struct CoreData;

/**
 * Interface for the classes coupling two parallel simulations.
 */
class ICoupler
{
public:
	/**
	 * Virtual destructor.
	 */
	virtual ~ICoupler() {}


	/**
	 * Initializes the two sides of the coupler.
	 * @param	parameters	The parameters for the coupler.
	 * @param	from		The first Sim of the coupling.
	 * @param	to		The second Sim of the coupling.
	 */
	virtual void Initialize(
		const boost::property_tree::ptree& parameters,
		const CoreData& from,
		const CoreData& to) = 0;

	/**
	 * Executes the coupler.
	 */
	virtual void Exec() = 0;
};

} // namespace

#endif // end-of-include-guard
