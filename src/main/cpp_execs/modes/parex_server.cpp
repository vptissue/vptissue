/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of main for parameter exploration server.
 */

#include "parex_server_mode.h"

#include "parex_server/Server.h"

#include <QCoreApplication>
#include <tclap/CmdLine.h>
#include <tclap/ValueArg.h>

namespace Modes {

int ParexServerMode::operator()(int argc, char** argv)
{
	QCoreApplication app(argc,argv);

	TCLAP::CmdLine cmdLine("SimPT Parex Server");
	TCLAP::ValueArg<int> nodesArg(
		"n", "nodes", "The minimum number of nodes that a server needs to connect with.",
		false, 0, "MIN_NODES", cmdLine);
	TCLAP::ValueArg<int> portArg(
		"p", "port", "The port used by clients to connect to.", false, 8888, "PORT", cmdLine);
	cmdLine.parse(argc, argv);

	const int min_nodes     = nodesArg.getValue();
	const int port_number   = portArg.getValue();
	new SimPT_Parex::Server(min_nodes, port_number, &app);

	return app.exec();
}

} // namespace
