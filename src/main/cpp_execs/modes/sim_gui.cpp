/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of main for simulator with gui.
 */

#include "sim_gui_mode.h"

#include "common/InstallDirs.h"
#include "gui/AppCustomizer.h"
#include "gui/controller/AppController.h"
#include "session/SimSession.h"
#include "util/clock_man/TimeStamp.h"
#include "util/revision/RevisionInfo.h"

#include <QApplication>
#include <QMessageBox>
#include <QToolTip>

#include <iostream>
#include <stdexcept>

namespace Modes {

using namespace std;
using namespace SimPT_Sim::ClockMan;
using namespace SimPT_Sim::Util;
using namespace SimPT_Shell;
using namespace SimShell::Gui::Controller;


int simPTGUIMode::operator()(int argc, char **argv)
{
	int exit_status = EXIT_SUCCESS;

	// qApp: closing last window quits app and exits app process.
	QApplication app(argc, argv, true);
	qRegisterMetaType<std::string>("std::string");
	QObject::connect(qApp, SIGNAL(lastWindowClosed()), qApp, SLOT(quit()));

	try {
		// Go ...
		cout << "simPT_sim starting up in gui mode at: " << TimeStamp().ToString() << endl;
		cout << "Executing " << argv[0]<< endl << endl;
		cout << "Revision: " << RevisionInfo::CompoundId() << endl;

		// Set icon search path
		QStringList search_paths = QIcon::themeSearchPaths();
		search_paths.push_back(QString::fromStdString(SimShell::InstallDirs::GetDataDir() + "/icons"));
		QIcon::setThemeSearchPaths(search_paths);
		QIcon::setThemeName("Tango");

		// Preliminary graphics objects
		QPalette tooltippalette = QToolTip::palette();
		QColor transparentcolor = QColor(tooltippalette.brush(QPalette::Window).color());
		tooltippalette.setBrush(QPalette::Window, QBrush(transparentcolor));
		QToolTip::setPalette(tooltippalette);
		qApp->setStyleSheet("QToolTip { color: red; border: 2px solid gray; }");

		// Main app components
		auto factory = make_shared<Gui::AppCustomizer>();
		AppController controller(factory);
		controller.setVisible(true);

		// Execute the app
		exit_status = app.exec();

		// Done ...
		cout << endl << controller.GetTimings() << endl << endl;
		cout << "simPT_sim exiting at: " << TimeStamp().ToString() << endl << endl;
	}
	catch (exception& e) {
		cerr << e.what() << endl;
		QString qmess = QString("Exception:\n%1\n").arg(e.what());
		QMessageBox::critical(0, "Critical Error", qmess,
					QMessageBox::Abort, QMessageBox::NoButton, QMessageBox::NoButton);
		exit_status = EXIT_FAILURE;
	}
	catch (...) {
		cerr << "Unknown exception." << endl;
		QString qmess = QString("Unknown exception.");
		QMessageBox::critical(0, "Critical Error", qmess);
		exit_status = EXIT_FAILURE;
	}

	return exit_status;
}

} // namespace

