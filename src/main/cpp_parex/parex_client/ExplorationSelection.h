#ifndef SIMPT_PAREX_EXPLORATION_SELECTION_H_INCLUDED
#define SIMPT_PAREX_EXPLORATION_SELECTION_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for ExplorationSelection
 */

#include <QObject>
#include <QDialog>
#include <QStatusBar>
#include <QListView>

#include <vector>

namespace SimPT_Parex {

/**
 * Dialog for selecting explorations
 */
class ExplorationSelection : public QDialog
{
	Q_OBJECT
public:
	/// Constructor
	ExplorationSelection(QWidget* parent = 0);

	/// Constructor
	ExplorationSelection(bool single, QWidget* parent = 0);

	void DrawGui();

	/// Destructor
	virtual ~ExplorationSelection();

	/// Get the name of the selected exploration
	std::vector<std::string> GetSelection();

	///Return true if an exploration is selected
	bool Selected();

private:
	std::vector<std::string>	m_selected_exploration;
	bool				m_single_selection;

	QListView*		m_exploration_widget;
	QPushButton*		m_ok_button;

private slots:
	/// Accept dialog
	void Accept();

public slots:
	/// updates the current displayed explorations
	void UpdateExplorations(std::vector<std::string> explorations);
};

} // namespace

#endif // end-of-include-guard
