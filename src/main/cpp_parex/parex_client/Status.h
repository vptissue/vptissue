#ifndef SIMPT_PAREX_STATUS_H_INCLUDED
#define SIMPT_PAREX_STATUS_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for Status
 */

#include "parex_protocol/ExplorationProgress.h"

#include <memory>
#include <QWidget>

class QTextEdit;

namespace SimPT_Parex {

/**
 * Widget to display status of subscribed exploration
 */
class Status : public QWidget
{
public:
	/// Constructor
	Status();

	/// Destructor
	virtual ~Status() {}

	/// Update the status of an exploration
	void UpdateStatus(const std::shared_ptr<const ExplorationProgress>& progress);

	/// Clear the current displayed exploration
	void Clear();

	/// Set font to connected state
	void Connected();

	/// Set font to disconnected state
	void Disconnected();

private:
	/// Update the status view
	void UpdateView();

private:
	std::shared_ptr<const ExplorationProgress>  m_progress;
	QTextEdit*                                  m_view;
};

} // namespace

#endif // end_of_include_guard
