/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for FilesPage.
 */

#include "FilesPage.h"

#include "parex_protocol/FileExploration.h"
#include "parex_protocol/ListSweep.h"
#include "parex_protocol/ParameterExploration.h"
#include "parex_protocol/RangeSweep.h"

#include <boost/property_tree/exceptions.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <QComboBox>
#include <QFileDialog>
#include <QLabel>
#include <QListView>
#include <QListWidget>
#include <QMessageBox>
#include <QPushButton>
#include <QRadioButton>
#include <QSettings>
#include <QString>
#include <QVBoxLayout>

#include <cctype>
#include <locale>
#include <memory>

using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;

namespace SimPT_Parex {

FilesPage::FilesPage(std::shared_ptr<Exploration>& exploration, boost::property_tree::ptree& preferences)
	: m_exploration(exploration), m_preferences(preferences), m_files(), m_files_widget(nullptr), m_remove_button(nullptr)
{
	setTitle("Select files");
	setSubTitle("Select all the files for the exploration.");

	QVBoxLayout *layout = new QVBoxLayout;

	m_files_widget = new QListWidget;
	m_files_widget->setSelectionMode(QListWidget::MultiSelection);
	connect(m_files_widget, SIGNAL(itemSelectionChanged()), this, SLOT(UpdateRemoveDisabled()));
	layout->addWidget(m_files_widget);

	QHBoxLayout *buttonsLayout = new QHBoxLayout;

	QPushButton *addButton = new QPushButton("Add...");
	connect(addButton, SIGNAL(clicked()), this, SLOT(AddFiles()));
	buttonsLayout->addWidget(addButton);

	m_remove_button = new QPushButton("Remove");
	connect(m_remove_button, SIGNAL(clicked()), this, SLOT(RemoveFiles()));
	buttonsLayout->addWidget(m_remove_button);

	layout->addLayout(buttonsLayout);

	setLayout(layout);
}


void FilesPage::UpdateRemoveDisabled()
{
	m_remove_button->setEnabled(!m_files_widget->selectedItems().isEmpty());
}


void FilesPage::AddFiles()
{
	QSettings settings;
	QString path;
	if (settings.contains("last_leaf_file"))
		path = settings.value("last_leaf_file").toString();
	else
		path = settings.value("workspace").toString();

	QStringList files = QFileDialog::getOpenFileNames(this, "Browse files", path);

	if (files.empty())
		return;

	for (auto file : files) {
		if (!QFile(file).exists())
			return;

		try {
			ptree pt;
			read_xml(file.toStdString(), pt, trim_whitespace);

			auto found = m_files.find(file);
			if (found != m_files.end()) {
				QMessageBox::warning(this, "Already added",
				        "The file '" + file + "' was already added.\nNot adding it again.", QMessageBox::Ok);
			} else {
				m_files.insert(found, std::make_pair(file, pt));
				m_files_widget->addItem(new QListWidgetItem(file));
			}
		} catch (ptree_bad_path& e) {
			QMessageBox::critical(this, "Read Error", "Error in file", QMessageBox::Ok);
			return;
		} catch (ptree_bad_data& e) {
			QMessageBox::critical(this, "Read Error", "Error in file.", QMessageBox::Ok);
			return;
		} catch (xml_parser_error& e) {
			QMessageBox::critical(this, "Error", "Not a valid file.", QMessageBox::Ok);
			return;
		} catch (std::exception& e) {
			QMessageBox::critical(this, "Error", "Could not read file.", QMessageBox::Ok);
			return;
		} catch (...) {
			QMessageBox::critical(this, "Error", "Unknown Exception", QMessageBox::Ok);
			return;
		}
	}

	settings.setValue("last_leaf_file", files.first());

	completeChanged();
}


void FilesPage::RemoveFiles()
{
	if (QMessageBox::warning(this, "Remove files", "Are you sure you want to remove the selected item(s)?", QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes) {
		auto selectedItems = m_files_widget->selectedItems();
		for (auto selectedItem : selectedItems) {
			auto selectedIt = m_files.find(selectedItem->text());
			assert(selectedIt != m_files.end() && "Selected item should be in files map");

			m_files.erase(selectedIt);
			delete selectedItem;
		}

		UpdateRemoveDisabled();

		completeChanged();
	}
}


bool FilesPage::isComplete() const
{
	return m_files_widget->count() > 0;
}


bool FilesPage::validatePage()
{
	assert(!m_files.empty() && "m_files cannot be empty");

	std::vector<std::pair<std::string, ptree>> files;

	for (int i = 0; i < m_files_widget->count(); ++i) {
		auto item = m_files_widget->item(i);
		files.push_back(std::make_pair(item->text().toStdString(), m_files.at(item->text())));
	}
	m_exploration = std::make_shared<FileExploration>("Untitled", files, m_preferences);

	return true;
}


} // namespace
