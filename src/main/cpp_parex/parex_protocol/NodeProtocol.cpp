/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for NodeProtocol
 */

#include "NodeProtocol.h"

#include "SimResult.h"
#include "SimTask.h"

using boost::property_tree::ptree;

namespace SimPT_Parex {

NodeProtocol::NodeProtocol(QTcpSocket *s, QObject *parent)
	: Protocol(s, parent)
{
}

NodeProtocol::~NodeProtocol()
{
}

void NodeProtocol::SendSimResult(const SimResult &result)
{
	ptree writer;
	writer.put("SimResult.Exploration", result.GetExplorationName());
	writer.put("SimResult.Id", result.GetTaskId());
	writer.put("SimResult.Success", static_cast<int>(result.GetResult()));

	SendPtree(writer);
}

void NodeProtocol::ReceivePtree(const ptree &reader)
{
	if (reader.find("SimTask") != reader.not_found()) {
		int task_id = reader.get<int>("SimTask.Id");
		std::string tree = reader.get<std::string>("SimTask.Tree");
		std::string workspace = reader.get<std::string>("SimTask.Workspace");
		std::string name = reader.get<std::string>("SimTask.Exploration");

		emit TaskReceived(new SimTask(task_id, tree, workspace, name));
	} else if (reader.find("SimResult") != reader.not_found()) {
		std::string tree = reader.get<std::string>("SimResult.Ack");
		emit SuccessfullySent();
	} else if (reader.find("Control") != reader.not_found()) {
		ptree control_pt = reader.get_child("Control");
		if (control_pt.find("Stop") != control_pt.not_found()) {
			emit StopTask();
		} else if (control_pt.find("Delete") != control_pt.not_found()){
			std::string name = control_pt.get<std::string>("Delete");
			emit Delete(name);
		}
	}
}

} // namespace
