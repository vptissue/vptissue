#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################

import unittest
import simPT

class UnitSimState(unittest.TestCase):
    def setUp(self):
        self.state = simPT.SimState()
        
    def testMeshState(self):
        self.state.SetMeshState(simPT.MeshState())
        self.assertIsInstance(self.state.GetMeshState(), simPT.MeshState)
        
    def testProjectName(self):
        self.state.SetProjectName("test")
        self.assertEqual(self.state.GetProjectName(), "test")
        
    def testTime(self):
        self.state.SetTime(60.0)
        self.assertEqual(self.state.GetTime(), 60.0)
        
    def testTimeStep(self):
        self.state.SetTimeStep(10)
        self.assertEqual(self.state.GetTimeStep(), 10)