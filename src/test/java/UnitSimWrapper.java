/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package be.ac.ua.simPT.test;

import org.junit.*;
import static org.junit.Assert.*;

import be.ac.ua.simPT.*;
import java.io.StringReader;
import javax.xml.parsers.*;
import javax.xml.xpath.*;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

public class UnitSimWrapper {
	private SimWrapper sim;
	private String path;

	@Before
	public void setUp() {
		// Adding the libsimnPT_sim library is only required on windows
		if (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0)
			System.loadLibrary("libsimPTlib_sim");
		
		System.loadLibrary("simPT_sim_jni");
		sim = new SimWrapper();
		path = "../data/simPT_Default_workspace";
	}

	@Test
	public void testTimeStep() {
		{
			ResultVoid r = sim.Initialize(path + "/TipGrowth/leaf_000000.xml");
			assertTrue(r.getStatus() == SimWrapperStatus.SUCCESS);
			for (int i = 0; i < 10; i++) {
				sim.TimeStep();
			}
		}
		{
			ResultSimState r = sim.GetState();
			assertTrue(r.getStatus() == SimWrapperStatus.SUCCESS);
			assertEquals(10, r.getValue().GetTimeStep());
		}
	}
}
