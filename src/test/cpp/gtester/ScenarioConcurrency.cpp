/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Concurrency test.
 * Run executions concurrently and check they produce identical results.
 */

#include "CompareSim.h"

#include "common/InstallDirs.h"
#include "session/SimSession.h"
#include "sim/Sim.h"
#include "workspace/CliWorkspace.h"

#include <gtest/gtest.h>
#include <boost/property_tree/xml_parser.hpp>
#include <QDir>
#include <QThread>
#include <tuple>

using namespace std;
using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;
using namespace SimPT_Shell;

namespace SimPT_Shell {
namespace Tests {

class ConcurrentSim : public QThread
{
public:
	ConcurrentSim(shared_ptr<Session::SimSession> s, unsigned int steps)
		: m_sim(s->GetSim()), m_steps(steps)
	{
	}

	virtual ~ConcurrentSim()
	{
	}

	const SimPT_Sim::Sim& GetSim() const
	{
		return *m_sim;
	}

	virtual void run() override
	{
		for (unsigned int i = 0; i < m_steps; ++i) {
			m_sim->TimeStep();
		}
	}

private:
	shared_ptr<SimPT_Sim::Sim>   m_sim;
	unsigned int                 m_steps;
};

class ScenarioConcurrency
	: public ::testing::TestWithParam<tuple<tuple<string, unsigned int>, unsigned int>>
{
public:
	/// Destructor has to be virtual.
	virtual ~ScenarioConcurrency() {}
};

/**
 * Run a few Simulators concurrently and sees if they get the same result,
 * testing whether a Sim component is reentrant.
 */
TEST_P(ScenarioConcurrency, TestConcurrentSim)
{
	string const data_dir = SimShell::InstallDirs::GetDataDir();
	QDir dir(QString::fromStdString(data_dir));
	ASSERT_EQ(true, dir.cd("simPT_Default_workspace"));

	tuple<tuple<string, unsigned int>, unsigned int> t(GetParam());
	const string project = get<0>(get<0>(t));
	const unsigned int simulationSteps = get<1>(get<0>(t));
	const unsigned int concurrentThreads = get<1>(t);

	// Read sim data and settings
	auto ws_ptr = make_shared<Ws::CliWorkspace>(dir.absolutePath().toStdString());
	auto project_ptr = ws_ptr->Get(project);
	auto file_ptr = project_ptr->Back();
	ASSERT_EQ(true, dir.exists(QString::fromStdString(file_ptr->GetPath())));

	vector<ConcurrentSim*> threads;
	for (unsigned int i = 0; i < concurrentThreads; ++i) {
		auto session = file_ptr->CreateSession(project_ptr, ws_ptr); // session creation, bypass project session "management"
		threads.push_back(new ConcurrentSim(
			static_pointer_cast<Session::SimSession>(session), simulationSteps));
	}

	for (auto thread : threads) {
		thread->start();
	}

	for (auto thread : threads) {
		thread->wait();
	}

	for (unsigned int i = 0; i < concurrentThreads - 1; ++i) {
		ASSERT_TRUE(CompareSim::PTrees(threads[i]->GetSim().ToPtree(), threads[i + 1]->GetSim().ToPtree(), 1.0e-4));
	}

	for (auto thread : threads) {
		delete thread;
	}
}

namespace {

tuple<string, unsigned int> scenarios[] {
	 make_tuple("AuxinGrowth",  50U)
	,make_tuple("Geometric",    50U)
	,make_tuple("Meinhardt",    50U)
        ,make_tuple("NoGrowth",     50U)
	,make_tuple("TipGrowth",    50U)
	,make_tuple("Wortel",        2U)
	};

unsigned int concurrentThreads[] { 5U };

}

INSTANTIATE_TEST_CASE_P(ScenarioConcurrency_a, ScenarioConcurrency,
	::testing::Combine(::testing::ValuesIn(scenarios), ::testing::ValuesIn(concurrentThreads)));


} // namespace
} // namespace
