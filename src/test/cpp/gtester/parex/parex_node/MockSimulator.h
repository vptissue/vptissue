#ifndef MOCK_SIMULATOR_H_INCLUDED
#define MOCK_SIMULATOR_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for MockSimulator.
 */

#include "MockServer.h"
#include "parex_node/Simulator.h"

namespace SimPT_Parex {
namespace Tests {

/**
 * Mock simulator class to test the node.
 */
class MockSimulator: public Simulator
{
	Q_OBJECT
public:
	///
	MockSimulator();

	///
	virtual ~MockSimulator();

	///
	bool TestSucceeded();

	///
	void setCurrentTest(stests t) { m_currentTest = t; }

public slots:
	virtual void SolveTask(const SimTask& );

signals:
        ///
	void TaskSolved(const SimResult& );

	///
	void Done();

private:
	bool m_testSucceeded;
	stests m_currentTest;
};

} // namespace
} // namespace

#endif // end-of-include-guard
