/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of test of Exploration.
 */

#include "common/InstallDirs.h"
#include "CompareSim.h"
#include "parex_protocol/Exploration.h"
#include "parex_protocol/ExplorationProgress.h"
#include "parex_protocol/ParameterExploration.h"
#include "parex_protocol/SimTask.h"
#include "parex_protocol/RangeSweep.h"
#include "sim/Sim.h"
#include "workspace/StartupFilePtree.h"

#include <gtest/gtest.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <sstream>
#include <string>

using namespace std;
using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;
using namespace SimPT_Shell;
using SimShell::InstallDirs;

namespace SimPT_Parex {
namespace Tests {


static const string g_file_name = "/simPT_Default_workspace/Geometric/leaf_000000.xml";

TEST(UnitParexExploration, CreateExploration)
{
	const string path = InstallDirs::GetDataDir().append(g_file_name);
	auto file = Ws::StartupFileBase::Constructor(path, path);
	auto sim_pt = static_pointer_cast<Ws::StartupFilePtree>(file)->ToPtree();

	Exploration* exploration = new ParameterExploration("test", sim_pt, ptree());
	ExplorationProgress progress(exploration);

	ASSERT_EQ(progress.GetTaskCount(TaskState::Waiting), 1U);
	ASSERT_EQ(progress.GetExploration().GetNumberOfTasks(), 1U);
}

TEST(UnitParexExploration, CopyExploration)
{
	const string path = InstallDirs::GetDataDir().append(g_file_name);
	auto file = Ws::StartupFileBase::Constructor(path, path);
	auto sim_pt = static_pointer_cast<Ws::StartupFilePtree>(file)->ToPtree();

	Exploration* exploration = new ParameterExploration("test", sim_pt, ptree());
	Exploration* copiedExploration = exploration->Clone();

	ExplorationProgress progress(exploration);
	ExplorationProgress copiedProgress(copiedExploration);

	ASSERT_EQ(progress.GetExploration().GetNumberOfTasks(), copiedProgress.GetExploration().GetNumberOfTasks());
	ASSERT_EQ(progress.GetTaskCount(TaskState::Waiting), copiedProgress.GetTaskCount(TaskState::Waiting));
	ASSERT_EQ(progress.GetTaskCount(TaskState::Finished), copiedProgress.GetTaskCount(TaskState::Finished));

	auto task = progress.NextTask();
	auto copiedTask = copiedProgress.NextTask();

	ASSERT_EQ(task->ToString(), copiedTask->ToString());

	delete task;
	delete copiedTask;
}

TEST(UnitParexExploration, GetSingleNextTask)
{
	const string path = InstallDirs::GetDataDir().append(g_file_name);
	auto file = Ws::StartupFileBase::Constructor(path, path);
	auto sim_pt = static_pointer_cast<Ws::StartupFilePtree>(file)->ToPtree();

	Exploration* exploration = new ParameterExploration("test", sim_pt, ptree());
	ExplorationProgress progress(exploration);

	auto task = progress.NextTask();
	ASSERT_NE(nullptr, task);
	delete task;

	task = progress.NextTask();
	ASSERT_EQ(nullptr, task);
}

TEST(UnitParexExploration, ConvertFromToPtree)
{
	const string path = InstallDirs::GetDataDir().append(g_file_name);
	auto file = Ws::StartupFileBase::Constructor(path, path);
	auto sim_pt = static_pointer_cast<Ws::StartupFilePtree>(file)->ToPtree();

	ParameterExploration exploration("test", sim_pt, ptree());
	ParameterExploration copiedExploration(exploration.ToPtree());

	auto task = exploration.CreateTask(0);
	auto copiedTask = copiedExploration.CreateTask(0);

	ASSERT_EQ(true, CompareSim::PTrees(task->ToPtree(), copiedTask->ToPtree(), 10e-6));

	delete task;
	delete copiedTask;
}

TEST(UnitParexExploration, BackUpNewExploration)
{
	const string path = InstallDirs::GetDataDir().append(g_file_name);
	auto file = Ws::StartupFileBase::Constructor(path, path);
	auto sim_pt = static_pointer_cast<Ws::StartupFilePtree>(file)->ToPtree();

	ParameterExploration* exploration = new ParameterExploration("test", sim_pt, ptree());
	exploration->SetSweep("cell_mechanics.base_area", new RangeSweep(1, 5, 1));
	ExplorationProgress progress(exploration);
	ExplorationProgress copiedProgress(progress.ToPtree());

	ASSERT_EQ(progress.GetExploration().GetNumberOfTasks(), copiedProgress.GetExploration().GetNumberOfTasks());
	ASSERT_EQ(progress.GetTaskCount(TaskState::Waiting), copiedProgress.GetTaskCount(TaskState::Waiting));

	auto task = progress.NextTask();
	auto copiedTask = copiedProgress.NextTask();

	ASSERT_EQ(task->ToString(), copiedTask->ToString());

	delete task;
	delete copiedTask;
}

TEST(UnitParexExploration, BackUp)
{
	const string path = InstallDirs::GetDataDir().append(g_file_name);
	auto file = Ws::StartupFileBase::Constructor(path, path);
	auto sim_pt = static_pointer_cast<Ws::StartupFilePtree>(file)->ToPtree();

	ParameterExploration* exploration = new ParameterExploration("test", sim_pt, ptree());
	exploration->SetSweep("cell_mechanics.base_area", new RangeSweep(1, 5, 1));
	ExplorationProgress progress(exploration);
	delete progress.NextTask();
	delete progress.NextTask();
	ExplorationProgress copiedProgress(progress.ToPtree());

	ASSERT_EQ(progress.GetExploration().GetNumberOfTasks(), copiedProgress.GetExploration().GetNumberOfTasks());
	ASSERT_EQ(progress.GetTaskCount(TaskState::Waiting), copiedProgress.GetTaskCount(TaskState::Waiting));

	auto task = progress.NextTask();
	auto copiedTask = copiedProgress.NextTask();

	ASSERT_EQ(task->ToString(), copiedTask->ToString());

	delete task;
	delete copiedTask;
}

} // namespace
} // namespace
