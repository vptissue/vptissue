/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Unit tests of AttributeStore.
 */

#include "AttributePtreeSetup.h"
#include "bio/AttributeStore.h"

#include <gtest/gtest.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include <ostream>
#include <stdexcept>
#include <string>

using namespace std;
using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;
using namespace SimPT_Sim::Util;

namespace SimPT_Sim {
namespace Tests {

TEST( UnitAttributeStore, Instatiation )
{
	AttributePtreeSetup a_pt;
	a_pt.Add<int>("attr2", 5);
	a_pt.Add<string>("haha", "None");
	a_pt.Add<double>("dd", 2.0);
	a_pt.Add<int>("i2", 33);
	AttributeStore a(a_pt.Get());

	const auto t = a.IsStrictSizeConsistent();
	ASSERT_EQ( true, get<0>(t));
	ASSERT_EQ( static_cast<size_t>(0), get<1>(t));
}

TEST( UnitAttributeStore, DeafultValues )
{
	AttributePtreeSetup a_pt;
	a_pt.Add<bool>("attr0", true);
	a_pt.Add<int>("attr1", 1);
	a_pt.Add<string>("attr2", "None");
	AttributeStore a(a_pt.Get());

	ASSERT_EQ( true, a.GetDefaultValue<bool>("attr0") );
	ASSERT_EQ( 1, a.GetDefaultValue<int>("attr1") );
	ASSERT_EQ( "None", a.GetDefaultValue<string>("attr2") );
}

TEST( UnitAttributeStore, StrictSizeConsistent )
{
	AttributePtreeSetup a_pt;
	a_pt.Add<int>("attr1", 1);
	a_pt.Add<double>("attr2", 5.0);
	AttributeStore a(a_pt.Get());

	// AttributeStore is StrictSizeConsistent after creation.
	auto t = a.IsStrictSizeConsistent();
	ASSERT_TRUE( get<0>(t) );
	ASSERT_EQ( static_cast<size_t>(0), get<1>(t) );

	// AttributeStore is not StrictSezeConsistent unless all attributes
	// have been push an equal number of times.
	a.Container<int>("attr1").push_back(1);
	ASSERT_FALSE( get<0>(a.IsStrictSizeConsistent()) );

	// AttributeStore is StrictSezeConsistent when all attributes
	// have been push an equal number of times.
	a.Container<double>("attr2").push_back(1.0);
	t = a.IsStrictSizeConsistent();
	ASSERT_TRUE( get<0>(t) );
	ASSERT_EQ( static_cast<size_t>(1), get<1>(t) );
}

TEST( UnitAttributeStore, WeakSizeConsistent )
{
	AttributePtreeSetup a_pt;
	a_pt.Add<int>("attr1", 1);
	a_pt.Add<double>("attr2", 5.0);
	AttributeStore a(a_pt.Get());

	ASSERT_TRUE( a.IsWeakSizeConsistent(1));
	auto& attr1 = a.Container<int>("attr1");
	attr1.push_back(1);

	ASSERT_TRUE( a.IsWeakSizeConsistent(1) );
	ASSERT_FALSE( a.IsWeakSizeConsistent(2) );
	auto& attr2 = a.Container<double>("attr2");
	attr2.push_back(2.0);
}

TEST( UnitAttributeStore, Initialize )
{
	AttributePtreeSetup a_pt;
	a_pt.Add<int>("attr1", 1);
	a_pt.Add<double>("attr2", 5.0);
	AttributeStore a(a_pt.Get());

	const auto v1 = std::vector<int> {1, 2};
	ASSERT_EQ( true, a.IsWeakSizeConsistent(v1.size()) );
	a.Initialize("attr1", v1);

	const auto v2 = std::vector<double> {4, 5};
	ASSERT_EQ( true, a.IsWeakSizeConsistent(v2.size()));
	a.Initialize("attr2", v2);

	const auto t = a.IsStrictSizeConsistent();
	ASSERT_EQ( true, get<0>(t));
	ASSERT_EQ( static_cast<size_t>(2), get<1>(t));

	// Throw if name not in index.
	ASSERT_THROW( a.Initialize("attr3", std::vector<int> {1, 2}), runtime_error );

	// Throw if size of input vector inconsistent.
	ASSERT_THROW( a.Initialize("attr2", std::vector<double> {1, 2, 3}), runtime_error );
}

TEST( UnitAttributeStore, Get )
{
	AttributePtreeSetup a_pt;
	a_pt.Add<int>("attr1", 1);
	a_pt.Add<string>("attr2", "None");
	AttributeStore a(a_pt.Get());

	a.Initialize("attr1", std::vector<int> {1, 2});
	a.Initialize("attr2", std::vector<string> {"aa", "bb"});

	const auto& attr1 = a.Container<int>("attr1");
	ASSERT_EQ( static_cast<size_t>(2), attr1.size());
	ASSERT_EQ( 1, attr1[0] );
	ASSERT_EQ( 2, attr1[1] );

	const auto& attr2 = a.Container<string>("attr2");
	ASSERT_EQ( static_cast<size_t>(2), attr2.size());
	ASSERT_EQ( string("aa"), attr2[0] );
	ASSERT_EQ( string("bb"), attr2[1] );
}

TEST( UnitAttributeStore, Set )
{
	AttributePtreeSetup a_pt;
	a_pt.Add<int>("attr1", 1);
	a_pt.Add<string>("attr2", "None");
	AttributeStore a(a_pt.Get());

	a.Initialize("attr1", std::vector<int> {1, 2});
	a.Initialize("attr2", std::vector<string> {"aa", "bb"});

	auto& attr1 = a.Container<int>("attr1");
	ASSERT_EQ( static_cast<size_t>(2), attr1.size());
	attr1[0] = 10;
	attr1[1] = 20;
	ASSERT_EQ( 10, attr1[0] );
	ASSERT_EQ( 20, attr1[1] );

	auto& attr2 = a.Container<string>("attr2");
	ASSERT_EQ( static_cast<size_t>(2), attr2.size());
	attr2[0] = "xx";
	attr2[1] = "yy";
	ASSERT_EQ( string("xx"), attr2[0] );
	ASSERT_EQ( string("yy"), attr2[1] );
}

} // namespace
} // namespace
