/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Unit tests for EditableEdgeItem.
 */

#include "bio/Mesh.h"
#include "bio/Node.h"
#include "editor/EditableEdgeItem.h"
#include "editor/EditableNodeItem.h"

#include <gtest/gtest.h>
#include <vector>

namespace SimPT_Editor {
namespace Tests {

class UnitEditableEdgeItem : public ::testing::Test
{
protected:
	UnitEditableEdgeItem() : ::testing::Test(), m_mesh(0)
	{
	}

	virtual void SetUp()
	{
		m_endpoints.push_back(m_mesh.BuildNode(0, {{0, 0, 0}}, true));
		m_endpoints.push_back(m_mesh.BuildNode(1, {{1, 0, 0}}, true));
		m_endpoints.push_back(m_mesh.BuildNode(2, {{0, 1, 0}}, false));
		m_endpoints.push_back(m_mesh.BuildNode(3, {{2, 2, 0}}, false));
		m_endpoints.push_back(m_mesh.BuildNode(4, {{3, 3, 0}}, false));
		m_nodes.push_back(new EditableNodeItem(m_endpoints[0], 1));
		m_nodes.push_back(new EditableNodeItem(m_endpoints[1], 1));
		m_nodes.push_back(new EditableNodeItem(m_endpoints[2], 1));
		m_nodes.push_back(new EditableNodeItem(m_endpoints[3], 1));
		m_nodes.push_back(new EditableNodeItem(m_endpoints[4], 1));
		m_edges.push_back(new EditableEdgeItem(m_nodes[0],m_nodes[1]));
		m_edges.push_back(new EditableEdgeItem(m_nodes[1],m_nodes[2]));
		//edge that doesn't share an endpoint
		m_edges.push_back(new EditableEdgeItem(m_nodes[3],m_nodes[4]));
	}

	virtual void TearDown()
	{
		//Clean up edges
		for(std::vector<EditableEdgeItem*>::iterator it = m_edges.begin(); it != m_edges.end(); ++it ){
			delete (*it);
		}

		//Clean up nodes
		for(std::vector<EditableNodeItem*>::iterator it = m_nodes.begin(); it != m_nodes.end(); ++it ){
			delete (*it);
		}
	}

	// Data members of the test fixture
	SimPT_Sim::Mesh 					m_mesh;
	std::vector<EditableNodeItem*>				m_nodes;
	std::vector<EditableEdgeItem*>				m_edges;
	std::vector<SimPT_Sim::Node*> 		m_endpoints;
};

TEST_F(UnitEditableEdgeItem, TestFirst)
{
	EditableNodeItem* node = m_edges[0]->First();
	EXPECT_EQ(m_nodes[0], node) << "Wrong first endpoint";
	EXPECT_NE(m_nodes[1], node) << "Wrong first endpoint";
	EXPECT_NE(m_nodes[2], node) << "Wrong first endpoint";
	EXPECT_NE(m_nodes[3], node) << "Wrong first endpoint";
	EXPECT_NE(m_nodes[4], node) << "Wrong first endpoint";

	EditableNodeItem* node2 = m_edges[1]->First();
	EXPECT_EQ(m_nodes[1],  node2) << "Wrong first endpoint";
	EXPECT_NE(m_nodes[2], node2) << "Wrong first endpoint";
	EXPECT_NE(m_nodes[0], node2) << "Wrong first endpoint";
	EXPECT_NE(m_nodes[3], node2) << "Wrong first endpoint";
	EXPECT_NE(m_nodes[4], node2) << "Wrong first endpoint";
}

TEST_F(UnitEditableEdgeItem, TestSecond)
{
	EditableNodeItem* node = m_edges[0]->Second();
	EXPECT_EQ(m_nodes[1], node) << "Wrong second endpoint";
	EXPECT_NE(m_nodes[0], node) << "Wrong second endpoint";
	EXPECT_NE(m_nodes[2], node) << "Wrong second endpoint";
	EXPECT_NE(m_nodes[3], node) << "Wrong second endpoint";
	EXPECT_NE(m_nodes[4], node) << "Wrong second endpoint";

	EditableNodeItem* node2 = m_edges[1]->Second();
	EXPECT_EQ(m_nodes[2], node2) << "Wrong second endpoint";
	EXPECT_NE(m_nodes[1], node2) << "Wrong second endpoint";
	EXPECT_NE(m_nodes[0], node2) << "Wrong second endpoint";
	EXPECT_NE(m_nodes[3], node2) << "Wrong second endpoint";
	EXPECT_NE(m_nodes[4], node2) << "Wrong second endpoint";
}

TEST_F(UnitEditableEdgeItem, TestIsAtBoundary)
{
	//Both at boundary
	EXPECT_TRUE(m_edges[0]->IsAtBoundary()) << "Endpoints should be at boundary";
	//One at boundary
	EXPECT_FALSE(m_edges[1]->IsAtBoundary()) << "Endpoints shouldn't be at boundary";
	//None at boundary
	EXPECT_FALSE(m_edges[2]->IsAtBoundary()) << "Endpoints shouldn't be at boundary";
}

TEST_F(UnitEditableEdgeItem, TestContainsEndpoint)
{
	EXPECT_TRUE(m_edges[0]->ContainsEndpoint(m_nodes[0])) << "Should contain node";
	EXPECT_TRUE(m_edges[0]->ContainsEndpoint(m_nodes[1])) << "Should contain node";
	EXPECT_FALSE(m_edges[0]->ContainsEndpoint(m_nodes[2])) << "Shouldn't contain node";
	EXPECT_FALSE(m_edges[0]->ContainsEndpoint(m_nodes[3])) << "Shouldn't contain node";
	EXPECT_FALSE(m_edges[0]->ContainsEndpoint(m_nodes[4])) << "Shouldn't contain node";

	EXPECT_TRUE(m_edges[1]->ContainsEndpoint(m_nodes[1])) << "Should contain node";
	EXPECT_TRUE(m_edges[1]->ContainsEndpoint(m_nodes[2])) << "Should contain node";
	EXPECT_FALSE(m_edges[1]->ContainsEndpoint(m_nodes[0])) << "Shouldn't contain node";
	EXPECT_FALSE(m_edges[1]->ContainsEndpoint(m_nodes[3])) << "Shouldn't contain node";
	EXPECT_FALSE(m_edges[1]->ContainsEndpoint(m_nodes[4])) << "Shouldn't contain node";
}

TEST_F(UnitEditableEdgeItem, TestContainsEndpoints)
{
	//Both nodes are endpoints of the edge
	EXPECT_TRUE(m_edges[0]->ContainsEndpoints(m_nodes[0], m_nodes[1])) << "Should contain nodes";
	//The first node is an endpoint of the edge, the second node isn't
	EXPECT_FALSE(m_edges[0]->ContainsEndpoints(m_nodes[0], m_nodes[2])) << "Shouldn't contain node";
	//The second node is an endpoint of the edge, the first node isn't
	EXPECT_FALSE(m_edges[0]->ContainsEndpoints(m_nodes[1], m_nodes[2])) << "Shouldn't contain node";
	//Both nodes aren't endpoints of the edge
	EXPECT_FALSE(m_edges[0]->ContainsEndpoints(m_nodes[2], m_nodes[3])) << "Shouldn't contain nodes";

	//Both nodes are endpoints of the edge
	EXPECT_TRUE(m_edges[1]->ContainsEndpoints(m_nodes[1], m_nodes[2])) << "Should contain nodes";
	//The first node is an endpoint of the edge, the second node isn't
	EXPECT_FALSE(m_edges[1]->ContainsEndpoints(m_nodes[1], m_nodes[3])) << "Shouldn't contain node";
	//The second node is an endpoint of the edge, the first node isn't
	EXPECT_FALSE(m_edges[1]->ContainsEndpoints(m_nodes[3], m_nodes[2])) << "Shouldn't contain node";
	//Both nodes aren't endpoints of the edge
	EXPECT_FALSE(m_edges[1]->ContainsEndpoints(m_nodes[3], m_nodes[4])) << "Shouldn't contain nodes";
}

TEST_F(UnitEditableEdgeItem, TestConnectingNode)
{
	//Connected edges
	EditableNodeItem* node = m_edges[0]->ConnectingNode(m_edges[1]);
	EXPECT_EQ(m_nodes[1], node) << "Should be the connection node";
	//The edges are connected, so should have an connecting node
	EXPECT_NE(nullptr, node) << "The two edges are disconnected";
	//The nodes are part of the edge but aren't the connecting node
	EXPECT_NE(m_nodes[0], node) << "Shouldn't be the connection node";
	EXPECT_NE(m_nodes[2], node) << "Shouldn't be the connection nodee";
	//The nodes are not part of the edge
	EXPECT_NE(m_nodes[3], node) << "Shouldn't be the connection node";
	EXPECT_NE(m_nodes[4], node) << "Shouldn't be the connection node";

	//Disconnected edges
	EditableNodeItem* node2 = m_edges[0]->ConnectingNode(m_edges[2]);
	//The edges are disconnected, so shouldn't have an connecting node
	EXPECT_EQ(nullptr, node2) << "The two edges are connected";
	//The nodes are the endpoint of one of the two edges
	EXPECT_NE(m_nodes[0], node2) << "Shouldn't be the connection node";
	EXPECT_NE(m_nodes[2], node2) << "Shouldn't be the connection node";
	EXPECT_NE(m_nodes[3], node2) << "Shouldn't be the connection node";
	EXPECT_NE(m_nodes[4], node2) << "Shouldn't be the connection node";
	//The node isn't part of any of the two edges
	EXPECT_NE(m_nodes[1], node2) << "Shouldn't be the connection node";
}

TEST_F(UnitEditableEdgeItem, TestMergeEdge)
{
	//Connected edges
	EditableEdgeItem* edge = m_edges[0]->MergeEdge(m_edges[1]);
	EXPECT_TRUE(edge->ContainsEndpoints(m_nodes[0], m_nodes[2])) << "Merged edge hasn't correct endpoints";
	EXPECT_FALSE(edge->ContainsEndpoints(m_nodes[0], m_nodes[1])) << "Merged edge contains connectingnode";
	EXPECT_FALSE(edge->ContainsEndpoints(m_nodes[1], m_nodes[2])) << "Merged edge contains connectingnode";
}

TEST_F(UnitEditableEdgeItem, TestSplitEdge)
{
	//Merge edge first
	EditableEdgeItem* edge = m_edges[0]->MergeEdge(m_edges[1]);

	//Split on point that is on the edge
	std::pair<EditableEdgeItem*, EditableEdgeItem*> edgepair = edge->SplitEdge(m_nodes[1]);
	EXPECT_TRUE(edgepair.first->ContainsEndpoints(m_nodes[0], m_nodes[1])) << "Edge hasn't correct endpoints";
	EXPECT_TRUE(edgepair.second->ContainsEndpoints(m_nodes[1], m_nodes[2])) << "Edge hasn't correct endpoints";

	//Split on point that is not on the edge
	std::pair<EditableEdgeItem*, EditableEdgeItem*> edgepair2 = edge->SplitEdge(m_nodes[4]);
	EXPECT_TRUE(edgepair2.first->ContainsEndpoints(m_nodes[0], m_nodes[4])) << "Edge hasn't correct endpoints";
	EXPECT_TRUE(edgepair2.second->ContainsEndpoints(m_nodes[4], m_nodes[2])) << "Edge hasn't correct endpoints";

	//Split on point that is endpoint of the edge
	std::pair<EditableEdgeItem*, EditableEdgeItem*> edgepair3 = edge->SplitEdge(m_nodes[0]);
	EXPECT_TRUE(edgepair3.second->ContainsEndpoints(m_nodes[0], m_nodes[2])) << "Edge hasn't correct endpoints";
	EXPECT_TRUE(edgepair3.second->ContainsEndpoints(m_nodes[0], m_nodes[2])) << "Edge hasn't correct endpoints";
}

} // namespace
} // namespace
