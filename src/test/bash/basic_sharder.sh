#!/bin/sh
#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################
#set -x

#----------------------------------------------------------------------------
# Defaults
#----------------------------------------------------------------------------
GTEST_DEFAULT_TOTAL_SHARDS=16

#----------------------------------------------------------------------------
# Script must be executed in the root tests location i.e. TESTS_DIR.
#----------------------------------------------------------------------------
TESTS_DIR=$PWD
SHARDS_DIR=$TESTS_DIR/basic_gshards_all
#
EXEC_DIR=$PWD/bin
GTESTER=$EXEC_DIR/gtester

#----------------------------------------------------------------------------
# Use GTest sharding. Arg provides number of shards, otherwise default
#----------------------------------------------------------------------------
if [ $# -eq 0 ]; then
	GTEST_TOTAL_SHARDS=$GTEST_DEFAULT_TOTAL_SHARDS
else
	GTEST_TOTAL_SHARDS=$1
fi
export GTEST_TOTAL_SHARDS

#----------------------------------------------------------------------------
# Command to be executed in each shard subdir
#----------------------------------------------------------------------------
COMMAND="$GTESTER --gtest_shuffle --gtest_filter=-*Perf*.*"

#----------------------------------------------------------------------------
# Execute the test with sharding.
#----------------------------------------------------------------------------
INDEX=0
rm -rf $SHARDS_DIR && mkdir $SHARDS_DIR
while [ $INDEX -lt $GTEST_TOTAL_SHARDS ]
do
	DIR=$SHARDS_DIR/all_shard_$INDEX
	rm -rf $DIR && mkdir $DIR
	(export GTEST_SHARD_INDEX=$INDEX; cd $DIR && $COMMAND) &
	INDEX=$(( $INDEX + 1 ))
done

#----------------------------------------------------------------------------
# Wait for tests to finish.
#----------------------------------------------------------------------------
wait

#----------------------------------------------------------------------------
# Done
#----------------------------------------------------------------------------
exit

#############################################################################
