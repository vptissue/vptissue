Copyright 2011-2016 Universiteit Antwerpen
Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
the European Commission - subsequent versions of the EUPL (the "Licence");
You may not use this work except in compliance with the Licence.
You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
Unless required by applicable law or agreed to in writing, software 
distributed under the Licence is distributed on an "AS IS" basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.


# User and reference documentation are available at <https://vptissue.bitbucket.io/>  


Information on directory layout, installation, supported platforms can be found in the files INSTALL.txt, DEPENDENCIES.txt and PLATFORMS.txt. For license info and a list of authors, see LICENSE.txt and AUTHORS.txt.
